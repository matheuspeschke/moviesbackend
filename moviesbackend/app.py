# coding=UTF-8
"""
Module: Flask API interface, with routes to a RESTful model and connected
to a database marshalling provider.
"""
from flask import jsonify, request, Response, json
from moviesbackend.models import Pino, Dvd, Movie, Actor, MovieActor, Genre, \
    Subtitle, MovieGenre, MovieSubtitle, PINONOTFOUND, \
    DVDNOTFOUND, MOVIENOTFOUND, ACTORNOTFOUND, GENRENOTFOUND, SUBTITLENOTFOUND
from moviesbackend.validations import valid_dvd, valid_dvd_for_patch, \
    valid_pino, valid_movie_for_put, valid_movie_for_post, \
    valid_movie_for_patch, valid_movie_for_get, valid_actor, \
    valid_actor_for_put, valid_actor_for_patch, valid_movieactor, \
    valid_genre, valid_subtitle, valid_moviegenre, valid_moviesubtitle
from moviesbackend.settings import APP

PINOHELPSTRING = "Use a json format: {'name': 'pino name'}. 'pino name' is m" \
                 "andatory, not empty, unique and up to 8 characters long."
DVDHELPSTRING = "Use a json format: {'name': 'dvd name', 'pino': 'pino name'" \
                "}. 'pino name' and 'dvd name' are mandatory, unique and up " \
                "to 8 characters long."
MOVIEHELPSTRING = "Use a json format: {'name': 'movie title', 'dvd': " \
                  "'dvd name', 'director': 'movie director', 'imdbid': " \
                  "'movie's imdb ID (ex: tt6820256)', 'year': 'movie's relea" \
                  "se year', 'format': 'format of the film', 'actors': [], '" \
                  "genres': [], 'subtitles': []}." \
                  " 'name', 'dvd', 'director' and 'year' are mandatory." \
                  "'name' is up to 100 characters long, 'director' is up to " \
                  "215 characters long, 'imdbid' is up to 12 characters long" \
                  " and unique, 'year' is up to 4 characters long, 'format' " \
                  "is up to 100 characters long. actors, genres and subtitle" \
                  "s must be empty arrays."
ACTORPATCHHELPSTRING = ACTORHELPSTRING = \
    "Use a json format: {'name': 'movie title', 'imdbid': " \
    "'actor's imdb url'}." \
    "'name' and 'imdbid' are mandatory." \
    "'name' is up to 200 characters long, 'imdbid' is up to 16" \
    " characters long"
MOVIEACTORHELPSTRING = \
    "Use a json format: {'actorimdbid': 'actor's imdbid', 'movieimdbid': " \
    "'movie's imdbid'}." \
    "'actorimdbid' and 'imdbid' are mandatory." \
    "'actorimdbid' is up to 16 characters long, 'movieimdbid' is up to 12" \
    " characters long"
MOVIEGENREHELPSTRING = \
    "Use a json format: {'genre': 'movie genre', 'movieimdbid': " \
    "'movie's imdbid'}." \
    "'genre' and 'imdbid' are mandatory." \
    "'genre' is up to 20 characters long, 'movieimdbid' is up to 12" \
    " characters long"
MOVIESUBTITLEHELPSTRING = \
    "Use a json format: {'subtitle': 'subtitle language', 'movieimdbid': " \
    "'movie's imdbid'}." \
    "'subtitle' and 'imdbid' are mandatory." \
    "'subtitle' is up to 20 characters long, 'movieimdbid' is up to 12" \
    " characters long"
GENREHELPSTRING = \
    "Use a json format: {'name': 'genre description'}." \
    "'name' is mandatory." \
    "'name' is up to 20 characters long"
SUBTITLEHELPSTRING = MOVIESUBTITLEHELPSTRING = \
    "Use a json format: {'name': 'subtitle description'}." \
    "'name' is mandatory." \
    "'name' is up to 20 characters long"
PINOPATCHHELPSTRING = "Use a json format: {'pino': 'pino name'}"
DVDPATCHHELPSTRING = "Use a json format: {'pino': 'pino name'}"
MOVIEPATCHHELPSTRING = "Use a json format: {'name': 'movie title', 'dvd': " \
                  "'dvd name', 'director': 'movie director', 'year': 'movie'" \
                  "s release year', 'format': 'format of the film'}." \
                  "'name', 'dvd', 'director' and 'year' are mandatory." \
                  "'name' is up to 100 characters long, 'director' is up to " \
                  "215 characters long, 'imdb' is up to 70 characters long, " \
                  "'year' is up to 4 characters long, 'format' is up to 100 " \
                  "characters long."
GENREPATCHHELPSTRING = "Use a json format: {'name': 'genre name'}." \
                  "'name' is mandatory." \
                  "'name' is up to 20 characters long."
SUBTITLEPATCHHELPSTRING = "Use a json format: {'name': 'subtitle name'}." \
                  "'name' is mandatory." \
                  "'name' is up to 20 characters long."

PINOMALFORMEDDOCUMENT = "Malformed Pino document '{}'"
DVDMALFORMEDDOCUMENT = "Malformed Dvd document '{}'"
MOVIEMALFORMEDDOCUMENT = "Malformed Movie document '{}'"
ACTORMALFORMEDDOCUMENT = "Malformed Actor document '{}'"
GENREMALFORMEDDOCUMENT = "Malformed Genre document '{}'"
SUBTITLEMALFORMEDDOCUMENT = "Malformed Subtitle document '{}'"
MOVIEACTORMALFORMEDDOCUMENT = "Malformed MovieActor document '{}'"
MOVIESUBTITLEMALFORMEDDOCUMENT = "Malformed MovieSubtitle document '{}'"
MOVIEGENREMALFORMEDDOCUMENT = "Malformed MovieGenre document '{}'"
MOVIESUBTITLEMALFORMEDDOCUMENT = "Malformed MovieSubtitle document '{}'"


@APP.route('/', methods=['GET'])
def get_resources():
    """
    HTTP GET method for Pino model
    :return: JSON object representing a list of all Pinos.
    """
    return jsonify(
        {
            'pinos': '{}pinos'.format(request.base_url),
            'dvds': '{}dvds'.format(request.base_url),
            'movies': '{}movies'.format(request.base_url),
            'actors': '{}actors'.format(request.base_url),
            'genres': '{}genres'.format(request.base_url),
            'subtitles': '{}subtitles'.format(request.base_url)
        }
    )


# PINOS #######################################################################

@APP.route('/pinos', methods=['GET'])
@APP.route('/pinos/page/<int:page>', methods=['GET'])
def get_pinos(page=1):
    """
    HTTP GET method for Pino model
    :return: JSON object representing a list of all Pinos.
    """
    return jsonify({'pinos': Pino.get_all_pinos(page=page)})


@APP.route('/pinos/<string:name>', methods=['GET'])
def get_pino_by_name(name):
    """
    HTTP GET method for Pino model
    :param name: query parameter: pino name.
    :return: JSON object representing a Pino. Since 'name' is an unique key,
    the first (and only) matching pino. If Pino not found, a JSON Response with
    an error description + help string.
    """
    pino = Pino.get_pino(name)
    if valid_pino(pino):
        return jsonify(pino)

    invalidpinoerrormsg = {
        "error": PINONOTFOUND.format(name),
        "helpString": PINOHELPSTRING
    }
    response = Response(json.dumps(invalidpinoerrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/pinos', methods=['POST'])
def post_pino():
    """
    HTTP POST method for Pino model
    :return: a HTTP response object
    """
    request_data = request.get_json(force=True)
    if valid_pino(request_data):
        try:
            Pino.add_pino(request_data['name'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/pinos/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidpinoerrormsg = {
                "error": str(type(exc)),
                "helpString": PINOHELPSTRING
            }
            response = Response(json.dumps(invalidpinoerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidpinoerrormsg = {
        "error": PINOMALFORMEDDOCUMENT.format(request_data),
        "helpString": PINOHELPSTRING
    }
    response = Response(json.dumps(invalidpinoerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/pinos/<string:name>', methods=['PUT'])
def put_pino(name):
    """
    HTTP PUT method for Pino model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the Pino resource do not exist?
       Pino resource will be created (201).
    2) What happens if the Pino resource exists?
       Pino resource will be updated (204).
    3) What happens if the update conflicts with an existing Pino?
       Pino resource will not be updated because the new one already exists -
       conflicting state (409).
    4) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Pino name
    :return: a HTTP response object (201, 204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_pino(request_data):
        try:
            httpcode = Pino.update_pino(name, request_data['name'])
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/pinos/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidpinoerrormsg = {
                "error": str(type(exc)),
                "helpString": PINOHELPSTRING
            }
            response = Response(json.dumps(invalidpinoerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidpinoerrormsg = {
        "error": PINOMALFORMEDDOCUMENT.format(request_data),
        "helpString": PINOHELPSTRING
    }
    response = Response(json.dumps(invalidpinoerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/pinos/<string:name>', methods=['PATCH'])
def patch_pino(name):
    """
    HTTP PATCH method for Pino model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No. It will update only
       the 'name' property, and return 409 (conflict) if that name
       already exists (key integrity error).
    2) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Pino name
    :return: a HTTP response object (204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_pino(request_data):
        try:
            Pino.update_pino(name, request_data['name'])
            response = Response("", status=204,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/pinos/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidpinoerrormsg = {
                "error": str(type(exc)),
                "helpString": PINOPATCHHELPSTRING
            }
            response = Response(json.dumps(invalidpinoerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidpinoerrormsg = {
        "error": PINOMALFORMEDDOCUMENT.format(request_data),
        "helpString": PINOPATCHHELPSTRING
    }
    response = Response(json.dumps(invalidpinoerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/pinos/<string:name>', methods=['DELETE'])
def delete_pino(name):
    """
    HTTP DELETE method for Pino model
    :param name: query parameter: pino name.
    :return: a HTTP response object
    """
    if Pino.delete_pino(name):
        return Response("", status=204)

    invalidpinoerrormsg = {
        "error": PINONOTFOUND.format(name)
    }
    return Response(invalidpinoerrormsg, status=404,
                    mimetype='application/json')


@APP.route('/pinos/<string:name>/dvds', methods=['GET'])
def get_dvds_from_pino(name):
    """
    HTTP GET method for Dvd model
    :return: JSON object representing a list of all Dvds.
    """
    return jsonify({'dvds': Dvd.get_all_dvds_from_pino(pinoname=name)})


@APP.route('/pinos/<string:pino>/dvds/<string:name>/movies', methods=['GET'])
def get_movies_from_pino_dvd(pino, name):
    """
    HTTP GET method for Movie model
    :param pino: Pino name
    :param name: Dvd name
    :return: JSON object representing a list of all Dvds.
    """
    return jsonify({'movies': Movie.get_all_movies_from_dvd(name)})


# DVDS ########################################################################


@APP.route('/dvds', methods=['GET'])
@APP.route('/dvds/page/<int:page>', methods=['GET'])
def get_dvds(page=1):
    """
    HTTP GET method for Dvd model
    :return: JSON object representing a list of all Dvds.
    """
    return jsonify({'dvds': Dvd.get_all_dvds(page=page)})


@APP.route('/dvds/<string:name>', methods=['GET'])
def get_dvd_by_name(name):
    """
    HTTP GET method for Dvd model
    :param name: query parameter: pino name.
    :return: JSON object representing a Pino. Since 'name' is an unique key,
    the first (and only) matching pino. If Pino not found, a JSON Response with
    an error description + help string.
    """
    dvd = Dvd.get_dvd(name)
    if valid_dvd(dvd):
        return jsonify(dvd)

    invaliddvderrormsg = {
        "error": DVDNOTFOUND.format(name),
        "helpString": DVDHELPSTRING
    }
    response = Response(json.dumps(invaliddvderrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/dvds', methods=['POST'])
def post_dvd():
    """
    HTTP POST method for Dvd model
    :return: A JSON response with a header representing the newly created Dvd.
    """
    request_data = request.get_json(force=True)
    if valid_dvd(request_data):
        try:
            Dvd.add_dvd(request_data['name'], request_data['pino'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/dvds/" + str(request_data['name'])
            return response
        except Exception as exc:
            invaliddvderrormsg = {
                "error": str(type(exc)),
                "helpString": DVDHELPSTRING
            }
            response = Response(json.dumps(invaliddvderrormsg), status=409,
                                mimetype="application/json")
            return response

    invaliddvderrormsg = {
        "error": DVDMALFORMEDDOCUMENT.format(request_data),
        "helpString": DVDHELPSTRING
    }
    response = Response(json.dumps(invaliddvderrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/dvds/<string:name>', methods=['PUT'])
def put_dvd(name):
    """
    HTTP PUT method for Dvd model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the dvd resource do not exist?
       Dvd resource will be created (201).
    2) What happens if the dvd resource exists?
       Dvd resource will be updated (204). All cascade updates on the
       underlying database tables will also take place.
    3) What happens if the update conflicts with an existing Dvd?
       Dvd resource will not be updated because the new one already exists -
       conflicting state (409).
    4) What happens if the updated Pino does not exist?
       A Dvd resource must not create a Pino resource. This is a conflicting
       state (409).
    5) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Dvd name
    :return: a HTTP response object (201, 204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_dvd(request_data):
        try:
            httpcode = Dvd.put_dvd(
                name, request_data['name'], request_data['pino']
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/dvds/" + str(request_data['name'])
            return response
        except Exception as exc:
            invaliddvderrormsg = {
                "error": str(type(exc)),
                "helpString": DVDHELPSTRING
            }
            response = Response(json.dumps(invaliddvderrormsg), status=409,
                                mimetype="application/json")
            return response

    invaliddvderrormsg = {
        "error": DVDMALFORMEDDOCUMENT.format(request_data),
        "helpString": DVDHELPSTRING
    }
    response = Response(json.dumps(invaliddvderrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/dvds/<string:name>', methods=['PATCH'])
def patch_dvd(name):
    """
    HTTP PATCH method for Dvd model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No, hence this method
       returns 409 (Conflicting state) - would be pointing to a new resource
       that will not be created.
    2) What happens if the pino or dvd resource do not exist? This is just
       another way of stating assumption #1 - returns 409 (Conflicting state).
    3) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: the name of an existing Dvd resource
    :return:
    """
    request_data = request.get_json(force=True)
    if valid_dvd_for_patch(request_data):
        try:
            httpcode = Dvd.patch_dvd(name, request_data['pino'])
            response = Response("", status=httpcode,
                                mimetype="application/json")
            if httpcode == 204:
                response.headers['Location'] = "/dvds/" + name
            return response
        except Exception as exc:
            invaliddvderrormsg = {
                "error": str(type(exc)),
                "helpString": DVDPATCHHELPSTRING
            }
            response = Response(json.dumps(invaliddvderrormsg), status=409,
                                mimetype="application/json")
            return response

    invaliddvderrormsg = {
        "error": DVDMALFORMEDDOCUMENT.format(request_data),
        "helpString": DVDPATCHHELPSTRING
    }
    response = Response(json.dumps(invaliddvderrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/dvds/<string:name>', methods=['DELETE'])
def delete_dvd(name):
    """
    HTTP DELETE method for Dvd model
    :param name: query parameter: dvd name.
    :return: JSON object representing a Dvd. Since 'name' is an unique key,
    the first (and only) matching pino. If Dvd not found, a JSON Response with
    an error description + help string.
    """
    if Dvd.delete_dvd(name):
        return Response("", status=204)

    invaliddvderrormsg = {
        "error": DVDNOTFOUND.format(name)
    }
    return Response(invaliddvderrormsg, status=404,
                    mimetype='application/json')


# MOVIES ######################################################################

@APP.route('/movies', methods=['GET'])
@APP.route('/movies/page/<int:page>', methods=['GET'])
def get_movies(page=1):
    """
    HTTP GET method for Movie model
    :return: JSON object representing a list of all Movies.
    """
    return jsonify({'movies': Movie.get_all_movies(page=page)})


@APP.route('/dvds/<string:name>/movies', methods=['GET'])
def get_movies_from_dvd(name):
    """
    HTTP GET method for Movie model
    :return: JSON object representing a list of all Dvds.
    """
    return jsonify({'movies': Movie.get_all_movies_from_dvd(name)})


@APP.route('/movies/<string:imdbid>', methods=['GET'])
def get_movie_by_imdbid(imdbid):
    """
    HTTP GET method for Movie model
    :param imdbid: query parameter: imdb movie id.
    :return: JSON object representing a Movie. Since 'imdbid' is an unique key,
    the first (and only) matching Movie. If Movie not found, a JSON Response
    with an error description + help string.
    """
    movie = Movie.get_movie(imdbid)
    if valid_movie_for_get(movie):
        return jsonify(movie)

    invalidmovieerrormsg = {
        "error": MOVIENOTFOUND.format(imdbid),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmovieerrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/actors', methods=['GET'])
def get_movie_actors(imdbid):
    """
    HTTP GET method for Movie model
    :param imdbid: query parameter: imdb movie id.
    :return: JSON object representing an array of MovieActor. If Movie not
    found, a JSON Response with an error description + help string.
    """
    movie = Movie.get_movie(imdbid)
    if valid_movie_for_get(movie):
        return jsonify({'actors': movie['actors']})

    invalidmovieactorserrormsg = {
        "error": MOVIENOTFOUND.format(imdbid),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmovieactorserrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/genres', methods=['GET'])
def get_movie_genres(imdbid):
    """
    HTTP GET method for Movie model
    :param imdbid: query parameter: imdb movie id.
    :return: JSON object representing an array of MovieGenres. If Movie not
    found, a JSON Response with an error description + help string.
    """
    movie = Movie.get_movie(imdbid)
    if valid_movie_for_get(movie):
        return jsonify({'genres': movie['genres']})

    invalidmoviegenreserrormsg = {
        "error": MOVIENOTFOUND.format(imdbid),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmoviegenreserrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/subtitles', methods=['GET'])
def get_movie_subtitles(imdbid):
    """
    HTTP GET method for Movie model
    :param imdbid: query parameter: imdb movie id.
    :return: JSON object representing an array of MovieSubtitles. If Movie not
    found, a JSON Response with an error description + help string.
    """
    movie = Movie.get_movie(imdbid)
    if valid_movie_for_get(movie):
        return jsonify({'subtitles': movie['subtitles']})

    invalidmoviegenreserrormsg = {
        "error": MOVIENOTFOUND.format(imdbid),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmoviegenreserrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/movies', methods=['POST'])
def post_movie():
    """
    HTTP POST method for Movie model
    :return: A JSON response with a header representing the newly created
    Movie.
    """
    request_data = request.get_json(force=True)
    if valid_movie_for_post(request_data):
        try:
            Movie.add_movie(request_data['name'], request_data['director'],
                            request_data['imdbid'], request_data['year'],
                            request_data['format'], request_data['dvd'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/movies/{}".format(
                    str(request_data['imdbid']))
            return response
        except Exception as exc:
            invalidmovieerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIEHELPSTRING
            }
            response = Response(json.dumps(invalidmovieerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidmovieerrormsg = {
        "error": MOVIEMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmovieerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/actors', methods=['POST'])
def post_movieactor(imdbid):
    """
    HTTP POST method for MovieActor model
    :return: A JSON response with a header representing the newly created
    MovieActor.
    """
    request_data = request.get_json(force=True)
    if valid_movieactor(request_data):
        try:
            MovieActor.add_movieactor(
                actorimdbid=request_data['actorimdbid'],
                movieimdbid=request_data['movieimdbid']
            )
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/movies/{}/actors".format(imdbid)
            return response
        except Exception as exc:
            invalidmovieactorerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIEACTORHELPSTRING
            }
            response = Response(
                json.dumps(invalidmovieactorerrormsg), status=409,
                mimetype="application/json")
            return response

    invalidmovieactorerrormsg = {
        "error": MOVIEACTORMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIEACTORHELPSTRING
    }
    response = Response(json.dumps(invalidmovieactorerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/genres', methods=['POST'])
def post_moviegenre(imdbid):
    """
    HTTP POST method for MovieGenre model
    :return: A JSON response with a header representing the newly created
    MovieGenre.
    """
    request_data = request.get_json(force=True)
    if valid_moviegenre(request_data):
        try:
            MovieGenre.add_moviegenre(
                genrename=request_data['genre'],
                movieimdbid=request_data['movieimdbid']
            )
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/movies/{}/genres".format(imdbid)
            return response
        except Exception as exc:
            invalidmoviegenreerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIEGENREHELPSTRING
            }
            response = Response(
                json.dumps(invalidmoviegenreerrormsg), status=409,
                mimetype="application/json")
            return response

    invalidmoviegenreerrormsg = {
        "error": MOVIEGENREMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIEGENREHELPSTRING
    }
    response = Response(json.dumps(invalidmoviegenreerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>/subtitles', methods=['POST'])
def post_moviesubtitle(imdbid):
    """
    HTTP POST method for MovieSubtitle model
    :return: A JSON response with a header representing the newly created
    MovieSubtitle.
    """
    request_data = request.get_json(force=True)
    if valid_moviesubtitle(request_data):
        try:
            MovieSubtitle.add_moviesubtitle(
                subtitlename=request_data['subtitle'],
                movieimdbid=request_data['movieimdbid']
            )
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/movies/{}/subtitles".format(imdbid)
            return response
        except Exception as exc:
            invalidmoviesubtitleerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIESUBTITLEHELPSTRING
            }
            response = Response(
                json.dumps(invalidmoviesubtitleerrormsg), status=409,
                mimetype="application/json")
            return response

    invalidmoviesubtitleerrormsg = {
        "error": MOVIESUBTITLEMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIESUBTITLEHELPSTRING
    }
    response = Response(json.dumps(invalidmoviesubtitleerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>', methods=['PUT'])
def put_movie(imdbid):
    """
    HTTP PUT method for Movie model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the movie resource do not exist?
       Dvd resource will be created (201).
    2) What happens if the movie resource exists?
       Dvd resource will be updated (204). All cascade updates on the
       underlying database tables will also take place.
    3) What happens if the dvd resource does not exist?
       Dvd resource will neither be created or updated because it would be in
       an inconsistent state (209).
    4) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param imdbid: Movie imdb id
    :return: a HTTP response object
    """
    request_data = request.get_json(force=True)
    if valid_movie_for_put(request_data):
        try:
            httpcode = Movie.put_movie(
                existingimdbid=imdbid,
                name=request_data['name'], director=request_data['director'],
                imdbid=request_data['imdbid'], year=request_data['year'],
                _format=request_data['format'], dvdname=request_data['dvd']
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/movies/" + str(request_data['imdbid'])
            return response
        except Exception as exc:
            invalidmovieerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIEHELPSTRING
            }
            response = Response(json.dumps(invalidmovieerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidmovieerrormsg = {
        "error": MOVIEMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIEHELPSTRING
    }
    response = Response(json.dumps(invalidmovieerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>', methods=['PATCH'])
def patch_movie(imdbid):
    """
    HTTP PATCH method for Movie model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No, hence this method
       returns 409 (Conflicting state) - would be pointing to a new resource
       that will not be created.
    2) What happens if the dvd resource do not exist? This is just
       another way of stating assumption #1 - returns 409 (Conflicting state).
    3) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param imdbid: the imdb id
    :return:
    """
    request_data = request.get_json(force=True)
    if valid_movie_for_patch(request_data):
        try:
            httpcode = Movie.patch_movie(
                existingimdbid=imdbid,
                name=request_data['name'] if 'name' in request_data else None,
                director=request_data['director'] if 'director' in request_data
                else None,
                newimdbid=request_data['imdb'] if 'imdb' in request_data
                else None,
                year=request_data['year'] if 'year' in request_data else None,
                _format=
                request_data['format'] if 'format' in request_data else None,
                dvdname=request_data['dvd'] if 'dvd' in request_data else None
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            if httpcode == 204:
                response.headers['Location'] = \
                    "/movies/" + imdbid
            return response
        except Exception as exc:
            invalidmovieerrormsg = {
                "error": str(type(exc)),
                "helpString": MOVIEPATCHHELPSTRING
            }
            response = Response(json.dumps(invalidmovieerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidmovieerrormsg = {
        "error": MOVIEMALFORMEDDOCUMENT.format(request_data),
        "helpString": MOVIEPATCHHELPSTRING
    }
    response = Response(json.dumps(invalidmovieerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/movies/<string:imdbid>', methods=['DELETE'])
def delete_movie(imdbid):
    """
    HTTP DELETE method for Movie model
    :param imdbid: query parameter: imdb id.
    :return: JSON object representing a Movie. Since 'name' is an unique key,
    the first (and only) matching pino. If Dvd not found, a JSON Response with
    an error description + help string.
    """
    if Movie.delete_movie(imdbid):
        return Response("", status=204)

    invalidmovieerrormsg = {
        "error": MOVIENOTFOUND.format(imdbid)
    }
    return Response(invalidmovieerrormsg, status=404,
                    mimetype='application/json')


# ACTORS ######################################################################

@APP.route('/actors', methods=['GET'])
@APP.route('/actors/page/<int:page>', methods=['GET'])
def get_actors(page=1):
    """
    HTTP GET method for Actor model
    :return: JSON object representing a list of all Actors.
    """
    return jsonify({'actors': Actor.get_all_actors(page=page)})


@APP.route('/actors/<string:imdbid>', methods=['GET'])
def get_actor_by_imdbid(imdbid):
    """
    HTTP GET method for Actor model
    :param imdbid: query parameter: imdb actor id.
    :return: JSON object representing an Actor. Since 'imdbid' is an unique
    key, the first (and only) matching Actor. If Actor not found, a JSON
    Response with an error description + help string.
    """
    actor = Actor.get_actor(imdbid)
    if valid_actor(actor):
        return jsonify(actor)

    invalidactorerrormsg = {
        "error": ACTORNOTFOUND.format(imdbid),
        "helpString": ACTORHELPSTRING
    }
    response = Response(json.dumps(invalidactorerrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/actors', methods=['POST'])
def post_actor():
    """
    HTTP POST method for Actor model
    :return: A JSON response with a header representing the newly created
    Actor.
    """
    request_data = request.get_json(force=True)
    if valid_actor(request_data):
        try:
            Actor.add_actor(request_data['name'],
                            request_data['imdbid'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/actors/{}".format(
                    str(request_data['imdbid']))
            return response
        except Exception as exc:
            invalidactorerrormsg = {
                "error": str(type(exc)),
                "helpString": ACTORHELPSTRING
            }
            response = Response(json.dumps(invalidactorerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidactorerrormsg = {
        "error": ACTORMALFORMEDDOCUMENT.format(request_data),
        "helpString": ACTORHELPSTRING
    }
    response = Response(json.dumps(invalidactorerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/actors/<string:imdbid>', methods=['PUT'])
def put_actor(imdbid):
    """
    HTTP PUT method for Actor model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the Actor resource do not exist?
       Pino resource will be created (201).
    2) What happens if the Actor resource exists?
       Pino resource will be updated (204).
    3) What happens if the update conflicts with an existing Actor?
       Pino resource will not be updated because the new one already exists -
       conflicting state (409).
    4) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param imdbid: Actor's iMDB id
    :return: a HTTP response object (201, 204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_actor_for_put(request_data):
        try:
            httpcode = Actor.put_actor(
                name=request_data['name'],
                existingimdbid=imdbid,
                newimdbid=request_data['imdbid']
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/actors/" + str(request_data['imdbid'])
            return response
        except Exception as exc:
            invalidactorerrormsg = {
                "error": str(type(exc)),
                "helpString": ACTORHELPSTRING
            }
            response = Response(json.dumps(invalidactorerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidactorerrormsg = {
        "error": ACTORMALFORMEDDOCUMENT.format(request_data),
        "helpString": ACTORHELPSTRING
    }
    response = Response(json.dumps(invalidactorerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/actors/<string:imdbid>', methods=['PATCH'])
def patch_actor(imdbid):
    """
    HTTP PATCH method for Actor model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No, hence this method
       returns 409 (Conflicting state) - would be pointing to a new resource
       that will not be created.
    2) What happens if the actor resource do not exist? This is just
       another way of stating assumption #1 - returns 409 (Conflicting state).
    3) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param imdbid: the imdb id
    :return:
    """
    request_data = request.get_json(force=True)
    if valid_actor_for_patch(request_data):
        try:
            httpcode = Actor.patch_actor(
                existingimdbid=imdbid,
                name=request_data['name'] if 'name' in request_data
                else None,
                newimdbid=request_data['imdbid'] if 'imdbid' in request_data
                else None
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            if httpcode == 204:
                response.headers['Location'] = \
                    "/actors/" + imdbid
            return response
        except Exception as exc:
            invalidactorerrormsg = {
                "error": str(type(exc)),
                "helpString": ACTORPATCHHELPSTRING
            }
            response = Response(json.dumps(invalidactorerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidactorerrormsg = {
        "error": ACTORMALFORMEDDOCUMENT.format(request_data),
        "helpString": ACTORPATCHHELPSTRING
    }
    response = Response(json.dumps(invalidactorerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/actors/<string:imdbid>', methods=['DELETE'])
def delete_actor(imdbid):
    """
    HTTP DELETE method for Actor model
    :param imdbid: query parameter: imdb id.
    :return: JSON object representing a Actor. Since 'imdbid' is an unique key,
    the first (and only) matching actor. If Actor not found, a JSON Response
    with an error description + help string.
    """
    if Actor.delete_actor(imdbid):
        return Response("", status=204)

    invalidactorerrormsg = {
        "error": ACTORNOTFOUND.format(imdbid)
    }
    return Response(invalidactorerrormsg, status=404,
                    mimetype='application/json')


# GENRES ######################################################################

@APP.route('/genres', methods=['GET'])
@APP.route('/genres/page/<int:page>', methods=['GET'])
def get_genres(page=1):
    """
    HTTP GET method for Genre model
    :return: JSON object representing a list of all Genres.
    """
    return jsonify({'genres': Genre.get_all_genres(page=page)})


@APP.route('/genres/<string:name>', methods=['GET'])
def get_genre_by_name(name):
    """
    HTTP GET method for Actor model
    :param name: Genre's description.
    :return: JSON object representing a Genre. Since 'name' is an unique
    key, the first (and only) matching Genre. If Genre not found, a JSON
    Response with an error description + help string.
    """
    genre = Genre.get_genre(name)
    if valid_genre(genre):
        return jsonify(genre)

    invalidgenreerrormsg = {
        "error": GENRENOTFOUND.format(name),
        "helpString": GENREHELPSTRING
    }
    response = Response(json.dumps(invalidgenreerrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/genres', methods=['POST'])
def post_genre():
    """
    HTTP POST method for Genre model
    :return: A JSON response with a header representing the newly created
    Genre.
    """
    request_data = request.get_json(force=True)
    if valid_genre(request_data):
        try:
            Genre.add_genre(request_data['name'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/genres/{}".format(
                    str(request_data['name']))
            return response
        except Exception as exc:
            invalidgenreerrormsg = {
                "error": str(type(exc)),
                "helpString": GENREHELPSTRING
            }
            response = Response(json.dumps(invalidgenreerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidgenreerrormsg = {
        "error": GENREMALFORMEDDOCUMENT.format(request_data),
        "helpString": GENREHELPSTRING
    }
    response = Response(json.dumps(invalidgenreerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/genres/<string:name>', methods=['PUT'])
def put_genre(name):
    """
    HTTP PUT method for Genre model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the Genre resource do not exist?
       Genre resource will be created (201).
    2) What happens if the Genre resource exists?
       Genre resource will be updated (204).
    3) What happens if the update conflicts with an existing Genre?
       Genre resource will not be updated because the new one already exists -
       conflicting state (409).
    4) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Genre name
    :return: a HTTP response object (201, 204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_genre(request_data):
        try:
            httpcode = Genre.put_genre(name, request_data['name'])
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/genres/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidgenreerrormsg = {
                "error": str(type(exc)),
                "helpString": GENREHELPSTRING
            }
            response = Response(json.dumps(invalidgenreerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidgenreerrormsg = {
        "error": GENREMALFORMEDDOCUMENT.format(request_data),
        "helpString": GENREHELPSTRING
    }
    response = Response(json.dumps(invalidgenreerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/genres/<string:name>', methods=['PATCH'])
def patch_genre(name):
    """
    HTTP PATCH method for Genre model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No, hence this method
       returns 409 (Conflicting state) - would be pointing to a new resource
       that will not be created.
    2) What happens if the actor resource do not exist? This is just
       another way of stating assumption #1 - returns 409 (Conflicting state).
    3) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Genre name
    :return:
    """
    request_data = request.get_json(force=True)
    if valid_genre(request_data):
        try:
            httpcode = Genre.patch_genre(
                name=name,
                newname=request_data['name']
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            if httpcode == 204:
                response.headers['Location'] = \
                    "/genres/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidgenreerrormsg = {
                "error": str(type(exc)),
                "helpString": GENREPATCHHELPSTRING
            }
            response = Response(json.dumps(invalidgenreerrormsg), status=409,
                                mimetype="application/json")
            return response

    invalidgenreerrormsg = {
        "error": GENREMALFORMEDDOCUMENT.format(request_data),
        "helpString": GENREPATCHHELPSTRING
    }
    response = Response(json.dumps(invalidgenreerrormsg), status=400,
                        mimetype="application/json")
    return response


# SUBTITLES ###################################################################

@APP.route('/subtitles', methods=['GET'])
@APP.route('/subtitles/page/<int:page>', methods=['GET'])
def get_subtitles(page=1):
    """
    HTTP GET method for Subtitles model
    :return: JSON object representing a list of all Subtitles.
    """
    return jsonify({'subtitles': Subtitle.get_all_subtitles(page=page)})


@APP.route('/subtitles/<string:name>', methods=['GET'])
def get_subtitle_by_name(name):
    """
    HTTP GET method for Subtitle model
    :param name: Subtitle's description.
    :return: JSON object representing a Subtitle. Since 'name' is an unique
    key, the first (and only) matching Subtitle. If Subtitle not found, a JSON
    Response with an error description + help string.
    """
    subtitle = Subtitle.get_subtitle(name)
    if valid_subtitle(subtitle):
        return jsonify(subtitle)

    invalidsubtitleerrormsg = {
        "error": SUBTITLENOTFOUND.format(name),
        "helpString": SUBTITLEHELPSTRING
    }
    response = Response(json.dumps(invalidsubtitleerrormsg), status=404,
                        mimetype="application/json")
    return response


@APP.route('/subtitles', methods=['POST'])
def post_subtitle():
    """
    HTTP POST method for Subtitle model
    :return: A JSON response with a header representing the newly created
    Subtitle.
    """
    request_data = request.get_json(force=True)
    if valid_subtitle(request_data):
        try:
            Subtitle.add_subtitle(request_data['name'])
            response = Response("", status=201, mimetype="application/json")
            response.headers['Location'] = \
                "/subtitles/{}".format(
                    str(request_data['name']))
            return response
        except Exception as exc:
            invalidsubtitleerrormsg = {
                "error": str(type(exc)),
                "helpString": SUBTITLEHELPSTRING
            }
            response = Response(
                json.dumps(invalidsubtitleerrormsg), status=409,
                mimetype="application/json"
            )
            return response

    invalidsubtitleerrormsg = {
        "error": SUBTITLEMALFORMEDDOCUMENT.format(request_data),
        "helpString": SUBTITLEHELPSTRING
    }
    response = Response(json.dumps(invalidsubtitleerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/subtitles/<string:name>', methods=['PUT'])
def put_subtitle(name):
    """
    HTTP PUT method for Subtitle model
    From https://tools.ietf.org/html/rfc7231#section-4.3.4:

    "The PUT method requests that the state of the target resource be
    created or replaced with the state defined by the representation
    enclosed in the request message payload.
    If the target resource does not have a current representation and the
    PUT successfully creates one, then the origin server MUST inform the
    user agent by sending a 201 (Created) response.  If the target
    resource does have a current representation and that representation
    is successfully modified in accordance with the state of the enclosed
    representation, then the origin server MUST send either a 200 (OK) or
    a 204 (No Content) response to indicate successful completion of the
    request."

    Assumptions:
    1) What happens if the Subtitle resource do not exist?
       Subtitle resource will be created (201).
    2) What happens if the Subtitle resource exists?
       Subtitle resource will be updated (204).
    3) What happens if the update conflicts with an existing Subtitle?
       Subtitle resource will not be updated because the new one already
       exists - conflicting state (409).
    4) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Subtitle name
    :return: a HTTP response object (201, 204, 400 or 409)
    """
    request_data = request.get_json(force=True)
    if valid_subtitle(request_data):
        try:
            httpcode = Subtitle.put_subtitle(name, request_data['name'])
            response = Response("", status=httpcode,
                                mimetype="application/json")
            response.headers['Location'] = \
                "/subtitles/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidsubtitleerrormsg = {
                "error": str(type(exc)),
                "helpString": SUBTITLEHELPSTRING
            }
            response = Response(
                json.dumps(invalidsubtitleerrormsg), status=409,
                mimetype="application/json"
            )
            return response

    invalidsubtitleerrormsg = {
        "error": SUBTITLEMALFORMEDDOCUMENT.format(request_data),
        "helpString": SUBTITLEHELPSTRING
    }
    response = Response(json.dumps(invalidsubtitleerrormsg), status=400,
                        mimetype="application/json")
    return response


@APP.route('/subtitles/<string:name>', methods=['PATCH'])
def patch_subtitle(name):
    """
    HTTP PATCH method for Subtitle model
    Assumptions from https://tools.ietf.org/html/rfc5789:
    1) This PATCH method MAY create new resources? No, hence this method
       returns 409 (Conflicting state) - would be pointing to a new resource
       that will not be created.
    2) What happens if the Subtitle resource do not exist? This is just
       another way of stating assumption #1 - returns 409 (Conflicting state).
    3) What happens if the JSON request data has unexpected values or missing
       keys? This method returns 400 (Malformed patch document).

    :param name: Subtitle name
    :return:
    """
    request_data = request.get_json(force=True)
    if valid_subtitle(request_data):
        try:
            httpcode = Subtitle.patch_subtitle(
                name=name,
                newname=request_data['name']
            )
            response = Response("", status=httpcode,
                                mimetype="application/json")
            if httpcode == 204:
                response.headers['Location'] = \
                    "/subtitles/" + str(request_data['name'])
            return response
        except Exception as exc:
            invalidsubtitleerrormsg = {
                "error": str(type(exc)),
                "helpString": SUBTITLEPATCHHELPSTRING
            }
            response = Response(
                json.dumps(invalidsubtitleerrormsg), status=409,
                mimetype="application/json"
            )
            return response

    invalidsubtitleerrormsg = {
        "error": SUBTITLEMALFORMEDDOCUMENT.format(request_data),
        "helpString": SUBTITLEPATCHHELPSTRING
    }
    response = Response(json.dumps(invalidsubtitleerrormsg), status=400,
                        mimetype="application/json")
    return response


def main():
    """
    Application entrypoint
    :return: int ErrorCode or 0 (Success)
    """
    APP.run(host='0.0.0.0')


if __name__ == '__main__':
    main()
