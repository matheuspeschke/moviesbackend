# coding=UTF-8
"""
Module: object validations
"""
MOVIEGET = {
    'imdb': '',
    'imdbid': '',
    'dvd': '',
    'name': '',
    'format': '',
    'year': '',
    'director': '',
    'actors': [],
    'genres': [],
    'subtitles': []
}

MOVIEPOST = {
    'imdbid': '',
    'dvd': '',
    'name': '',
    'format': '',
    'year': '',
    'director': '',
    'actors': [],
    'genres': [],
    'subtitles': []
}

MOVIEPUT = {
    'imdbid': '',
    'dvd': '',
    'name': '',
    'format': '',
    'year': '',
    'director': '',
    'actors': [],
    'genres': [],
    'subtitles': []
}

MOVIEPATCH = {
    'imdb': '',
    'dvd': '',
    'name': '',
    'format': '',
    'year': '',
    'director': '',
    'actors': [],
    'genres': [],
    'subtitles': []
}

ACTORPATCH = ACTORPUT = ACTOR = {
    'imdbid': '',
    'name': ''
}

MOVIEACTOR = {
    'actorimdbid': '',
    'movieimdbid': ''
}

MOVIEGENRE = {
    'genre': '',
    'movieimdbid': ''
}

MOVIESUBTITLE = {
    'subtitle': '',
    'movieimdbid': ''
}


def valid_subtitle(subtitle):
    """
    Validates subtitle as a correct dictionary
    :param subtitle: a python dictionary
    :return: True or False
    """
    return isinstance(subtitle, dict) and "name" in subtitle


def valid_genre(genre):
    """
    Validates genre as a correct dictionary
    :param genre: a python dictionary
    :return: True or False
    """
    return isinstance(genre, dict) and "name" in genre


def valid_pino(pino):
    """
    Validates pino as a correct dictionary
    :param pino: a python dictionary
    :return: True or False
    """
    return isinstance(pino, dict) and "name" in pino


def valid_dvd(dvd):
    """
    Validates dvd as a correct dictionary
    :param dvd: a python dictionary
    :return: True or False
    """
    return isinstance(dvd, dict) and "name" in dvd and "pino" in dvd


def valid_dvd_for_patch(dvd):
    """
    The Dvd API does not permit to change the Dvd name, which is the primary
    key for other API resources - potentially creating orphaned records. Thus
    only the pino field can be changed.
    :param dvd: a python dictionary
    :return: True or False
    """
    return isinstance(dvd, dict) and "pino" in dvd \
        and len(dvd) == 1


def valid_model(model, requestdata):
    """
    Validates a model representation as a Python dictionary.
    This templates serves for the following scenario: ALL dict keys MUST be
    in the request data.
    :param requestdata: a python dictionary
    :param model: a python dictionary
    :return: True or False
    """
    valid = True

    if isinstance(requestdata, dict):
        for key in requestdata:
            if key not in model:
                valid = False
                break
    else:
        return False

    return valid and len(requestdata) == len(model)


def valid_model_for_patch(model, requestdata):
    """
    Validates movie as a correct dictionary
    :param requestdata: a python dictionary
    :param model: a python dictionary
    :return: True or False
    """
    valid = True

    if isinstance(requestdata, dict):
        for key in requestdata:
            if key not in model:
                valid = False
                break
    else:
        return False

    return valid and len(requestdata) > 0


def valid_movie_for_get(requestdata):
    """
    Validates movie as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIEGET, requestdata)


def valid_movie_for_post(requestdata):
    """
    Validates movie as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIEPOST, requestdata)


def valid_movie_for_put(requestdata):
    """
    Validates movie as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIEPUT, requestdata)


def valid_movie_for_patch(requestdata):
    """
    Validates:
    1. movie is a dictionary
    2. movie has only valid keys for a Movie instance
    3. movie has the imdbid key and at least one other key
    :param requestdata: a request object (json)
    :return: True or False
    """
    return valid_model_for_patch(MOVIEPATCH, requestdata)


def valid_actor(actor):
    """
    Validates actor as a correct dictionary
    :param actor: a python dictionary
    :return: True or False
    """
    return isinstance(actor, dict) and "name" in actor and "imdbid" in actor


def valid_actor_for_put(requestdata):
    """
    Validates actor as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(ACTORPUT, requestdata)


def valid_actor_for_patch(requestdata):
    """
    Validates actor as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model_for_patch(ACTORPATCH, requestdata)


def valid_movieactor(requestdata):
    """
    Validates movie actor as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIEACTOR, requestdata)


def valid_moviegenre(requestdata):
    """
    Validates movie genre as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIEGENRE, requestdata)


def valid_moviesubtitle(requestdata):
    """
    Validates movie subtitle as a correct dictionary
    :param requestdata: a python dictionary
    :return: True or False
    """
    return valid_model(MOVIESUBTITLE, requestdata)
