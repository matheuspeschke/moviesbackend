# coding=UTF-8
"""
Module: Data marshalling from an underlying storage provider to internal usage.
"""
import json
import flask_sqlalchemy
from sqlalchemy import ForeignKey
from moviesbackend.settings import APP
from moviesbackend.exceptions import ExceptionAPIResourceNotFound

DB = flask_sqlalchemy.SQLAlchemy(APP)

IMDBTITLEURL = 'https://www.imdb.com/title/{}'
IMDBACTORURL = 'https://www.imdb.com/name/nm{}'
PINONOTFOUND = "Pino '{}' not found."
DVDNOTFOUND = "Dvd '{}' not found."
MOVIENOTFOUND = "Movie '{}' not found."
ACTORNOTFOUND = "Actor '{}' not found."
GENRENOTFOUND = "Genre '{}' not found."
SUBTITLENOTFOUND = "Subtitle '{}' not found."


class Pino(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Pino data as
    a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_pino'
    id = DB.Column(DB.Integer, primary_key=True, autoincrement="auto")
    name = DB.Column(DB.String(8), nullable=False, unique=True, index=True)
    dvds = DB.relationship('Dvd', backref='pino', lazy=True)

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Pino data.
        """
        if not isinstance(self, type(None)):
            return {'name': self.name}

        return {}

    @staticmethod
    def add_pino(name):
        """
        Persists Pino data as a record in a database table.
        :param name: name of the new Pino
        :return: None
        """
        new_pino = Pino(name=name)
        DB.session.add(new_pino)
        DB.session.commit()

    @staticmethod
    def get_all_pinos(page):
        """
        Retrieve all Pino records from the database table.
        :return: a list of Python dictionaries representing all Pinos.
        """
        return [
            Pino.dict(pino) for pino in Pino.query.order_by(
                Pino.name.asc()
            ).paginate(page, per_page=APP.config['PINOS_PER_PAGE']).items
        ]

    @staticmethod
    def get_pino(name):
        """
        Retrieve the Pino record filtered by the parameter _name from the
        database table.
        :return: a Python dictionary representing an unique Pino.
        """
        return Pino.dict(Pino.query.filter_by(name=name).first())

    @staticmethod
    def update_pino(existingname, name):
        """
        Update or create the Pino resource on the database.
        :param existingname: existing Pino name
        :param name: update Pino name
        :return: HTTP Code 204 if updated, 201 if created.
        """
        httpcode = 204
        pino = Pino.query.filter_by(name=existingname).first()
        if pino is None:
            httpcode = 201
            pino = Pino(name=name)
            DB.session.add(pino)
        else:
            pino.name = name

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_pino(existingname, name):
        """
        Update the Pino resource on the database.
        :param existingname: existing Pino name
        :param name: update Pino name
        :return: HTTP Code 204 if updated.
        """
        httpcode = 204
        pino = Pino.query.filter_by(name=existingname).first()
        if pino is None:
            return 404

        pino.name = name

        DB.session.commit()
        return httpcode

    @staticmethod
    def delete_pino(name):
        """
        Delete the Pino in the database.
        :param name: name of the Pino (unique)
        :return: True (deleted) or False (not found)
        """
        okreturn = Pino.query.filter_by(name=name).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        return json.dumps(self.dict())


class Dvd(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Dvd data as
    a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_dvd'
    id = DB.Column(DB.Integer, primary_key=True, autoincrement="auto")
    name = DB.Column(DB.String(8), nullable=False, unique=True, index=True)
    pino_id = DB.Column(DB.Integer, ForeignKey('movies_pino.id'))
    movies = DB.relationship('Movie', backref='dvd', lazy=True)

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Pino data.
        """
        if not isinstance(self, type(None)) and \
                not isinstance(self.pino, type(None)):
            return {'name': self.name, 'pino': self.pino.name}

        return {}

    @staticmethod
    def add_dvd(name, pinoname):
        """
        Persists Dvd data as a record in a database table.
        :param name: Dvd name
        :param pinoname: Pino name
        :return: None
        """
        pino = Pino.query.filter_by(name=pinoname).first()
        if pino is None:
            raise ExceptionAPIResourceNotFound(
                PINONOTFOUND.format(pinoname)
            )
        dvd = Dvd(name=name, pino_id=pino.id)

        DB.session.add(dvd)
        DB.session.commit()

    @staticmethod
    def get_all_dvds(page):
        """
        Retrieve all Dvd records from the database table.
        :return: a list of dictionaries representing all available Dvds.
        """
        return [
            Dvd.dict(dvd) for dvd in Dvd.query.order_by(
                Dvd.name.asc()
            ).paginate(page, per_page=APP.config['DVDS_PER_PAGE']).items
        ]

    @staticmethod
    def get_all_dvds_from_pino(pinoname):
        """
        Retrieve all Dvd records from the database table.
        :param pinoname: Pino name that contains the Dvd
        :return: a list of dictionaries representing all available Dvds in the
        Pino.
        """
        pino = Pino.query.filter_by(name=pinoname).first()
        if pino is not None:
            return [Dvd.dict(dvd) for dvd in pino.dvds]

        return []

    @staticmethod
    def get_dvd(name):
        """
        Retrieve the Dvd record filtered by the parameter _name from the
        database table.
        :return: a Python dictionary representing an unique Dvd.
        """
        return Dvd.dict(Dvd.query.filter_by(name=name).first())

    @staticmethod
    def put_dvd(dvdname, newdvdname, pinoname):
        """
        Update or create the Dvd information on the database.
        :param dvdname: old Dvd name
        :param newdvdname: new Dvd name
        :param pinoname: Pino name
        :return: HTTP Code 201 if Dvd resource created, 204 if updated.
        """
        httpcode = 204
        pino = Pino.query.filter_by(name=pinoname).first()
        if pino is None:
            raise ExceptionAPIResourceNotFound(
                PINONOTFOUND.format(pinoname)
            )

        dvd = Dvd.query.filter_by(name=dvdname).first()
        if dvd is None:
            httpcode = 201
            dvd = Dvd(name=newdvdname, pino_id=pino.id)
            DB.session.add(dvd)
        else:
            dvd.name = newdvdname

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_dvd(dvdname, pinoname):
        """
        Update the Dvd information on the database.
        :param dvdname: Dvd name
        :param pinoname: new Pino name
        :return: None
        """
        dvd = Dvd.query.filter_by(name=dvdname).first()
        if dvd is None:
            return 404
        pino = Pino.query.filter_by(name=pinoname).first()
        if pino is None:
            raise ExceptionAPIResourceNotFound(
                PINONOTFOUND.format(pinoname)
            )
        dvd.pino_id = pino.id
        DB.session.commit()
        return 204

    @staticmethod
    def delete_dvd(name):
        """
        Delete the Dvd in the database.
        :param name: name of the Dvd (unique)
        :return: True (deleted) or False (not found)
        """
        okreturn = Dvd.query.filter_by(name=name).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        dvd = Dvd.query.filter_by(name=self.name).first()
        pino = Pino.query.filter_by(id=dvd.pino_id).first()
        dvd_object = {
            'name': dvd.name,
            'pino': pino.name
        }
        return json.dumps(dvd_object)


class MovieActor(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Actors in
    a Movie data as a Python dictionary.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_movie_actors'
    id = DB.Column(DB.Integer, primary_key=True, autoincrement="auto")
    movie_id = DB.Column(DB.Integer, ForeignKey('movies_movie.id'))
    actor_id = DB.Column(DB.Integer, ForeignKey('movies_actor.id'))

    @staticmethod
    def add_movieactor(actorimdbid, movieimdbid):
        """
        Persists Movie Actor data as a record in a database table.
        :param actorimdbid: iMDB actor id
        :param movieimdbid: iMDB movie id
        :return: None
        """
        actor = Actor.query.filter_by(imdbid=actorimdbid).first()
        if actor is None:
            raise ExceptionAPIResourceNotFound(
                ACTORNOTFOUND.format(actorimdbid)
            )
        movie = Movie.query.filter_by(imdb=movieimdbid).first()
        if movie is None:
            raise ExceptionAPIResourceNotFound(
                MOVIENOTFOUND.format(movieimdbid)
            )
        movieactor = MovieActor(movie_id=movie.id, actor_id=actor.id)
        DB.session.add(movieactor)
        DB.session.commit()


class Movie(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Movie data as
    a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_movie'
    id = DB.Column(DB.Integer, primary_key=True, autoincrement="auto")
    name = DB.Column(DB.String(100), nullable=False, unique=False, index=True)
    director = DB.Column(DB.String(215), nullable=False, unique=False,
                         index=True)
    imdb = DB.Column(DB.String(12), nullable=False, unique=True, index=True)
    year = DB.Column(DB.String(4), nullable=False, unique=False, index=False)
    format = DB.Column(DB.String(100), nullable=False, unique=False,
                       index=False)
    dvd_id = DB.Column(DB.Integer, ForeignKey('movies_dvd.id'))
    movieactors = DB.relationship('MovieActor', backref='movie', lazy=True)
    moviegenres = DB.relationship('MovieGenre', backref='movie', lazy=True)
    moviesubtitles = DB.relationship(
        'MovieSubtitle', backref='movie', lazy=True
    )

    @staticmethod
    def filtermovieactor(movieactor):
        """
        Creates a dictionary from a MovieActor model object.
        :param movieactor: MovieActor model object
        :return: A dictionary
        """
        dictionary = movieactor.actor.dict()
        return {'name': dictionary['name'], 'imdb': dictionary['imdb']}

    @staticmethod
    def filtermoviegenre(moviegenre):
        """
        Creates a dictionary from a MovieGenre model object.
        :param moviegenre: MovieGenre model object
        :return: A dictionary
        """
        dictionary = moviegenre.genre.dict()
        return {'name': dictionary['name']}

    @staticmethod
    def filtermoviesubtitle(moviesubtitle):
        """
        Creates a dictionary from a MovieSubtitle model object.
        :param moviesubtitle: MovieSubtitle model object
        :return: A dictionary
        """
        dictionary = moviesubtitle.subtitle.dict()
        return {'name': dictionary['name']}

    def get_movieactors(self):
        """
        Creates an array (list) of MovieActor dictionaries.
        :return: Array (list) of MovieActor dictionaries.
        """
        return [self.filtermovieactor(movieactor) for movieactor in
                self.movieactors
                ]

    def get_moviegenres(self):
        """
        Creates an array (list) of MovieGenre dictionaries.
        :return: Array (list) of MovieGenre dictionaries.
        """
        return [self.filtermoviegenre(moviegenres) for moviegenres in
                self.moviegenres
                ]

    def get_moviesubtitles(self):
        """
        Creates an array (list) of MovieSubtitle dictionaries.
        :return: Array (list) of MovieSubtitle dictionaries.
        """
        return [self.filtermoviesubtitle(moviesubtitles) for moviesubtitles in
                self.moviesubtitles
                ]

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Movie data.
        """
        if not isinstance(self, type(None)) and \
                not isinstance(self.dvd, type(None)):
            return {'name': self.name, 'director': self.director,
                    'imdb': IMDBTITLEURL.format(self.imdb), 'year': self.year,
                    'format': self.format, 'dvd': self.dvd.name,
                    'imdbid': self.imdb,
                    'actors': self.get_movieactors(),
                    'genres': self.get_moviegenres(),
                    'subtitles': self.get_moviesubtitles()
                    }

        return {}

    @staticmethod
    def add_movie(name, director, imdbid, year, _format, dvdname):
        """
        Persists Movie data as a record in a database table.
        :param name: Dvd name
        :param director: Movie's director
        :param imdbid: iMDB movie id of the film
        :param year: the year this film was released
        :param _format: digital audio format (e.g. MP4, MKV, etc)
        :param dvdname: Dvd name
        :return: None
        """
        dvd = Dvd.query.filter_by(name=dvdname).first()
        if dvd is None:
            raise ExceptionAPIResourceNotFound(
                DVDNOTFOUND.format(dvdname)
            )
        movie = Movie(name=name, director=director, imdb=imdbid, year=year,
                      format=_format, dvd_id=dvd.id)
        DB.session.add(movie)
        DB.session.commit()

    @staticmethod
    def get_all_movies(page):
        """
        Retrieve all Movies records from the database table.
        :return: a list of dictionaries representing all available Movies.
        """
        return [
            Movie.dict(movie) for movie in Movie.query.order_by(
                Movie.name.asc()
            ).paginate(page, per_page=APP.config['MOVIES_PER_PAGE']).items
        ]

    @staticmethod
    def get_all_movies_from_dvd(dvdname):
        """
        Retrieve all Movie records from the database table.
        :param dvdname: Dvd name that contains the Movie
        :return: a list of dictionaries representing all available Movies in
        the Dvd.
        """
        dvd = Dvd.query.filter_by(name=dvdname).first()
        if dvd is not None:
            return [Movie.dict(movie) for movie in dvd.movies]

        return [Movie.dict(Movie())]

    @staticmethod
    def get_movie(imdbid):
        """
        Retrieve the Movie record filtered by the parameter _name from the
        database table.
        :param imdbid: iMDB movie id of the film
        :return: a Python dictionary representing an unique Movie.
        """
        return Movie.dict(Movie.query.filter_by(imdb=imdbid).first())

    @staticmethod
    def put_movie(
            existingimdbid, name, director, imdbid, year, _format, dvdname
    ):
        """
        Update or create the Movie information on the database.
        :param name: Movie's title
        :param director: Director's name
        :param existingimdbid: Movie's imdb id
        :param imdbid: new Movie's imdb id
        :param year: movie's release year
        :param _format: movie's format
        :param dvdname: Dvd's name
        :return: HTTP Code 201 if Movie resource created, 204 if updated.
        """
        dvd = Dvd.query.filter_by(name=dvdname).first()
        if dvd is None:
            raise ExceptionAPIResourceNotFound(
                DVDNOTFOUND.format(dvdname)
            )

        httpcode = 204
        movie = Movie.query.filter_by(imdb=existingimdbid).first()
        if movie is None:
            httpcode = 201
            movie = Movie(
                imdb=imdbid,
                name=name,
                dvd_id=dvd.id,
                director=director,
                year=year,
                format=_format
                )
            DB.session.add(movie)
        else:
            movie.imdb = imdbid
            movie.director = director
            movie.format = _format
            movie.name = name
            movie.dvd_id = dvd.id
            movie.year = year

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_movie(
            existingimdbid, name, director, newimdbid, year, _format, dvdname):
        """
        Update the Dvd information on the database.
        :param name: Movie's title
        :param director: Director's name
        :param existingimdbid: Movie's imdb id
        :param newimdbid: new Movie's imdb id
        :param year: movie's release year
        :param _format: movie's format
        :param dvdname: Dvd's name
        :return: None
        """
        httpcode = 404
        movie = Movie.query.filter_by(imdb=existingimdbid).first()
        if movie is None:
            return httpcode
        if dvdname is not None:
            dvd = Dvd.query.filter_by(name=dvdname).first()
            if dvd is None:
                raise ExceptionAPIResourceNotFound(
                    DVDNOTFOUND.format(dvdname)
                )
            movie.dvd_id = dvd.id

        if newimdbid is not None:
            movie.imdb = newimdbid
        if director is not None:
            movie.director = director
        if _format is not None:
            movie.format = _format
        if name is not None:
            movie.name = name
        if year is not None:
            movie.year = year
        httpcode = 204

        DB.session.commit()
        return httpcode

    @staticmethod
    def delete_movie(imdbid):
        """
        Delete the Movie from the database.
        :param imdbid: iMDB movie id of the film
        :return: True (deleted) or False (not found)
        """
        okreturn = Movie.query.filter_by(imdb=imdbid).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        return json.dumps(self.dict())


class Actor(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Actor data as
    a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_actor'
    id = DB.Column(DB.Integer, primary_key=True, autoincrement="auto")
    imdbid = DB.Column(DB.String(16), nullable=False, unique=True, index=True)
    name = DB.Column(DB.String(200), nullable=False, unique=False, index=True)
    movies = DB.relationship('MovieActor', backref='actor', lazy=True)

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Actor data.
        """
        if not isinstance(self, type(None)):
            if not isinstance(self.imdbid, type(None)):
                return {
                    'imdbid': self.imdbid,
                    'imdb': IMDBACTORURL.format(self.imdbid),
                    'name': self.name
                }

        return {}

    @staticmethod
    def get_all_actors(page):
        """
        Retrieve all Actor records from the database table.
        :return: a list of dictionaries representing all available Actors.
        """
        return [
            Actor.dict(actor) for actor in Actor.query.order_by(
                Actor.name.asc()
            ).paginate(page, per_page=APP.config['ACTORS_PER_PAGE']).items
        ]

    @staticmethod
    def get_actor(imdbid):
        """
        Retrieve the Actor record filtered by the parameter imdbid from the
        database table.
        :param imdbid: iMDB movie id of the Actor
        :return: a Python dictionary representing an unique Actor.
        """
        return Actor.dict(Actor.query.filter_by(imdbid=imdbid).first())

    @staticmethod
    def add_actor(name, imdbid):
        """
        Persists Actor data as a record in a database table.
        :param name: Actor name
        :param imdbid: iMDB id of the actor
        :return: None
        """
        actor = Actor(imdbid=imdbid, name=name)

        DB.session.add(actor)
        DB.session.commit()

    @staticmethod
    def put_actor(name, existingimdbid, newimdbid):
        """
        Update or create the Actor information on the database.
        :param name: Actor's name
        :param existingimdbid: existing Actor's imdb id
        :param newimdbid: new Actor's imdb id
        :return: HTTP Code 201 if Movie resource created, 204 if updated.
        """
        httpcode = 204
        actor = Actor.query.filter_by(imdbid=existingimdbid).first()
        if actor is None:
            httpcode = 201
            actor = Actor(
                imdbid=newimdbid,
                name=name
                )
            DB.session.add(actor)
        else:
            actor.name = name
            actor.imdbid = newimdbid

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_actor(existingimdbid, name, newimdbid):
        """
        Update the Actor information on the database.
        :param name: Actor's name
        :param existingimdbid: Actor's imdb id
        :param newimdbid: new Actor's imdb id
        :return: None
        """
        httpcode = 404
        actor = Actor.query.filter_by(imdbid=existingimdbid).first()
        if actor is None:
            return httpcode

        if newimdbid is not None:
            actor.imdbid = newimdbid
        if name is not None:
            actor.name = name
        httpcode = 204

        DB.session.commit()
        return httpcode

    @staticmethod
    def delete_actor(imdbid):
        """
        Delete the Actor from the database.
        :param imdbid: iMDB id of the Actor
        :return: True (deleted) or False (not found)
        """
        okreturn = Actor.query.filter_by(
            imdbid=imdbid).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        return json.dumps(self.dict())


class Genre(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Genre data as
    a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_genre'
    id = DB.Column(DB.Integer, primary_key=True, nullable=False,
                   autoincrement="auto")
    name = DB.Column(DB.String(20), nullable=False, unique=True, index=True)
    movies = DB.relationship('MovieGenre', backref='genre', lazy=True)

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Genre data.
        """
        if not isinstance(self, type(None)):
            if not isinstance(self.name, type(None)):
                return {
                    'name': self.name
                }

        return {}

    @staticmethod
    def get_all_genres(page):
        """
        Retrieve all Genre records from the database table.
        :return: a list of dictionaries representing all available Genres.
        """
        return [
            Genre.dict(genre) for genre in Genre.query.order_by(
                Genre.name.asc()
            ).paginate(page, per_page=APP.config['GENRES_PER_PAGE']).items
        ]

    @staticmethod
    def get_genre(name):
        """
        Retrieve the Genre record filtered by the parameter name from the
        database table.
        :param name: Genre description
        :return: a Python dictionary representing an unique Genre.
        """
        return Genre.dict(Genre.query.filter_by(name=name).first())

    @staticmethod
    def add_genre(name):
        """
        Persists Genre data as a record in a database table.
        :param name: Genre description
        :return: None
        """
        genre = Genre(name=name)

        DB.session.add(genre)
        DB.session.commit()

    @staticmethod
    def put_genre(name, newname):
        """
        Update or create the Genre information on the database.
        :param name: Genre's name
        :param newname: new Genre's name
        :return: HTTP Code 201 if Genre resource created, 204 if updated.
        """
        httpcode = 204
        genre = Genre.query.filter_by(name=name).first()
        if genre is None:
            httpcode = 201
            genre = Genre(
                name=newname
                )
            DB.session.add(genre)
        else:
            genre.name = newname

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_genre(name, newname):
        """
        Update the Genre information on the database.
        :param name: Genre's name
        :param newname: new Genre's name
        :return: None
        """
        httpcode = 404
        genre = Genre.query.filter_by(name=name).first()
        if genre is None:
            return httpcode

        if newname is not None:
            genre.name = newname
        httpcode = 204

        DB.session.commit()
        return httpcode

    @staticmethod
    def delete_genre(name):
        """
        Delete the Genre from the database.
        :param name: Genre's name
        :return: True (deleted) or False (not found)
        """
        okreturn = Genre.query.filter_by(
            name=name).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        return json.dumps(self.dict())


class MovieGenre(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Genres in
    a Movie data as a Python dictionary.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_movie_genres'
    id = DB.Column(
        DB.Integer, primary_key=True, nullable=False, autoincrement="auto"
    )
    movie_id = DB.Column(DB.Integer, ForeignKey('movies_movie.id'))
    genre_id = DB.Column(DB.Integer, ForeignKey('movies_genre.id'))

    @staticmethod
    def add_moviegenre(genrename, movieimdbid):
        """
        Persists Movie Genre data as a record in a database table.
        :param genrename: Genre name
        :param movieimdbid: iMDB movie id
        :return: None
        """
        genre = Genre.query.filter_by(name=genrename).first()
        if genre is None:
            raise ExceptionAPIResourceNotFound(
                GENRENOTFOUND.format(genrename)
            )
        movie = Movie.query.filter_by(imdb=movieimdbid).first()
        if movie is None:
            raise ExceptionAPIResourceNotFound(
                MOVIENOTFOUND.format(movieimdbid)
            )
        moviegenre = MovieGenre(movie_id=movie.id, genre_id=genre.id)
        DB.session.add(moviegenre)
        DB.session.commit()


class Subtitle(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Subtitle
    data as a Python dictionary and providing CRUD methods.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_subtitlelang'
    id = DB.Column(DB.Integer, primary_key=True, nullable=False,
                   autoincrement="auto")
    name = DB.Column(DB.String(20), nullable=False, unique=True, index=True)
    movies = DB.relationship('MovieSubtitle', backref='subtitle', lazy=True)

    def dict(self):
        """
        Converts class members into a Python dictionary.
        :return: a Python dictionary representing the Genre data.
        """
        if not isinstance(self, type(None)):
            if not isinstance(self.name, type(None)):
                return {
                    'name': self.name
                }

        return {}

    @staticmethod
    def get_all_subtitles(page):
        """
        Retrieve all Subtitle records from the database table.
        :return: a list of dictionaries representing all available Subtitles.
        """
        return [
            Subtitle.dict(subtitle) for subtitle in Subtitle.query.order_by(
                Subtitle.name.asc()
            ).paginate(page, per_page=APP.config['SUBTITLES_PER_PAGE']).items
        ]

    @staticmethod
    def get_subtitle(name):
        """
        Retrieve the Subtitle record filtered by the parameter name from the
        database table.
        :param name: Subtitle description
        :return: a Python dictionary representing an unique Subtitle.
        """
        return Subtitle.dict(Subtitle.query.filter_by(name=name).first())

    @staticmethod
    def add_subtitle(name):
        """
        Persists Subtitle data as a record in a database table.
        :param name: Subtitle description
        :return: None
        """
        subtitle = Subtitle(name=name)

        DB.session.add(subtitle)
        DB.session.commit()

    @staticmethod
    def put_subtitle(name, newname):
        """
        Update or create the Subtitle information on the database.
        :param name: Subtitle's name
        :param newname: new Subtitle's name
        :return: HTTP Code 201 if Subtitle resource created, 204 if updated.
        """
        httpcode = 204
        genre = Subtitle.query.filter_by(name=name).first()
        if genre is None:
            httpcode = 201
            genre = Subtitle(
                name=newname
                )
            DB.session.add(genre)
        else:
            genre.name = newname

        DB.session.commit()
        return httpcode

    @staticmethod
    def patch_subtitle(name, newname):
        """
        Update the Subtitle information on the database.
        :param name: Subtitle's name
        :param newname: new Subtitle's name
        :return: None
        """
        httpcode = 404
        subtitle = Subtitle.query.filter_by(name=name).first()
        if subtitle is None:
            return httpcode

        if newname is not None:
            subtitle.name = newname
        httpcode = 204

        DB.session.commit()
        return httpcode

    @staticmethod
    def delete_subtitle(name):
        """
        Delete the Subtitle from the database.
        :param name: Subtitle's name
        :return: True (deleted) or False (not found)
        """
        okreturn = Subtitle.query.filter_by(
            name=name).delete()
        DB.session.commit()
        return bool(okreturn)

    def __repr__(self):
        """
        Class representation
        :return: a JSON object with the fields used by the API layer.
        """
        return json.dumps(self.dict())


class MovieSubtitle(DB.Model):
    """
    Interface to marshal data with a Database provider, returning Subtitles in
    a Movie data as a Python dictionary.
    https://docs.sqlalchemy.org/en/13/core/metadata.html#sqlalchemy.schema.Colu
    mn
    """
    __tablename__ = 'movies_movie_subtitlelangs'
    id = DB.Column(
        DB.Integer, primary_key=True, nullable=False, autoincrement="auto"
    )
    movie_id = \
        DB.Column(DB.Integer, ForeignKey('movies_movie.id'))
    subtitlelang_id = \
        DB.Column(DB.Integer, ForeignKey('movies_subtitlelang.id'))

    @staticmethod
    def add_moviesubtitle(subtitlename, movieimdbid):
        """
        Persists Movie Genre data as a record in a database table.
        :param subtitlename: Subtitle name
        :param movieimdbid: iMDB movie id
        :return: None
        """
        subtitle = Subtitle.query.filter_by(name=subtitlename).first()
        if subtitle is None:
            raise ExceptionAPIResourceNotFound(
                SUBTITLENOTFOUND.format(subtitlename)
            )
        movie = Movie.query.filter_by(imdb=movieimdbid).first()
        if movie is None:
            raise ExceptionAPIResourceNotFound(
                MOVIENOTFOUND.format(movieimdbid)
            )
        moviesubtitle = MovieSubtitle(
            movie_id=movie.id, subtitlelang_id=subtitle.id
        )
        DB.session.add(moviesubtitle)
        DB.session.commit()
