# coding=UTF-8
"""
Module: Environments config file.
https://pythonise.com/series/learning-flask/flask-configuration-files
https://flask.palletsprojects.com/en/1.1.x/config/
"""
import os


class Config(object):
    """
    Base class for environment configuration
    """
    DEBUG = False
    TESTING = False
    DATABASE_URI = ''
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASECONNECTION")
    # https://stackoverflow.com/questions/33738467/how-do-i-know-if-i-can-di
    # sable-sqlalchemy-track-modifications/33790196#33790196
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PINOS_PER_PAGE = 5
    DVDS_PER_PAGE = 5
    MOVIES_PER_PAGE = 5
    ACTORS_PER_PAGE = 5
    GENRES_PER_PAGE = 5
    SUBTITLES_PER_PAGE = 5


class TestingConfig(Config):
    """
    Derived Class: TESTING ENVIRONMENT
    """
    DEBUG = True
    TESTING = True


class ProdConfig(Config):
    """
    Derived Class: PRODUCTION ENVIRONMENT
    """
    DEBUG = False
    TESTING = False
