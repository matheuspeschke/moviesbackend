FROM ubuntu:bionic

COPY system-ubuntu18-dependencies.sh /opt/moviesbackend/system-ubuntu18-dependencies.sh
RUN chmod +x /opt/moviesbackend/system-ubuntu18-dependencies.sh
RUN /opt/moviesbackend/system-ubuntu18-dependencies.sh

COPY requirements.txt /opt/moviesbackend/requirements.txt
RUN pip3 install -r /opt/moviesbackend/requirements.txt

COPY wsgi.py /opt/wsgi.py
COPY moviesbackend /opt/moviesbackend

WORKDIR /opt

ENV DATABASECONNECTION='sqlite:///:memory:'
ENV CONFIGENV=moviesbackend.config.TestingConfig
ENV PYTHONDONTWRITEBYTECODE=1
ENV WEBSERVHOST=0.0.0.0
ENV WEBSERVPORT=5000
ENV WORKERS=4

ENTRYPOINT "gunicorn" \
"-w" \
"$WORKERS" \
"-b" \
"$WEBSERVHOST:$WEBSERVPORT" \
"--access-logfile" \
"-" \
"wsgi:APP"