# coding=UTF-8
"""
Test Module: Test suites for the app.py module.
"""
from tests.utils import TestPopulatedDatabase, TestEmptyDatabase, \
    UNITTESTPINO, UNITTESTMOVIE, NEWPINO, NONEXISTINGPINO, \
    EXISTINGPINO, NEWDVD, NONEXISTINGDVD, EXISTINGDVD, EXISTINGACTOR, \
    NONEXISTINGACTOR, UNITTESTACTOR, NEWACTOR, \
    GENERICMALFORMEDDOC, NEWMOVIE, NONEXISTINGMOVIE, EXISTINGMOVIE, \
    TESTSERVER, MIME_APPJSON, MIME_TEXTHTML, EXCAPIRESNOTFOUND, \
    EXCDATABASEINTEGRCHECK, \
    UNITTESTCHECK_TEXTHTML, UNITTESTCHECK_APPJSON, UNITTESTCHECK_CODE200, \
    UNITTESTCHECK_CODE400, UNITTESTCHECK_CODE404, UNITTESTCHECK_CODE409, \
    UNITTESTCHECK_CODE201, UNITTESTCHECK_CODE204, UNITTESTCHECK_ERRHELPSTR, \
    UNITTESTCHECK_LOCATION, UNITTESTCHECK_LOCNONE, UNITTESTCHECK_ARRELEM, \
    UNITTESTGENRE, EXISTINGGENRE, NONEXISTINGGENRE, NEWGENRE, \
    UNITTESTSUBTITLE, EXISTINGSUBTITLE, NONEXISTINGSUBTITLE, NEWSUBTITLE
from moviesbackend.app import PINOMALFORMEDDOCUMENT, PINOHELPSTRING, \
    PINONOTFOUND, PINOPATCHHELPSTRING, DVDHELPSTRING, DVDNOTFOUND, \
    DVDPATCHHELPSTRING, DVDMALFORMEDDOCUMENT, MOVIEHELPSTRING, \
    MOVIEPATCHHELPSTRING, MOVIENOTFOUND, MOVIEMALFORMEDDOCUMENT, \
    ACTORHELPSTRING, ACTORNOTFOUND, ACTORMALFORMEDDOCUMENT, \
    ACTORPATCHHELPSTRING, MOVIEACTORHELPSTRING, MOVIEACTORMALFORMEDDOCUMENT, \
    GENRENOTFOUND, GENREHELPSTRING, GENREMALFORMEDDOCUMENT, \
    GENREPATCHHELPSTRING, \
    SUBTITLENOTFOUND, SUBTITLEHELPSTRING, SUBTITLEMALFORMEDDOCUMENT, \
    SUBTITLEPATCHHELPSTRING, MOVIEGENREHELPSTRING, \
    MOVIEGENREMALFORMEDDOCUMENT, MOVIESUBTITLEHELPSTRING, \
    MOVIESUBTITLEMALFORMEDDOCUMENT


#
# FLASK ROUTE FRAMEWORK TESTING
# (Validates responses from Flask when supplying an invalid route)
#
class TestAppFlaskRoutesFramework(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_flask_get_nonexistent_route(self):
        """
        Test Flask response for non-existent HTTP GET routes
        """
        response = self.client.get('this route does not exist')
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)
        self.assertEqual(response.location,
                         None,
                         UNITTESTCHECK_LOCNONE)

    def test_flask_post_nonexistent_route(self):
        """
        Test Flask response for non-existent HTTP POST routes
        """
        response = self.client.post('/PARALELEPIPEDO/XPTO')
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)
        self.assertEqual(response.location,
                         None,
                         UNITTESTCHECK_LOCNONE)

    def test_flask_put_nonexistent_route(self):
        """
        Test Flask response for non-existent HTTP PUT routes
        """
        response = self.client.put('/PARALELEPIPEDO/XPTO')
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)
        self.assertEqual(response.location,
                         None,
                         UNITTESTCHECK_LOCNONE)

    def test_flask_patch_nonexistent_route(self):
        """
        Test Flask response for non-existent HTTP PATCH routes
        """
        response = self.client.patch('/PARALELEPIPEDO/XPTO')
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)
        self.assertEqual(response.location,
                         None,
                         UNITTESTCHECK_LOCNONE)

    def test_flask_delete_nonexistent_route(self):
        """
        Test Flask response for non-existent HTTP DELETE routes
        """
        response = self.client.delete('/PARALELEPIPEDO/XPTO')
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)
        self.assertEqual(response.location,
                         None,
                         UNITTESTCHECK_LOCNONE)


#
# HTTP GET TESTS
# (Validates both empty and unit test database)
#
class TestAppPinoGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_get(self):
        """
        This unit test validates that a test case will find no Pinos
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/pinos')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'pinos': []},
                         UNITTESTCHECK_ARRELEM)


class TestAppPinoGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_get(self):
        """
        This unit test validates that a test case will find all Pinos
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/pinos')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'pinos': [EXISTINGPINO]},
                         UNITTESTCHECK_ARRELEM)

    def test_pinos_get_a_pino(self):
        """
        This unit test validates that a test case will find a specific Pino
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get(
            '/pinos/{}'.format(EXISTINGPINO['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         UNITTESTPINO,
                         UNITTESTCHECK_ARRELEM)

    def test_pinos_get_a_pino_does_not_exist(self):
        """
        This unit test validates that a specific Pino object does not
        exist in the database.
        """
        response = self.client.get(
            '/pinos/{}'.format(NEWPINO['name'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': PINONOTFOUND.format(NEWPINO['name']),
                          'helpString': PINOHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppDvdGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_get(self):
        """
        This unit test validates that a test case will find no Dvds
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/dvds')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'dvds': []},
                         UNITTESTCHECK_ARRELEM)


class TestAppDvdGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_get_existing_dvd(self):
        """
        This unit test validates that a test case will find a specific Dvd
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/dvds/{}'.format(EXISTINGDVD['name']))
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         EXISTINGDVD,
                         UNITTESTCHECK_ARRELEM)

    def test_dvds_get_non_existing_dvd(self):
        """
        This unit test validates that a specific Dvd object does not
        exist in the database.
        """
        response = self.client.get('/dvds/{}'.format(NEWDVD['name']))
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': DVDNOTFOUND.format(NEWDVD['name']),
                          'helpString': DVDHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_dvds_get_all(self):
        """
        This unit test validates that there are no Pinos objects in the
        database.
        """
        response = self.client.get('/dvds')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'dvds': [EXISTINGDVD]},
                         UNITTESTCHECK_ARRELEM)

    def test_dvds_get_from_pino(self):
        """
        This unit test validates that there are no Pinos objects in the
        database.
        """
        response = self.client.get(
            '/pinos/{}/dvds'.format(EXISTINGPINO['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'dvds': [EXISTINGDVD]},
                         UNITTESTCHECK_ARRELEM)


class TestAppMovieGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_get_all_movies(self):
        """
        This unit test validates that a test case will find no Movies
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/movies')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'movies': []},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movie_actors(self):
        """
        This unit test validates Actors not found on the database.
        """
        response = self.client.get(
            '/movies/{}/actors'.format(NONEXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIENOTFOUND.format(
                             NONEXISTINGMOVIE['imdbid']
                         ),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movies_get_movie_genres(self):
        """
        This unit test validates Genres not found on the database.
        """
        response = self.client.get(
            '/movies/{}/genres'.format(NONEXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIENOTFOUND.format(
                             NONEXISTINGMOVIE['imdbid']
                         ),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_subtitles_get_movie_subtitles(self):
        """
        This unit test validates Subtitle not found on the database.
        """
        response = self.client.get(
            '/movies/{}/subtitles'.format(NONEXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIENOTFOUND.format(
                             NONEXISTINGMOVIE['imdbid']
                         ),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppMovieGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_get_all_movies(self):
        """
        This unit test validates that a test case will find all Movies
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/movies')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'movies': [UNITTESTMOVIE]},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_a_movie(self):
        """
        This unit test validates that there are no Pinos objects in the
        database.
        """
        response = self.client.get(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         UNITTESTMOVIE,
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movie_actors(self):
        """
        This unit test validates the Actors in the Movie object from the
        database.
        """
        response = self.client.get(
            '/movies/{}/actors'.format(EXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'actors': EXISTINGMOVIE['actors']},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movie_genres(self):
        """
        This unit test validates the Genres in the Movie object from the
        database.
        """
        response = self.client.get(
            '/movies/{}/genres'.format(EXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'genres': EXISTINGMOVIE['genres']},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movie_subtitles(self):
        """
        This unit test validates the Subtitles in the Movie object from the
        database.
        """
        response = self.client.get(
            '/movies/{}/subtitles'.format(EXISTINGMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'subtitles': EXISTINGMOVIE['subtitles']},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_a_movie_does_not_exist(self):
        """
        This unit test validates the API response of retrieving a Movie that
        does not exist.
        """
        response = self.client.get(
            '/movies/{}'.format(NEWMOVIE['imdbid'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIENOTFOUND.format(NEWMOVIE['imdbid']),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movies_get_movies_from_dvd(self):
        """
        This unit test validates the API response of retrieving a Movie from
        a Dvd.
        """
        response = self.client.get(
            '/dvds/{}/movies'.format(EXISTINGDVD['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'movies': [UNITTESTMOVIE]},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movies_from_pino_and_dvd(self):
        """
        This unit test validates the API response of retrieving a Movie -
        from a Pino + Dvd route.
        """
        response = self.client.get(
            '/pinos/{}/dvds/{}/movies'.format(
                EXISTINGPINO['name'],
                EXISTINGDVD['name']
            )
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'movies': [UNITTESTMOVIE]},
                         UNITTESTCHECK_ARRELEM)

    def test_movies_get_movies_from_non_existing_dvd(self):
        """
        This unit test validates the API response of retrieving a Movie -
        from a non-existing Dvd.
        """
        response = self.client.get(
            '/dvds/{}/movies'.format(NEWDVD['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'movies': [{}]},
                         UNITTESTCHECK_ARRELEM)


class TestAppActorGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_get(self):
        """
        This unit test validates that a test case will find no Actors
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/actors')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'actors': []},
                         UNITTESTCHECK_ARRELEM)


class TestAppActorGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_get(self):
        """
        This unit test validates that a test case will find all Actors
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/actors')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'actors': [UNITTESTACTOR]},
                         UNITTESTCHECK_ARRELEM)

    def test_actors_get_a_actor(self):
        """
        This unit test validates that a test case will find a specific Actor
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get(
            '/actors/{}'.format(EXISTINGACTOR['imdbid'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         UNITTESTACTOR,
                         UNITTESTCHECK_ARRELEM)

    def test_actors_get_a_actor_does_not_exist(self):
        """
        This unit test validates that a specific Actor object does not
        exist in the database.
        """
        response = self.client.get(
            '/actors/{}'.format(NEWACTOR['imdbid'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': ACTORNOTFOUND.format(NEWACTOR['imdbid']),
                          'helpString': ACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppGenreGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_get(self):
        """
        This unit test validates that a test case will find no Genres
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/genres')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'genres': []},
                         UNITTESTCHECK_ARRELEM)


class TestAppSubtitleGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_get(self):
        """
        This unit test validates that a test case will find no Subtitles
        when using a 'TestEmptyDatabase' template.
        """
        response = self.client.get('/subtitles')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'subtitles': []},
                         UNITTESTCHECK_ARRELEM)


class TestAppGenreGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_get(self):
        """
        This unit test validates that a test case will find all Genres
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/genres')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'genres': [UNITTESTGENRE]},
                         UNITTESTCHECK_ARRELEM)

    def test_genres_get_a_genre(self):
        """
        This unit test validates that a test case will find a specific Genre
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get(
            '/genres/{}'.format(EXISTINGGENRE['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         UNITTESTGENRE,
                         UNITTESTCHECK_ARRELEM)

    def test_genres_get_a_genre_does_not_exist(self):
        """
        This unit test validates that a specific Genre object does not
        exist in the database.
        """
        response = self.client.get(
            '/genres/{}'.format(NEWGENRE['name'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': GENRENOTFOUND.format(NEWGENRE['name']),
                          'helpString': GENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppSubtitleGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_get(self):
        """
        This unit test validates that a test case will find all Subtitles
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get('/subtitles')
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'subtitles': [UNITTESTSUBTITLE]},
                         UNITTESTCHECK_ARRELEM)

    def test_subtitles_get_a_subtitle(self):
        """
        This unit test validates that a test case will find a specific Subtitle
        supplied by the 'TestPopulatedDatabase' template.
        """
        response = self.client.get(
            '/subtitles/{}'.format(EXISTINGSUBTITLE['name'])
        )
        self.assertEqual(response.status_code,
                         200,
                         UNITTESTCHECK_CODE200)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         UNITTESTSUBTITLE,
                         UNITTESTCHECK_ARRELEM)

    def test_subtitles_get_a_subtitle_does_not_exist(self):
        """
        This unit test validates that a specific Subtitle object does not
        exist in the database.
        """
        response = self.client.get(
            '/subtitles/{}'.format(NEWSUBTITLE['name'])
        )
        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': SUBTITLENOTFOUND.format(
                             NEWSUBTITLE['name']),
                          'helpString': SUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


#
# HTTP POST TESTS
# (Populates the database)
#
class TestAppPinoPostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_post(self):
        """
        This unit test validates the API response of creating a Pino.
        """
        response = self.client.post('/pinos', json=NEWPINO)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/pinos/{}').format(
                             NEWPINO['name']),
                         UNITTESTCHECK_LOCATION.format(NEWPINO['name']))

    def test_pinos_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/pinos',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': PINOMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': PINOHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppPinoPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_post_pino_exists(self):
        """
        This unit test validates the API response of creating a Pino -
        but the Pino already exists.
        """
        response = self.client.post('/pinos', json=EXISTINGPINO)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': PINOHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppDvdPostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_post_pino_does_not_exist(self):
        """
        This unit test validates the API response of creating a Dvd -
        but the Pino does not exist.
        """
        response = self.client.post('/dvds', json=NONEXISTINGDVD)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': DVDHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_dvds_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/dvds',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': DVDMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': DVDHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppDvdPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_post(self):
        """
        This unit test validates the API response of creating a Dvd.
        """
        response = self.client.post('/dvds', json=NEWDVD)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/dvds/{}').format(
                             NEWDVD['name']),
                         UNITTESTCHECK_LOCATION.format(NEWDVD['name']))


class TestAppMoviePostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_post_dvd_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie -
        but the Dvd does not exist.
        """
        response = self.client.post('/movies', json=NONEXISTINGMOVIE)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movies_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/movies',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIEMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppMoviePost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_post(self):
        """
        This unit test validates the API response of creating a Movie.
        """
        response = self.client.post('/movies', json=NEWMOVIE)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             NEWMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(NEWMOVIE['imdbid']))

    def test_movies_post_movie_exists(self):
        """
        This unit test validates the API response of creating a Movie -
        but the Movie already exists.
        """
        response = self.client.post('/movies', json=EXISTINGMOVIE)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppActorPostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_post(self):
        """
        This unit test validates the API response of creating an Actor.
        """
        response = self.client.post('/actors', json=NONEXISTINGACTOR)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             EXISTINGACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGACTOR['imdbid']))

    def test_actors_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/actors',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': ACTORMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': ACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppActorPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_post(self):
        """
        This unit test validates the API response of creating an Actor.
        """
        response = self.client.post('/actors', json=NEWACTOR)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             NEWACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(NEWACTOR['imdbid']))

    def test_actors_post_actor_exists(self):
        """
        This unit test validates the API response of creating an Actor -
        but the Actor already exists.
        """
        response = self.client.post('/actors', json=EXISTINGACTOR)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': ACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMovieActorPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movieactor_post(self):
        """
        This unit test validates the API response of creating a Movie Actor -
        but the Movies does not exist.
        """
        self.client.post('/actors', json=NEWACTOR)
        response = self.client.post(
            '/movies/{}/actors'.format(EXISTINGMOVIE['imdbid']),
            json={
                'actorimdbid': NEWACTOR['imdbid'],
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}/actors').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(EXISTINGMOVIE['imdbid']))

    def test_movieactor_post_movie_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Actor -
        but the Movie does not exist.
        """
        response = self.client.post(
            '/movies/{}/actors'.format(EXISTINGMOVIE['imdbid']),
            json={
                'actorimdbid': EXISTINGACTOR['imdbid'],
                'movieimdbid': NEWMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movieactor_post_actor_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Actor -
        but the Actor does not exist.
        """
        response = self.client.post(
            '/movies/{}/actors'.format(EXISTINGMOVIE['imdbid']),
            json={
                'actorimdbid': NEWACTOR['imdbid'],
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movieactor_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post(
            '/movies/{}/actors'.format(EXISTINGMOVIE['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIEACTORMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': MOVIEACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppMovieGenrePost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_moviegenre_post(self):
        """
        This unit test validates the API response of creating a Movie Genre.
        """
        self.client.post('/genres', json=NEWGENRE)
        response = self.client.post(
            '/movies/{}/genres'.format(EXISTINGMOVIE['imdbid']),
            json={
                'genre': NEWGENRE['name'],
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}/genres').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid'])
                         )

    def test_moviegenre_post_movie_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Genre -
        but the Movie does not exist.
        """
        response = self.client.post(
            '/movies/{}/genres'.format(EXISTINGMOVIE['imdbid']),
            json={
                'genre': EXISTINGGENRE['name'],
                'movieimdbid': NEWMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEGENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_moviegenre_post_genre_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Genre -
        but the Genre does not exist.
        """
        response = self.client.post(
            '/movies/{}/genres'.format(EXISTINGMOVIE['imdbid']),
            json={
                'genre': 'XPTO',
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEGENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_moviegenre_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post(
            '/movies/{}/genres'.format(EXISTINGMOVIE['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIEGENREMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': MOVIEGENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppMovieSubtitlePost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_moviesubtitle_post(self):
        """
        This unit test validates the API response of creating a Movie Subtitle.
        """
        self.client.post('/subtitles', json=NEWSUBTITLE)
        response = self.client.post(
            '/movies/{}/subtitles'.format(EXISTINGMOVIE['imdbid']),
            json={
                'subtitle': NEWSUBTITLE['name'],
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}/subtitles').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid'])
                         )

    def test_moviesubtitle_post_movie_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Subtitle-
        but the Movie does not exist.
        """
        response = self.client.post(
            '/movies/{}/subtitles'.format(EXISTINGMOVIE['imdbid']),
            json={
                'subtitle': EXISTINGSUBTITLE['name'],
                'movieimdbid': NEWMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIESUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_moviesubtitle_post_subtitle_does_not_exist(self):
        """
        This unit test validates the API response of creating a Movie Subtitle-
        but the Subtitle does not exist.
        """
        response = self.client.post(
            '/movies/{}/subtitles'.format(EXISTINGMOVIE['imdbid']),
            json={
                'subtitle': NEWSUBTITLE['name'],
                'movieimdbid': EXISTINGMOVIE['imdbid']
            }
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIESUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_moviesubtitle_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post(
            '/movies/{}/subtitles'.format(EXISTINGMOVIE['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIESUBTITLEMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': MOVIESUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppGenrePostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_post(self):
        """
        This unit test validates the API response of creating a Genre.
        """
        response = self.client.post('/genres', json=NONEXISTINGGENRE)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/genres/{}').format(
                             EXISTINGGENRE['name']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGGENRE['name']))

    def test_genres_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/genres',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': GENREMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': GENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppSubtitlePostEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_post(self):
        """
        This unit test validates the API response of creating a Subtitle.
        """
        response = self.client.post('/subtitles', json=NONEXISTINGSUBTITLE)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/subtitles/{}').format(
                             EXISTINGSUBTITLE['name']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGGENRE['name']))

    def test_subtitles_post_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.post('/subtitles',
                                    json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {
                             'error': SUBTITLEMALFORMEDDOCUMENT.format(
                                 GENERICMALFORMEDDOC
                             ),
                             'helpString': SUBTITLEHELPSTRING
                         },
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppGenrePost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_post(self):
        """
        This unit test validates the API response of creating a Genre.
        """
        response = self.client.post('/genres', json=NEWGENRE)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/genres/{}').format(
                             NEWGENRE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWGENRE['name']))

    def test_genres_post_actor_exists(self):
        """
        This unit test validates the API response of creating a Genre -
        but the Genre already exists.
        """
        response = self.client.post('/genres', json=EXISTINGGENRE)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': GENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppSubtitlePost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_post(self):
        """
        This unit test validates the API response of creating a Subtitle.
        """
        response = self.client.post('/subtitles', json=NEWSUBTITLE)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/subtitles/{}').format(
                             NEWSUBTITLE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWSUBTITLE['name']))

    def test_subtitles_post_subtitle_exists(self):
        """
        This unit test validates the API response of creating a Subtitle -
        but the Subtitle already exists.
        """
        response = self.client.post('/subtitles', json=EXISTINGSUBTITLE)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': SUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


#
# HTTP PUT TESTS
# (Validates both empty and unit test database)
#
class TestAppPinoPutEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_put_pino_does_not_exist(self):
        """
        This unit test validates the API response of updating a Pino -
        but the Pino does not exist.
        """
        response = self.client.put('/pinos/{}'.format(NONEXISTINGPINO['name']),
                                   json=NEWPINO)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/pinos/{}').format(
                             NEWPINO['name']),
                         UNITTESTCHECK_LOCATION.format(NEWPINO['name']))


class TestAppPinoPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_put_pino_exists(self):
        """
        This unit test validates the API response of updating a Pino -
        and the Pino exists.
        """
        response = self.client.put('/pinos/{}'.format(EXISTINGPINO['name']),
                                   json=NEWPINO)
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/pinos/{}').format(
                             NEWPINO['name']),
                         UNITTESTCHECK_LOCATION.format(NEWPINO['name']))

    def test_pinos_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put('/pinos/{}'.format(EXISTINGPINO['name']),
                                   json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': PINOMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': PINOHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppPinoPutIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_put_new_pino(self):
        """
        This unit test validates the API response of updating a Pino -
        but the Pino does not exist.
        """
        response = self.client.put('/pinos/{}'.format(NEWPINO['name']),
                                   json=NEWPINO)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/pinos/{}').format(
                             NEWPINO['name']),
                         UNITTESTCHECK_LOCATION.format(NEWPINO['name']))

    def test_pinos_put_existing_pino(self):
        """
        This unit test validates the API response of updating a Pino -
        but the new name belongs to another Pino.
        """
        response = self.client.put('/pinos/{}'.format(NEWPINO['name']),
                                   json=EXISTINGPINO)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': PINOHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppDvdPutIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put('/dvds/{}'.format(EXISTINGDVD['name']),
                                   json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': DVDMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': DVDHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_dvds_put(self):
        """
        This unit test validates the API response of updating a Dvd -
        but the Dvd does not exist.
        """
        response = self.client.put('/dvds/{}'.format(NEWDVD['name']),
                                   json=NEWDVD)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/dvds/{}').format(NEWDVD['name']),
                         UNITTESTCHECK_LOCATION.format(NEWDVD['name']))

    def test_dvds_put_existing_dvd(self):
        """
        This unit test validates the API response of updating a Dvd -
        but the new name belongs to another Dvd.
        """
        response = self.client.put('/dvds/{}'.format(NEWDVD['name']),
                                   json=EXISTINGDVD)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': DVDHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)

    def test_dvds_put_new_dvd(self):
        """
        This unit test validates the API response of updating a Dvd -
        but the Dvd does not exist.
        """
        response = self.client.put('/dvds/{}'.format(EXISTINGDVD['name']),
                                   json=NEWDVD)
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/dvds/{}').format(NEWDVD['name']),
                         UNITTESTCHECK_LOCATION.format(NEWDVD['name']))


class TestAppMoviePutIntegrityError(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put(
            '/movies/{}'.format(NONEXISTINGMOVIE['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIEMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movies_put_dvd_does_not_exist(self):
        """
        This unit test validates the API response of updating a Movie -
        but the Dvd does not exist.
        """
        response = self.client.put(
            '/movies/{}'.format(NONEXISTINGMOVIE['imdbid']),
            json=NONEXISTINGMOVIE
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMoviePutIntegrityErrorNonEmpty(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_put_new_movie_already_exists(self):
        """
        This unit test validates the API response of updating a Movie -
        but the new Movie already exists.
        """
        response = self.client.put(
            '/movies/{}'.format(NEWMOVIE['imdbid']),
            json=EXISTINGMOVIE
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': MOVIEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMoviePut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_put_movie_exists(self):
        """
        This unit test validates the API response of updating a Movie -
        and the Movie exists.
        """
        response = self.client.put(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=NEWMOVIE
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             NEWMOVIE['imdbid']
                         ),
                         UNITTESTCHECK_LOCATION.format(NEWMOVIE['imdbid']))

    def test_movies_put_movie_does_not_exist(self):
        """
        This unit test validates the API response of updating a Movie -
        the Movie does not exist.
        """
        response = self.client.put(
            '/movies/{}'.format(NEWMOVIE['imdbid']),
            json=NEWMOVIE
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             NEWMOVIE['imdbid']
                         ),
                         UNITTESTCHECK_LOCATION.format(NEWMOVIE['imdbid']))


class TestAppActorPutEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_put_actor_does_not_exist(self):
        """
        This unit test validates the API response of updating an Actor -
        but the Actor does not exist.
        """
        response = self.client.put('/actors/{}'.format(
            NONEXISTINGACTOR['imdbid']
        ),
                                   json=NONEXISTINGACTOR)
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             EXISTINGACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGACTOR['imdbid']))


class TestAppActorPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_put_actor_exists(self):
        """
        This unit test validates the API response of updating an Actor -
        and the Actor exists.
        """
        response = self.client.put('/actors/{}'.format(
            EXISTINGACTOR['imdbid']
        ),
                                   json=NEWACTOR)
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             NEWACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(NEWACTOR['imdbid']))

    def test_actors_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put('/actors/{}'.format(
            EXISTINGACTOR['imdbid']),
                                   json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': ACTORMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': ACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppActorPutIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_put_actor_exists(self):
        """
        This unit test validates the API response of updating a Movie -
        but the Dvd does not exist.
        """
        response = self.client.put(
            '/actors/{}'.format(NEWACTOR['imdbid']),
            json=EXISTINGACTOR
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': ACTORHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppGenrePutEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """

    def test_genres_put_genre_does_not_exist(self):
        """
        This unit test validates the API response of updating a Genre -
        but the Genre does not exist.
        """
        response = self.client.put(
            '/genres/{}'.format(
                NONEXISTINGGENRE['name']
            ),
            json=NEWGENRE
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/genres/{}').format(
                             NEWGENRE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWGENRE['name']))


class TestAppSubtitlePutEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_put_subtitle_does_not_exist(self):
        """
        This unit test validates the API response of updating a Subtitle -
        but the Subtitle does not exist.
        """
        response = self.client.put(
            '/subtitles/{}'.format(
                NONEXISTINGSUBTITLE['name']
            ),
            json=NEWSUBTITLE
        )
        self.assertEqual(response.status_code,
                         201,
                         UNITTESTCHECK_CODE201)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/subtitles/{}').format(
                             NEWSUBTITLE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWSUBTITLE['name']))


class TestAppGenrePut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_put_genre_exists(self):
        """
        This unit test validates the API response of updating an Genre -
        and the Genre exists.
        """
        response = self.client.put(
            '/genres/{}'.format(
                EXISTINGGENRE['name']
            ),
            json=NEWGENRE
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/genres/{}').format(
                             NEWGENRE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWGENRE['name']))

    def test_genres_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put(
            '/genres/{}'.format(
                EXISTINGGENRE['name']
            ),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {
                             'error': GENREMALFORMEDDOCUMENT.format(
                                 GENERICMALFORMEDDOC
                             ),
                             'helpString': GENREHELPSTRING
                         },
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppSubtitlePut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_put_subtitle_exists(self):
        """
        This unit test validates the API response of updating an Subtitle -
        and the Subtitle exists.
        """
        response = self.client.put(
            '/subtitles/{}'.format(
                EXISTINGSUBTITLE['name']
            ),
            json=NEWSUBTITLE
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/subtitles/{}').format(
                             NEWSUBTITLE['name']),
                         UNITTESTCHECK_LOCATION.format(NEWSUBTITLE['name']))

    def test_subtitles_put_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.put(
            '/subtitles/{}'.format(
                EXISTINGSUBTITLE['name']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {
                             'error': SUBTITLEMALFORMEDDOCUMENT.format(
                                 GENERICMALFORMEDDOC
                             ),
                             'helpString': SUBTITLEHELPSTRING
                         },
                         UNITTESTCHECK_ERRHELPSTR)


class TestAppGenrePutIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_put_genre_exists(self):
        """
        This unit test validates the API response of updating a Genre -
        but the Genre does not exist.
        """
        response = self.client.put(
            '/genres/{}'.format(NEWGENRE['name']),
            json=EXISTINGGENRE
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': GENREHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppSubtitlePutIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_put_subtitle_exists(self):
        """
        This unit test validates the API response of updating a Subtitle -
        but the Subtitle does not exist.
        """
        response = self.client.put(
            '/subtitles/{}'.format(NEWSUBTITLE['name']),
            json=EXISTINGSUBTITLE
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': SUBTITLEHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


#
# HTTP PATCH TESTS
# (Validates both empty and unit test database)
#
class TestAppPinoPatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch('/pinos/{}'.format(EXISTINGPINO['name']),
                                     json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': PINOMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': PINOPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_pinos_patch_conflict_another_one(self):
        """
        This unit test validates the API response of patching a Pino's name -
        but the name belongs to another Pino.
        """
        self.client.post('/pinos', json=NEWPINO)
        response = self.client.patch('/pinos/{}'.format(
            EXISTINGPINO['name']
        ), json=NEWPINO)
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': PINOPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)

    def test_pino_patch_update_pino(self):
        """
        This unit test validates the API response of patching a Pino's name.
        """
        response = self.client.patch(
            '/pinos/{}'.format(EXISTINGPINO['name']),
            json=NEWPINO
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppDvdPatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_patch_pino_does_not_exist(self):
        """
        This unit test validates the API response of patching a Dvd's Pino -
        but the Pino does not exist.
        """
        dvdpatchedpino = {
            'pino': 'XPTOXPTO'
        }

        response = self.client.patch(
            '/dvds/{}'.format(EXISTINGDVD['name']),
            json=dvdpatchedpino
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': DVDPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppDvdPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch('/dvds/{}'.format(EXISTINGDVD['name']),
                                     json=GENERICMALFORMEDDOC)
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': DVDMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString': DVDPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_dvds_patch_new_existing_pino(self):
        """
        This unit test validates the API response of patching a Dvd's Pino -
        the Pino exists.
        """
        self.client.post('/pinos', json=NEWPINO)

        dvdpatchedpino = {'pino': NEWPINO['name']}

        response = self.client.patch(
            '/dvds/{}'.format(EXISTINGDVD['name']),
            json=dvdpatchedpino
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMoviePatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': MOVIEMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString':
                              MOVIEPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_movies_patch_dvd_does_not_exist(self):
        """
        This unit test validates the API response of patching a Movie's Dvd -
        but the Dvd does not exist.
        """
        dvdpatcheddvd = {'dvd': 'XPTOXPTO'}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=dvdpatcheddvd
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCAPIRESNOTFOUND,
                          'helpString': MOVIEPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)

    def test_movies_patch_movie_already_exists(self):
        """
        This unit test validates the API response of patching a Movie's imdbid-
        but the Movies already exists.
        """
        patchedimdbid = {'imdb': EXISTINGMOVIE['imdbid']}

        self.client.post('/movies', json=NEWMOVIE)
        response = self.client.patch(
            '/movies/{}'.format(NEWMOVIE['imdbid']),
            json=patchedimdbid
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': MOVIEPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMoviePatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_patch_dvd(self):
        """
        This unit test validates the API response of patching a Movie's dvd.
        """
        patch = {'dvd': EXISTINGDVD['name']}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))

    def test_movies_patch_name(self):
        """
        This unit test validates the API response of patching a Movie's name.
        """
        patch = {'name': 'O Quinto Elemento'}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))

    def test_movies_patch_director(self):
        """
        This unit test validates the API response of patching a Movie's
        director.
        """
        patch = {'director': 'Luca Beção'}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))

    def test_movies_patch_format(self):
        """
        This unit test validates the API response of patching a Movie's format.
        """
        patch = {'format': 'VHS'}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))

    def test_movies_patch_year(self):
        """
        This unit test validates the API response of patching a Movie's year.
        """
        patch = {'year': '9997'}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))

    def test_movies_patch_imdbid(self):
        """
        This unit test validates the API response of patching a Movie's imdbid.
        """
        patch = {'imdb': NEWMOVIE['imdbid']}

        response = self.client.patch(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/movies/{}').format(
                             EXISTINGMOVIE['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGMOVIE['imdbid']))


class TestAppActorPatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actor_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch(
            '/actors/{}'.format(EXISTINGACTOR['imdbid']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {'error': ACTORMALFORMEDDOCUMENT.format(
                             GENERICMALFORMEDDOC),
                          'helpString':
                              ACTORPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)

    def test_actors_patch_actor_already_exists(self):
        """
        This unit test validates the API response of patching an Actor's imdbid
        but the Actor already exists.
        """
        patchedimdbid = {'imdbid': EXISTINGACTOR['imdbid']}

        self.client.post('/actors', json=NEWACTOR)
        response = self.client.patch(
            '/actors/{}'.format(NEWACTOR['imdbid']),
            json=patchedimdbid
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': ACTORPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppActorPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_patch_name(self):
        """
        This unit test validates the API response of patching a Actor's name.
        """
        patch = {'name': NEWACTOR['name']}

        response = self.client.patch(
            '/actors/{}'.format(EXISTINGACTOR['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             EXISTINGACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGACTOR['imdbid']))

    def test_actors_patch_imdbid(self):
        """
        This unit test validates the API response of patching a Actor's imdbid.
        """
        patch = {'imdbid': NEWACTOR['imdbid']}

        response = self.client.patch(
            '/actors/{}'.format(EXISTINGACTOR['imdbid']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/actors/{}').format(
                             EXISTINGACTOR['imdbid']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGACTOR['imdbid']))


class TestAppGenrePatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """

    def test_genre_patch_name(self):
        """
        This unit test validates the API response of patching a Genre's name.
        """
        patch = {'name': NEWGENRE['name']}

        response = self.client.patch(
            '/genres/{}'.format(EXISTINGGENRE['name']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/genres/{}').format(
                             patch['name']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGGENRE['name']))


class TestAppSubtitlePatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitle_patch_name(self):
        """
        This unit test validates the API response of patching a
        Subtitle's name.
        """
        patch = {'name': NEWSUBTITLE['name']}

        response = self.client.patch(
            '/subtitles/{}'.format(EXISTINGSUBTITLE['name']),
            json=patch
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.location,
                         TESTSERVER.format('/subtitles/{}').format(
                             patch['name']),
                         UNITTESTCHECK_LOCATION.format(
                             EXISTINGSUBTITLE['name']))


class TestAppGenrePatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_genres_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch(
            '/genres/{}'.format(EXISTINGGENRE['name']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {
                             'error': GENREMALFORMEDDOCUMENT.format(
                                 GENERICMALFORMEDDOC
                             ),
                             'helpString':
                                 GENREPATCHHELPSTRING
                         },
                         UNITTESTCHECK_ERRHELPSTR)

    def test_genres_patch_genre_does_not_exist(self):
        """
        This unit test validates the API response of patching a Genre's Name -
        but the Genre does not exist.
        """
        patchedgenre = {'name': 'XPTOXPTO'}

        response = self.client.patch(
            '/genres/{}'.format(EXISTINGGENRE['name']),
            json=patchedgenre
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)

    def test_genres_patch_genre_already_exists(self):
        """
        This unit test validates the API response of patching a Genre's name-
        but the Genre already exists.
        """
        patchedgenre = {'name': EXISTINGGENRE['name']}

        self.client.post('/genres', json=NEWGENRE)
        response = self.client.patch(
            '/genres/{}'.format(NEWGENRE['name']),
            json=patchedgenre
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': GENREPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppSubtitlePatchIntegrityError(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_subtitles_patch_malformed_document(self):
        """
        This unit test validates the API response when the request json is
        incorrect.
        """
        response = self.client.patch(
            '/subtitles/{}'.format(EXISTINGSUBTITLE['name']),
            json=GENERICMALFORMEDDOC
        )
        self.assertEqual(response.status_code,
                         400,
                         UNITTESTCHECK_CODE400)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
        self.assertEqual(response.json,
                         {
                             'error':
                                 SUBTITLEMALFORMEDDOCUMENT.format(
                                     GENERICMALFORMEDDOC
                                 ),
                             'helpString':
                                 SUBTITLEPATCHHELPSTRING
                         },
                         UNITTESTCHECK_ERRHELPSTR)

    def test_subtitles_patch_subtitle_does_not_exist(self):
        """
        This unit test validates the API response of patching a Subtitle's
        Name - but the Subtitle does not exist.
        """
        patchedsubtitle = {'name': 'XPTOXPTO'}

        response = self.client.patch(
            '/subtitles/{}'.format(EXISTINGSUBTITLE['name']),
            json=patchedsubtitle
        )
        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)

    def test_subtitles_patch_subtitle_already_exists(self):
        """
        This unit test validates the API response of patching a Subtitle's
        name - but the Subtitle already exists.
        """
        patchedsubtitle = {'name': EXISTINGSUBTITLE['name']}

        self.client.post('/subtitles', json=NEWSUBTITLE)
        response = self.client.patch(
            '/subtitles/{}'.format(NEWSUBTITLE['name']),
            json=patchedsubtitle
        )
        self.assertEqual(response.status_code,
                         409,
                         UNITTESTCHECK_CODE409)
        self.assertEqual(response.json,
                         {'error': EXCDATABASEINTEGRCHECK,
                          'helpString': SUBTITLEPATCHHELPSTRING},
                         UNITTESTCHECK_ERRHELPSTR)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


#
# HTTP DELETE TESTS
# (Validates both empty and unit test database)
#
class TestAppPinoDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_pinos_delete_pino_exists(self):
        """
        This unit test validates the API response of deleting a Pino.
        """
        response = self.client.delete('/pinos/{}'.format(EXISTINGPINO['name']))

        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)

    def test_pinos_delete_pino_does_not_exist(self):
        """
        This unit test validates the API response of deleting a Pino that
        does not exist.
        """
        response = self.client.delete('/pinos/{}'.format(NEWPINO['name']))

        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppDvdDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_dvds_delete_dvd_exists(self):
        """
        This unit test validates the API response of deleting a Dvd that
        exists.
        """
        response = self.client.delete('/dvds/{}'.format(EXISTINGDVD['name']))

        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)

    def test_dvds_delete_dvd_does_not_exist(self):
        """
        This unit test validates the API response of deleting a Dvd that
        does not exist.
        """
        response = self.client.delete('/dvds/{}'.format(NEWDVD['name']))

        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppMovieDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_movies_delete_movie_exists(self):
        """
        This unit test validates the API response of deleting a Movie that
        exists.
        """
        response = self.client.delete(
            '/movies/{}'.format(EXISTINGMOVIE['imdbid'])
        )

        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)

    def test_movies_delete_movie_does_not_exist(self):
        """
        This unit test validates the API response of deleting a Movie that
        does not exist.
        """
        response = self.client.delete(
            '/movies/{}'.format(NEWMOVIE['imdbid'])
        )

        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)


class TestAppActorDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.app module features.
    """
    def test_actors_delete_actor_exists(self):
        """
        This unit test validates the API response of deleting a Actor that
        exists.
        """
        response = self.client.delete(
            '/actors/{}'.format(EXISTINGACTOR['imdbid'])
        )

        self.assertEqual(response.status_code,
                         204,
                         UNITTESTCHECK_CODE204)
        self.assertEqual(response.mimetype,
                         MIME_TEXTHTML,
                         UNITTESTCHECK_TEXTHTML)

    def test_actors_delete_actor_does_not_exist(self):
        """
        This unit test validates the API response of deleting a Actor that
        does not exist.
        """
        response = self.client.delete(
            '/actors/{}'.format(NEWACTOR['imdbid'])
        )

        self.assertEqual(response.status_code,
                         404,
                         UNITTESTCHECK_CODE404)
        self.assertEqual(response.mimetype,
                         MIME_APPJSON,
                         UNITTESTCHECK_APPJSON)
