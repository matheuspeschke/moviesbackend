# coding=UTF-8
"""
Test Module: Test suites for the models.py module.
"""
import inspect
import unittest
import subprocess
from sqlalchemy.exc import IntegrityError, StatementError
from moviesbackend.exceptions import ExceptionAPIResourceNotFound
from moviesbackend.models import Pino, Dvd, Movie, Actor, MovieActor, \
    MovieGenre, Genre, MovieSubtitle, Subtitle, DB, IMDBACTORURL, IMDBTITLEURL
from moviesbackend.app import APP

UNITTESTPINO = {
    'name': '00000001'
}
UNITTESTDVD = {
    'name': '00000001', 'pino': UNITTESTPINO['name']
}
UNITTESTACTOR = {
    'imdbid': 'nm0000170',
    'imdb': IMDBACTORURL.format('nm0000170'),
    'name': 'Milla Jovovich'
}
UNITTESTMOVIEACTOR = {
    'name': UNITTESTACTOR['name'],
    'imdb': UNITTESTACTOR['imdb']
}
UNITTESTGENRE = {
    'name': 'Sci-Fi'
}
UNITTESTMOVIEGENRE = {
    'name': UNITTESTGENRE['name']
}
UNITTESTSUBTITLE = {
    'name': 'Portuguese'
}
UNITTESTMOVIESUBTITLE = {
    'name': UNITTESTSUBTITLE['name']
}
UNITTESTMOVIE = {
    'name': 'The Fifth Element',
    'director': 'Luc Besson',
    'imdb': IMDBTITLEURL.format('tt0119116'),
    'year': '1997',
    'format': 'MPEG-2',
    'dvd': UNITTESTDVD['name'],
    'imdbid': 'tt0119116',
    'actors': [UNITTESTMOVIEACTOR],
    'genres': [UNITTESTMOVIEGENRE],
    'subtitles': [UNITTESTMOVIESUBTITLE]
}

NEWPINO = \
    {
        'name': '00000002'
    }
NONEXISTINGPINO = EXISTINGPINO = \
    {
        'name': UNITTESTPINO['name'],
    }
NEWDVD = \
    {
        'name': '00000002',
        'pino': UNITTESTPINO['name']
    }
NONEXISTINGDVD = EXISTINGDVD = \
    {
        'name': UNITTESTDVD['name'],
        'pino': UNITTESTDVD['pino']
    }
GENERICMALFORMEDDOC = \
    {
        'DOESNOT': UNITTESTDVD['name'],
        'EXIST': UNITTESTDVD['pino']
    }
NONEXISTINGACTOR = EXISTINGACTOR = {
    'imdbid': UNITTESTACTOR['imdbid'],
    'name': UNITTESTACTOR['name']
}
NEWACTOR = {
    'name': 'Michael J. Fox',
    'imdbid': 'nm0000150'
}
NEWMOVIE = \
    {
        'name': 'Back to the Future',
        'director': 'Robert Zemeckis',
        'year': '1985',
        'format': 'BluRay',
        'dvd': UNITTESTDVD['name'],
        'imdbid': 'tt0088763',
        'actors': [],
        'genres': [],
        'subtitles': []
    }
NONEXISTINGMOVIE = EXISTINGMOVIE = \
    {
        'name': UNITTESTMOVIE['name'],
        'director': UNITTESTMOVIE['director'],
        'year': UNITTESTMOVIE['year'],
        'format': UNITTESTMOVIE['format'],
        'dvd': UNITTESTMOVIE['dvd'],
        'imdbid': UNITTESTMOVIE['imdbid'],
        'actors': [UNITTESTMOVIEACTOR],
        'genres': [UNITTESTMOVIEGENRE],
        'subtitles': [UNITTESTMOVIESUBTITLE]
    }
NONEXISTINGMOVIEACTOR = EXISTINGMOVIEACTOR = {
    'name': UNITTESTACTOR['name'],
    'imdb': UNITTESTACTOR['imdb']
}
NEWMOVIEACTOR = {
    'name': NEWACTOR['name'],
    'imdb': IMDBACTORURL.format(NEWACTOR['imdbid'])
}
NEWGENRE = {
    'name': 'Action'
}
NONEXISTINGGENRE = EXISTINGGENRE = {
    'name': UNITTESTGENRE['name']
}
NEWMOVIEGENRE = {
    'imdb': IMDBTITLEURL.format(NEWMOVIE['imdbid']),
    'name': NEWGENRE['name']
}
NONEXISTINGMOVIEGENRE = EXISTINGMOVIEGENRE = {
    'name': UNITTESTGENRE['name']
}
NEWSUBTITLE = {
    'name': 'German'
}
NONEXISTINGSUBTITLE = EXISTINGSUBTITLE = {
    'name': UNITTESTSUBTITLE['name']
}
NEWMOVIESUBTITLE = {
    'imdb': IMDBTITLEURL.format(NEWMOVIE['imdbid']),
    'name': NEWSUBTITLE['name']
}

TESTSERVER = 'http://localhost{}'
MIME_APPJSON = 'application/json'
MIME_TEXTHTML = 'text/html'
EXCAPIRESNOTFOUND = str(ExceptionAPIResourceNotFound('').__class__)
EXCDATABASEINTEGRCHECK = str(IntegrityError(
    statement=StatementError(
        message=None,
        statement=None,
        params=None,
        orig=None
    ),
    params=None,
    orig=None).__class__)

UNITTESTCHECK_TEXTHTML = 'Expected text/html as mime type.'
UNITTESTCHECK_APPJSON = 'Expected application/json as mime type.'
UNITTESTCHECK_CODE200 = 'Expected HTTP response code 200 OK.'
UNITTESTCHECK_CODE400 = 'Expected HTTP response code 400 MALFORMED DOCUMENT.'
UNITTESTCHECK_CODE404 = 'Expected HTTP response code 404 NOT FOUND.'
UNITTESTCHECK_CODE409 = 'Expected HTTP response code 409 CONFLICT.'
UNITTESTCHECK_CODE201 = 'Expected HTTP response code 201 CREATED.'
UNITTESTCHECK_CODE204 = 'Expected HTTP response code 204 NO CONTENT.'
UNITTESTCHECK_ERRHELPSTR = "Unexpected field/values 'error' and 'helpString'"
UNITTESTCHECK_LOCATION = "Unexpected location of the created/updated resourc' \
                         'e '{}'."
UNITTESTCHECK_LOCNONE = "Expected location to be 'None'"
UNITTESTCHECK_ARRELEM = 'Unexpected element(s) or number of elements in the ' \
                        'array.'


class TestEmptyDatabase(unittest.TestCase):
    """
    Test Case template:
    This superclass does NOT populate the test database and uses
    the APP instance to access the API.
    """
    @classmethod
    def setUpClass(cls):
        """
        Creates all database objects (tables) and defines a client to access
        the Flask webserver interface.
        Scope: before the first class instance initiates
        :return: None
        """
        Pino.metadata.create_all(DB.engine)
        Dvd.metadata.create_all(DB.engine)
        Movie.metadata.create_all(DB.engine)
        Actor.metadata.create_all(DB.engine)
        Genre.metadata.create_all(DB.engine)
        Subtitle.metadata.create_all(DB.engine)
        MovieActor.metadata.create_all(DB.engine)
        MovieGenre.metadata.create_all(DB.engine)
        MovieSubtitle.metadata.create_all(DB.engine)
        cls.client = APP.test_client()

    def tearDown(self):
        """
        Deletes all records from the tables.
        Scope: after every test case is completed.
        :return: None
        """
        DB.session.query(MovieGenre).delete()
        DB.session.query(MovieSubtitle).delete()
        DB.session.query(MovieActor).delete()
        DB.session.query(Actor).delete()
        DB.session.query(Movie).delete()
        DB.session.query(Dvd).delete()
        DB.session.query(Pino).delete()
        DB.session.query(Genre).delete()
        DB.session.query(Subtitle).delete()
        DB.session.commit()

    @classmethod
    def tearDownClass(cls):
        """
        Destroys all database tables.
        Scope: after the last class instance exits
        :return: None
        """
        MovieGenre.metadata.drop_all(DB.engine)
        MovieSubtitle.metadata.drop_all(DB.engine)
        MovieActor.metadata.drop_all(DB.engine)
        Actor.metadata.drop_all(DB.engine)
        Movie.metadata.drop_all(DB.engine)
        Dvd.metadata.drop_all(DB.engine)
        Pino.metadata.drop_all(DB.engine)
        Genre.metadata.drop_all(DB.engine)
        Subtitle.metadata.drop_all(DB.engine)


class TestPopulatedDatabase(TestEmptyDatabase):
    """
    Test Case template:
    This superclass populates the test database and uses
    the DB instance to access the database engine only.
    """
    def setUp(self):
        """
        Creates all table records.
        Scope: every time a test case runs
        :return:
        """
        pino = Pino()
        pino.name = UNITTESTPINO['name']
        DB.session.add(pino)

        dvd = Dvd()
        dvd.name = UNITTESTDVD['name']
        dvd.pino_id = 1
        DB.session.add(dvd)

        movie = Movie()
        movie.name = UNITTESTMOVIE['name']
        movie.director = UNITTESTMOVIE['director']
        movie.imdb = UNITTESTMOVIE['imdbid']
        movie.format = UNITTESTMOVIE['format']
        movie.year = UNITTESTMOVIE['year']
        movie.dvd_id = 1
        DB.session.add(movie)

        actor = Actor()
        actor.imdbid = UNITTESTACTOR['imdbid']
        actor.name = UNITTESTACTOR['name']
        DB.session.add(actor)
        # Commit here Movie and Actor changes to the database,
        # so the MovieActor has valid Movie and Actor data.
        DB.session.commit()

        movieactor = MovieActor()
        movieactor.actor_id = actor.id
        movieactor.movie_id = movie.id
        DB.session.add(movieactor)
        DB.session.commit()

        genre = Genre()
        genre.name = UNITTESTGENRE['name']
        DB.session.add(genre)
        # Commit here Genre changes to the database,
        # so the MovieGenre has valid Genre data.
        DB.session.commit()

        moviegenre = MovieGenre()
        moviegenre.movie_id = movie.id
        moviegenre.genre_id = genre.id
        DB.session.add(moviegenre)
        DB.session.commit()

        subtitle = Subtitle()
        subtitle.name = UNITTESTSUBTITLE['name']
        DB.session.add(subtitle)
        # Commit here Subtitle changes to the database,
        # so the MovieSubtitle has valid Subtitle data.
        DB.session.commit()

        moviesubtitle = MovieSubtitle()
        moviesubtitle.movie_id = movie.id
        moviesubtitle.subtitlelang_id = subtitle.id
        DB.session.add(moviesubtitle)
        DB.session.commit()


class TestImdbBashScript(object):
    """
    Test Case suite: Test the ../imdb-bash/imdb.sh script.
    These unit tests are performed using bash, and there are several
    system dependencies to run these unit tests (mostly GNU utilities) -
    check the imdbfunctions.sh script's comments.
    """
    imdbbash = "./imdb-bash/imdbfunctions.sh"
    timeouterror = "File \"{}\", line {}, in {}: aborting unit test, child b" \
                   "ash process didn't return before configured timeout ({}s)"
    returnerror = "Running bash command '{}' returned: '{}'"
    bashdependencies = ['recode', 'awk', 'gawk', 'xmllint']
    timeout = 15

    def __init__(self):
        """
        Initialize shared variables.
        :return:
        """
        self.imdbid = ""
        self.imdb = ""
        self.title = ""
        self.year = ""
        self.directors = ""
        self.genres = []
        self.actors = []

    def runprocess(self, command):
        """
        Run a (child) process.
        Requires: Python 3.6.X
        :param command: a list of a process command line (with parameters).
        :return: a CompletedProcess instance
        """
        process = subprocess.run(command,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT,
                                 timeout=self.timeout,
                                 check=False)
        return process

    def test_000_bash_dependencies(self):
        """
        Test the required bash dependencies.
        """
        callerframerecord = inspect.stack()[1]
        frame = callerframerecord[0]
        info = inspect.getframeinfo(frame)

        for dep in self.bashdependencies:
            bashcommand = "which {}".format(dep)
            try:
                process = self.runprocess(bashcommand.split())
                self.assertTrue(process.returncode is 0,
                                msg=self.returnerror.format(
                                    bashcommand,
                                    process.returncode
                                )
                                )
            except subprocess.TimeoutExpired:
                print(
                    self.timeouterror.format(
                        info.filename,
                        info.lineno,
                        info.function,
                        self.timeout
                    )
                )
                return

    def test_001_valid_movie_url(self):
        """
        Test iMDB url construction from the iMDB movie ID.
        """
        callerframerecord = inspect.stack()[1]
        frame = callerframerecord[0]
        info = inspect.getframeinfo(frame)

        bashcommand = "source {}; echo \"$(valid_movie_url \"{}\")\"".format(
            self.imdbbash,
            self.imdbid
        )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        self.assertTrue(
            process.stdout.decode('utf-8').strip('\n') == self.imdb,
            msg="Expected url '{}'. Got '{}' instead.".format(
                self.imdb, process.stdout.decode('utf-8').strip('\n')
            )
        )

    def test_002_get_movie_elements_from_html(self):
        """
        Test bash functions parsing and extracting HTML information from
        iMDB web pages.
        """
        callerframerecord = inspect.stack()[1]
        frame = callerframerecord[0]
        info = inspect.getframeinfo(frame)

        # Main iMDB page content
        bashcommand = "source {}; echo \"$(get_page_content \"{}\")\"".format(
            self.imdbbash,
            self.imdb
        )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        self.assertTrue(
            str(process.stdout.decode('utf-8')) is not "",
            msg="Expected output of not empty HTML page content."
        )

        # Full Credit iMDB url
        bashcommand = "source {}; echo \"$(get_movie_fullcredit_url \"{}\")" \
                      "\"".format(
                                    self.imdbbash,
                                    self.imdbid
                                )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        htmlfullcrediturl = str(process.stdout.decode('utf-8')).strip('\n')

        # Full Credit iMDB page content
        bashcommand = "source {}; echo \"$(get_page_content \"{}\")\"".format(
            self.imdbbash,
            htmlfullcrediturl
        )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        self.assertTrue(
            str(process.stdout.decode('utf-8')) is not "",
            msg="Expected output of not empty HTML page content."
        )

        # Movie title
        bashcommand = "source {}; echo \"$(get_movie_title \"$(get_page_cont" \
                      "ent \"{}\")\")\"".format(
                                                self.imdbbash,
                                                htmlfullcrediturl
                                                )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        title = str(process.stdout.decode('utf-8')).strip('\n')
        self.assertTrue(
            title == self.title,
            msg="Expected title '{}'. Got '{}' instead.".format(
                self.title, title
            )
        )

        # Movie director
        bashcommand = "source {}; echo \"$(get_movie_directors \"$(get_page_" \
                      "content \"{}\")\""")\"".format(
                                                        self.imdbbash,
                                                        htmlfullcrediturl
                                                    )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        directors = str(process.stdout.decode('utf-8')).strip('\n')
        self.assertTrue(
            directors == self.directors,
            msg="Expected directors '{}'. Got '{}' instead.".format(
                self.directors, directors
            )
        )

        # Movie year (release)
        bashcommand = "source {}; ec" \
                      "ho \"$(get_movie_release_year \"$(get_page_content \"" \
                      "{}\")\""")\"".format(
                                            self.imdbbash,
                                            self.imdb
                                            )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        year = str(process.stdout.decode('utf-8')).strip('\n')
        self.assertTrue(
            year == self.year,
            msg="Expected year '{}'. Got '{}' instead.".format(
                self.year, year
            )
        )

        # Movie actors
        bashcommand = "source {}; echo \"$(get_movie_actors_jsons \"$(get_mo" \
                      "vie_cast_html \"$(get_page_content \"{}\")\")\")\"".\
            format(
                    self.imdbbash,
                    htmlfullcrediturl
                    )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        actors = str(process.stdout.decode('utf-8')).strip('\n').split("|")
        self.assertTrue(
            # Compare after converting to sets, because order doesn't matter.
            # It also removes duplicates, but however unlikely, it won't affect
            # the result of the unit test.
            set(actors) == set(self.actors),
            msg="Expected actors '{}'. Got '{}' instead.".format(
                self.actors, actors
            )
        )

        # Movie genres
        bashcommand = "source {}; echo \"$(get_movie_genres \"$(get_page_con" \
                      "tent \"{}\")\")\"".format(
                                                self.imdbbash,
                                                self.imdb
                                                )
        try:
            process = self.runprocess(["bash", "-c", bashcommand])
        except subprocess.TimeoutExpired:
            print(
                self.timeouterror.format(
                    info.filename,
                    info.lineno,
                    info.function,
                    self.timeout
                )
            )
            return

        self.assertTrue(process.returncode is 0,
                        msg=self.returnerror.format(
                            bashcommand,
                            process.returncode
                        )
                        )
        genres = str(process.stdout.decode('utf-8')).strip('\n').split('\n')
        self.assertTrue(
            # Compare after converting to sets, because order doesn't matter.
            # It also removes duplicates, but however unlikely, it won't affect
            # the result of the unit test.
            set(genres) == set(self.genres),
            msg="Expected genres '{}'. Got '{}' instead.".format(
                self.genres, genres
            )
        )
