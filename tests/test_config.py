# coding=UTF-8
"""
Test Module: Test suites for the config.py module.
"""
import unittest
from moviesbackend.settings import APP


class TestConfig(unittest.TestCase):
    """
    Test Case suite: test TESTING configurations.
    """
    def test_config_exists(self):
        """
        Test APP.config setting has been instantiated.
        :return: None
        """
        self.assertFalse(isinstance(APP.config, type(None)),
                         'APP.config setting is missing!')

    def test_config_testing_set(self):
        """
        Test APP.config setting 'TESTING'.
        :return: None
        """
        self.assertTrue(APP.config['TESTING'],
                        'Config NOT set for TESTING run!')

    def test_config_testing_sqlitememory(self):
        """
        Test APP.config setting 'SQLALCHEMY_DATABASE_URI'.
        :return: None
        """
        self.assertEqual(APP.config['SQLALCHEMY_DATABASE_URI'],
                         'sqlite:///:memory:',
                         'Unit testing database NOT set to sqlite memory!')

    def test_config_testing_debug(self):
        """
        Test APP.config setting 'TESTING'.
        :return: None
        """
        self.assertTrue(APP.config['DEBUG'],
                        'Config NOT set for DEBUG!')

    def test_config_pinos_per_page(self):
        """
        Test APP.config setting 'PINOS_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['PINOS_PER_PAGE'], type(None)),
                         'Pinos per page (pagination) not set!')

    def test_config_dvds_per_page(self):
        """
        Test APP.config setting 'DVDS_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['DVDS_PER_PAGE'], type(None)),
                         'Dvds per page (pagination) not set!')

    def test_config_movies_per_page(self):
        """
        Test APP.config setting 'MOVIES_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['MOVIES_PER_PAGE'], type(None)),
                         'Movies per page (pagination) not set!')

    def test_config_actors_per_page(self):
        """
        Test APP.config setting 'MOVIES_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['MOVIES_PER_PAGE'], type(None)),
                         'Actors per page (pagination) not set!')

    def test_config_genres_per_page(self):
        """
        Test APP.config setting 'GENRES_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['GENRES_PER_PAGE'], type(None)),
                         'Genres per page (pagination) not set!')

    def test_config_subtitles_per_page(self):
        """
        Test APP.config setting 'SUBTITLES_PER_PAGE'.
        :return: None
        """
        self.assertFalse(isinstance(APP.config['SUBTITLES_PER_PAGE'],
                                    type(None)),
                         'Subtitles per page (pagination) not set!')


if __name__ == '__main__':
    unittest.main()
