# coding=UTF-8
"""
Test Module: Test suites for the models.py module.
"""
import unittest
import json
from moviesbackend.models import Pino, Dvd, Movie, Actor, MovieActor, Genre, \
    MovieGenre, MovieSubtitle, Subtitle, ExceptionAPIResourceNotFound, \
    PINONOTFOUND, \
    DVDNOTFOUND, MOVIENOTFOUND, ACTORNOTFOUND, GENRENOTFOUND, SUBTITLENOTFOUND
from tests.utils import TestPopulatedDatabase, TestEmptyDatabase, \
    UNITTESTMOVIE, NEWPINO, NONEXISTINGPINO, \
    EXISTINGPINO, NEWDVD, NONEXISTINGDVD, EXISTINGDVD, UNITTESTACTOR, \
    NEWACTOR, EXISTINGACTOR, NONEXISTINGACTOR, NEWMOVIE, NONEXISTINGMOVIE, \
    EXISTINGMOVIE, EXISTINGMOVIEACTOR, NEWMOVIEACTOR, \
    UNITTESTSUBTITLE, EXISTINGSUBTITLE, NONEXISTINGSUBTITLE, NEWSUBTITLE, \
    UNITTESTGENRE, NEWGENRE, EXISTINGGENRE, NONEXISTINGGENRE, \
    UNITTESTCHECK_CODE201, UNITTESTCHECK_CODE204, \
    UNITTESTCHECK_CODE404


class TestPinoModelPost(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models class Pino.
    """
    def test_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        pino = Pino()
        self.assertEqual(pino.dict(),
                         {'name': None},
                         'Unexpected default constructor for Pino model!')

    def test_add(self):
        """
        Test adding a new Pino object.
        :return: None
        """
        Pino.add_pino(NONEXISTINGPINO['name'])
        self.assertEqual(Pino.get_pino(EXISTINGPINO['name'])['name'],
                         EXISTINGPINO['name'],
                         'Incorrect Pino instance')
        pino = Pino(name=EXISTINGPINO['name'])
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(EXISTINGPINO),
                         pino.__repr__())

    def test_delete(self):
        """
        Test deleting a non-existing Pino object.
        :return: None
        """
        self.assertFalse(Pino.delete_pino(NONEXISTINGPINO['name']),
                         "This Pino instance shouldn't exist")


class TestPinoModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Pino.
    """
    def test_put_pino_does_not_exist(self):
        """
        Test updating a non-existing Pino object.
        :return: None
        """
        httpcode = Pino.update_pino(
            existingname=NEWPINO['name'], name=NEWPINO['name'])
        self.assertEqual(httpcode,
                         201,
                         UNITTESTCHECK_CODE201)

    def test_put_pino_exists(self):
        """
        Test updating a existing Pino object.
        :return: None
        """
        httpcode = Pino.update_pino(
            existingname=EXISTINGPINO['name'], name=NEWPINO['name'])
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)

    def test_delete(self):
        """
        Test deleting a existing Pino object.
        :return: None
        """
        self.assertTrue(Pino.delete_pino(EXISTINGPINO['name']),
                        'Failed to delete Pino instance')


class TestPinoModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Pino.
    """
    def test_patch_pino_exists(self):
        """
        Test patching a Pino object's name.
        :return: None
        """
        httpcode = Pino.patch_pino(
            existingname=EXISTINGPINO['name'], name=NEWPINO['name'])
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)

    def test_patch_pino_does_not_exist(self):
        """
        Test patching a non-existing Pino object.
        :return: None
        """
        httpcode = Pino.patch_pino(
            existingname=NEWPINO['name'], name=NEWPINO['name'])
        self.assertEqual(httpcode,
                         404,
                         UNITTESTCHECK_CODE404)


class TestDvdModelEmptyConstructor(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_dvd_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        dvd = Dvd()
        self.assertEqual(dvd.dict(),
                         {},
                         'Unexpected default constructor for Dvd model!')

    def test_add(self):
        """
        Test adding a new Pino object.
        :return: None
        """
        Pino.add_pino(NONEXISTINGPINO['name'])
        Dvd.add_dvd(
            name=NONEXISTINGDVD['name'],
            pinoname=EXISTINGPINO['name']
        )
        self.assertEqual(Dvd.get_dvd(EXISTINGDVD['name'])['name'],
                         EXISTINGDVD['name'],
                         'Incorrect Dvd instance')

        pino = Pino.query.filter_by(name=EXISTINGPINO['name']).first()
        dvd = Dvd(name=EXISTINGDVD['name'], pino_id=pino.id)
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(EXISTINGDVD),
                         dvd.__repr__())


class TestDvdModelConstructor(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_dvd_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        dvd = Dvd()
        self.assertEqual(dvd.dict(),
                         {},
                         'Unexpected constructor for Dvd model!')


class TestDvdModelGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_all_pino_dvds(self):
        """
        Test retrieving all Dvds from a Pino.
        :return: None
        """
        self.assertEqual(len(Dvd.get_all_dvds_from_pino(EXISTINGPINO['name'])),
                         1,
                         'Incorrect number of Dvds on this Pino')

    def test_all_pino_dvds_pino_does_not_exist(self):
        """
        Test retrieving all Dvds from a Pino that does not exist.
        :return: None
        """
        self.assertEqual(len(Dvd.get_all_dvds_from_pino(
            pinoname=NEWPINO['name']
        )),
                         0,
                         'Incorrect number of Dvds on this Pino')


class TestDvdModelPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_add_pino_exists(self):
        """
        Test adding a new Dvd object.
        :return: None
        """
        Dvd.add_dvd(name=NEWDVD['name'], pinoname=EXISTINGPINO['name'])
        self.assertEqual(Dvd.get_dvd(NEWDVD['name'])['name'],
                         NEWDVD['name'],
                         'Incorrect Dvd instance')

    def test_add_dvd_pino_does_not_exist(self):
        """
        Test adding a new Dvd object, but the Pino does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Dvd.add_dvd(name=NEWDVD['name'], pinoname=NEWPINO['name'])
            self.assertEqual(exc.message,
                             PINONOTFOUND.format(NEWPINO['name']),
                             'Incorrect Pino exception message')


class TestDvdModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_put_dvd_dvd_does_not_exist(self):
        """
        Test updating a non-existing Dvd object.
        :return: None
        """
        httpcode = Dvd.put_dvd(
            dvdname=NEWDVD['name'],
            newdvdname=NEWDVD['name'],
            pinoname=EXISTINGPINO['name']
        )
        self.assertEqual(httpcode,
                         201,
                         UNITTESTCHECK_CODE201)

    def test_put_dvd_dvd_exists(self):
        """
        Test updating a existing Dvd object.
        :return: None
        """
        httpcode = Dvd.put_dvd(
            dvdname=EXISTINGDVD['name'],
            newdvdname=NEWDVD['name'],
            pinoname=EXISTINGPINO['name']
        )
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)

    def test_put_dvd_pino_does_not_exist(self):
        """
        Test updating a Dvd object, but the Pino does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Dvd.put_dvd(
                dvdname=NEWDVD['name'],
                newdvdname=NEWDVD['name'],
                pinoname=NEWPINO['name']
            )
            self.assertEqual(exc.message,
                             PINONOTFOUND.format(NEWPINO['name']),
                             'Incorrect Pino exception message')


class TestDvdModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_patch_dvd_dvd_does_not_exist(self):
        """
        Test patching a non-existing Dvd.
        :return: None
        """
        httpcode = Dvd.patch_dvd(
            dvdname=NEWDVD['name'], pinoname=EXISTINGPINO['name']
        )
        self.assertEqual(httpcode,
                         404,
                         UNITTESTCHECK_CODE404)

    def test_patch_dvd_pino_does_not_exist(self):
        """
        Test patching a existing Dvd object, but the Pino does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Dvd.patch_dvd(
                dvdname=EXISTINGDVD['name'],
                pinoname=NEWPINO['name']
            )
            self.assertEqual(exc.message,
                             PINONOTFOUND.format(NEWPINO['name']),
                             'Incorrect Pino exception message')

    def test_patch(self):
        """
        Test patching a existing Dvd's pino.
        :return: None
        """
        # Fake change to the same Pino... the underlying database update
        # runs nevertheless.
        Dvd.patch_dvd(
            dvdname=EXISTINGDVD['name'],
            pinoname=EXISTINGPINO['name']
        )
        dvdobj = Dvd.get_dvd(name=EXISTINGDVD['name'])

        self.assertEqual(dvdobj['pino'],
                         EXISTINGPINO['name'],
                         'Incorrect Pino reference')


class TestDvdModelDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_delete_dvd_exists(self):
        """
        Test deleting a existing Dvd object.
        :return: None
        """
        self.assertTrue(Dvd.delete_dvd(name=EXISTINGDVD['name']),
                        'Failed to delete Dvd instance')

    def test_delete_dvd_does_not_exist(self):
        """
        Test deleting a non-existing Dvd object.
        :return: None
        """
        self.assertFalse(Dvd.delete_dvd(name=NEWDVD['name']),
                         'Failed to delete Dvd instance')


class TestMovieModelEmptyConstructor(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_movie_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        movie = Movie()
        self.assertEqual(movie.dict(),
                         {},
                         'Unexpected default constructor for Movie model')

    def test_add(self):
        """
        Test adding a new Movie object.
        :return: None
        """
        Pino.add_pino(name=NONEXISTINGPINO['name'])
        pino = Pino.query.filter_by(name=EXISTINGPINO['name']).first()
        Dvd.add_dvd(name=NONEXISTINGDVD['name'], pinoname=pino.name)
        dvd = Dvd(name=EXISTINGDVD['name'], pino_id=pino.id)
        Actor.add_actor(
            imdbid=NONEXISTINGACTOR['imdbid'],
            name=NONEXISTINGACTOR['name']
        )
        Genre.add_genre(name=NONEXISTINGGENRE['name'])
        Subtitle.add_subtitle(name=NONEXISTINGSUBTITLE['name'])
        Movie.add_movie(
            name=NONEXISTINGMOVIE['name'],
            director=NONEXISTINGMOVIE['director'],
            imdbid=NONEXISTINGMOVIE['imdbid'],
            year=NONEXISTINGMOVIE['year'],
            _format=NONEXISTINGMOVIE['format'],
            dvdname=dvd.name
        )
        MovieActor.add_movieactor(
            actorimdbid=EXISTINGACTOR['imdbid'],
            movieimdbid=EXISTINGMOVIE['imdbid']
        )
        MovieGenre.add_moviegenre(
            genrename=EXISTINGGENRE['name'],
            movieimdbid=EXISTINGMOVIE['imdbid']
        )
        MovieSubtitle.add_moviesubtitle(
            subtitlename=EXISTINGSUBTITLE['name'],
            movieimdbid=EXISTINGMOVIE['imdbid']
        )
        movie = Movie.query.filter_by(imdb=EXISTINGMOVIE['imdbid']).first()
        self.assertEqual(
            Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])['imdbid'],
            EXISTINGMOVIE['imdbid'],
            'Incorrect Movie instance'
        )
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTMOVIE),
                         movie.__repr__())


class TestMovieModelConstructor(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_movie_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        movie = Movie()
        self.assertEqual(movie.dict(),
                         {},
                         'Unexpected constructor for Movie model')


class TestMovieModelGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_movie_get(self):
        """
        Test getting an existing Movie.
        :return: None
        """
        self.assertEqual(Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid']),
                         UNITTESTMOVIE,
                         'Unexpected dictionary for Movie model')


class TestMovieModelPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_add_new(self):
        """
        Test adding a new Movie object.
        :return: None
        """
        Movie.add_movie(
            name=NEWMOVIE['name'],
            director=NEWMOVIE['director'],
            imdbid=NEWMOVIE['imdbid'],
            year=NEWMOVIE['year'],
            _format=NEWMOVIE['format'],
            dvdname=EXISTINGDVD['name']
        )
        self.assertEqual(Movie.get_movie(imdbid=NEWMOVIE['imdbid'])['name'],
                         NEWMOVIE['name'],
                         'Incorrect Movie instance')

    def test_add_movie_dvd_does_not_exist(self):
        """
        Test adding a existing Movie object, but the Dvd does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Movie.add_movie(
                name=EXISTINGMOVIE['name'],
                director=EXISTINGMOVIE['director'],
                imdbid=EXISTINGMOVIE['imdbid'],
                year=EXISTINGMOVIE['year'],
                _format=EXISTINGMOVIE['format'],
                dvdname=NEWDVD['name']
            )
            self.assertEqual(exc.message,
                             DVDNOTFOUND.format(
                                 NEWDVD['name']
                             ),
                             'Incorrect Dvd exception message')


class TestMovieModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_patch_name(self):
        """
        Test patching a existing Movie object's name.
        :return: None
        """
        patchedvalue = 'O Quinto Elemento'
        currentmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(currentmovie['name'],
                         EXISTINGMOVIE['name'],
                         'Incorrect Movie instance')
        httpcode = Movie.patch_movie(existingimdbid=EXISTINGMOVIE['imdbid'],
                                     newimdbid=None,
                                     name=patchedvalue,
                                     dvdname=None,
                                     _format=None,
                                     year=None,
                                     director=None)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(updatedmovie['name'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_format(self):
        """
        Test patching a existing Movie object's format.
        :return: None
        """
        patchedvalue = 'BluRay'
        currentmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(currentmovie['format'],
                         EXISTINGMOVIE['format'],
                         'Incorrect Movie instance')
        httpcode = Movie.patch_movie(existingimdbid=EXISTINGMOVIE['imdbid'],
                                     newimdbid=None,
                                     name=None,
                                     dvdname=None,
                                     _format=patchedvalue,
                                     year=None,
                                     director=None)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(updatedmovie['format'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_year(self):
        """
        Test patching a existing Movie object's year.
        :return: None
        """
        patchedvalue = '2986'
        currentmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(currentmovie['year'],
                         EXISTINGMOVIE['year'],
                         'Incorrect Movie instance')
        httpcode = Movie.patch_movie(existingimdbid=EXISTINGMOVIE['imdbid'],
                                     newimdbid=None,
                                     name=None,
                                     dvdname=None,
                                     _format=None,
                                     year=patchedvalue,
                                     director=None)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(updatedmovie['year'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_director(self):
        """
        Test patching a existing Movie object's director.
        :return: None
        """
        patchedvalue = 'João Camarão'
        currentmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(currentmovie['director'],
                         EXISTINGMOVIE['director'],
                         'Incorrect Movie instance')
        httpcode = Movie.patch_movie(existingimdbid=EXISTINGMOVIE['imdbid'],
                                     newimdbid=None,
                                     name=None,
                                     dvdname=None,
                                     _format=None,
                                     year=None,
                                     director=patchedvalue)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedmovie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(updatedmovie['director'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_movie_movie_does_not_exist(self):
        """
        Test patching a non-existing Movie object.
        :return: None
        """
        self.assertEqual(404,
                         Movie.patch_movie(existingimdbid=NEWMOVIE['imdbid'],
                                           newimdbid=None,
                                           name=None,
                                           dvdname=None,
                                           _format=None,
                                           year=None,
                                           director=None),
                         UNITTESTCHECK_CODE404
                         )

    def test_patch_movie_dvd_does_not_exist(self):
        """
        Test patching a existing Movie object, but the Dvd does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Movie.patch_movie(existingimdbid=EXISTINGMOVIE['imdbid'],
                              newimdbid=None,
                              name=None,
                              dvdname=NEWDVD['name'],
                              _format=None,
                              year=None,
                              director=None)
            self.assertEqual(exc.message,
                             DVDNOTFOUND.format(NEWDVD['name']),
                             'Incorrect Dvd exception message')


class TestMovieModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_put_movie_dvd_does_not_exist(self):
        """
        Test updating a existing Movie object, but the Dvd does not exist.
        :return: None
        """
        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            Movie.put_movie(existingimdbid=NEWMOVIE['imdbid'],
                            imdbid=NEWMOVIE['imdbid'],
                            name=NEWMOVIE['name'],
                            dvdname=NEWDVD['name'],
                            _format=NEWMOVIE['format'],
                            year=NEWMOVIE['year'],
                            director=NEWMOVIE['director'])
            self.assertEqual(exc.message,
                             DVDNOTFOUND.format(NEWDVD['name']),
                             'Incorrect Movie exception message')

    def test_update_movie_dvd_exists(self):
        """
        Test updating a existing Movie object.
        :return: None
        """
        self.assertEqual(204,
                         Movie.put_movie(existingimdbid=
                                         EXISTINGMOVIE['imdbid'],
                                         imdbid=EXISTINGMOVIE['imdbid'],
                                         name=NEWMOVIE['name'],
                                         dvdname=EXISTINGDVD['name'],
                                         _format=NEWMOVIE['format'],
                                         year=NEWMOVIE['year'],
                                         director=NEWMOVIE['director']),
                         UNITTESTCHECK_CODE204
                         )

    def test_update_movie_movie_does_not_exist(self):
        """
        Test updating a non-existing Movie object.
        :return: None
        """
        self.assertEqual(201,
                         Movie.put_movie(existingimdbid=NEWMOVIE['imdbid'],
                                         imdbid=NEWMOVIE['imdbid'],
                                         name=NEWMOVIE['name'],
                                         dvdname=EXISTINGDVD['name'],
                                         _format=NEWMOVIE['format'],
                                         year=NEWMOVIE['year'],
                                         director=NEWMOVIE['director']),
                         UNITTESTCHECK_CODE201
                         )


class TestMovieModelCountPerDvd(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_all_dvd_movies(self):
        """
        Test retrieving all Movies from a Dvd.
        :return: None
        """
        self.assertEqual(len(Movie.get_all_movies_from_dvd(
            dvdname=EXISTINGDVD['name']
        )),
                         1,
                         'Incorrect number of movies on this Dvd')


class TestMovieModelDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_delete(self):
        """
        Test deleting a existing Movie object.
        :return: None
        """
        self.assertTrue(Movie.delete_movie(imdbid=EXISTINGMOVIE['imdbid']),
                        'Failed to delete Movie instance')


class TestActorModelEmptyConstructor(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_actor_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        actor = Actor()
        self.assertEqual(actor.dict(),
                         {},
                         'Unexpected default constructor for Actor model')


class TestActorModelConstructor(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_actor_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        self.assertEqual(Actor.get_actor(imdbid=EXISTINGACTOR['imdbid']),
                         UNITTESTACTOR,
                         'Unexpected default constructor for Actor model')


class TestActorModelGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_actors_get_all(self):
        """
        Test retrieving all Movies from a Dvd.
        :return: None
        """
        self.assertEqual(len(Actor.get_all_actors(page=1)),
                         0,
                         'Incorrect number of Actors')

    def test_actors_get(self):
        """
        Test retrieving an Actor.
        :return: None
        """
        self.assertEqual(Actor.get_actor(imdbid=NONEXISTINGACTOR['imdbid']),
                         {},
                         'Incorrect Actor instance')

    def test_actors_add(self):
        """
        Test adding a new Actor object.
        :return: None
        """
        Actor.add_actor(
            name=NONEXISTINGACTOR['name'],
            imdbid=NONEXISTINGACTOR['imdbid']
        )
        self.assertEqual(Actor.get_actor(EXISTINGACTOR['imdbid'])['imdbid'],
                         EXISTINGACTOR['imdbid'],
                         'Incorrect Actor instance')
        actor = Actor.query.filter_by(
            name=EXISTINGACTOR['name'],
            imdbid=EXISTINGACTOR['imdbid']
        ).first()
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTACTOR),
                         actor.__repr__(),
                         "Actor representation does not correspond to the"
                         " JSON object")


class TestActorModelGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_actors_get_all(self):
        """
        Test retrieving all Movies from a Dvd.
        :return: None
        """
        self.assertEqual(len(Actor.get_all_actors(page=1)),
                         1,
                         'Incorrect number of Actors')

    def test_actors_get(self):
        """
        Test retrieving an Actor.
        :return: None
        """
        self.assertEqual(Actor.get_actor(imdbid=EXISTINGACTOR['imdbid']),
                         UNITTESTACTOR,
                         'Incorrect Actor instance')


class TestActorModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_update_actor_actor_exists(self):
        """
        Test updating a existing Actor object.
        :return: None
        """
        self.assertEqual(204,
                         Actor.put_actor(name=EXISTINGACTOR['name'],
                                         existingimdbid=
                                         EXISTINGACTOR['imdbid'],
                                         newimdbid=NEWACTOR['imdbid']),
                         UNITTESTCHECK_CODE204
                         )

    def test_update_actor_actor_does_not_exist(self):
        """
        Test updating a non-existing Actor object.
        :return: None
        """
        self.assertEqual(201,
                         Actor.put_actor(name=NEWACTOR['name'],
                                         existingimdbid=NEWACTOR['imdbid'],
                                         newimdbid=NEWACTOR['imdbid']
                                         ),
                         UNITTESTCHECK_CODE201
                         )


class TestActorModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_patch_name(self):
        """
        Test patching a existing Actor object's name.
        :return: None
        """
        patchedvalue = 'O Quinto Elemento'
        currentactor = Actor.get_actor(imdbid=EXISTINGACTOR['imdbid'])
        self.assertEqual(currentactor['name'],
                         EXISTINGACTOR['name'],
                         'Incorrect Actor instance')
        httpcode = Actor.patch_actor(existingimdbid=EXISTINGACTOR['imdbid'],
                                     newimdbid=None,
                                     name=patchedvalue)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedactor = Actor.get_actor(imdbid=EXISTINGACTOR['imdbid'])
        self.assertEqual(updatedactor['name'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_imdbid(self):
        """
        Test patching a existing Actor object's imdbid.
        :return: None
        """
        patchedvalue = NEWACTOR['imdbid']
        currentactor = Actor.get_actor(imdbid=EXISTINGACTOR['imdbid'])
        self.assertEqual(currentactor['imdbid'],
                         EXISTINGACTOR['imdbid'],
                         'Incorrect Actor instance')
        httpcode = Actor.patch_actor(existingimdbid=EXISTINGACTOR['imdbid'],
                                     newimdbid=patchedvalue,
                                     name=None)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedactor = Actor.get_actor(imdbid=NEWACTOR['imdbid'])
        self.assertEqual(updatedactor['imdbid'],
                         patchedvalue,
                         'Incorrect Movie instance')

    def test_patch_actor_actor_does_not_exist(self):
        """
        Test patching a non-existing Actor object.
        :return: None
        """
        self.assertEqual(404,
                         Actor.patch_actor(existingimdbid=NEWACTOR['imdbid'],
                                           newimdbid=None,
                                           name=None),
                         UNITTESTCHECK_CODE404
                         )


class TestActorModelDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_delete_actor_exists(self):
        """
        Test deleting a existing Dvd object.
        :return: None
        """
        self.assertTrue(Actor.delete_actor(imdbid=EXISTINGACTOR['imdbid']),
                        'Failed to delete Actor instance')

    def test_delete_actor_does_not_exist(self):
        """
        Test deleting a non-existing Actor object.
        :return: None
        """
        self.assertFalse(Actor.delete_actor(imdbid=NEWACTOR['imdbid']),
                         'Failed to delete Actor instance')


class TestMovieActorModelPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_add_new(self):
        """
        Test adding a new Movie Actor object.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['actors'],
                         [EXISTINGMOVIEACTOR])

        Actor.add_actor(name=NEWACTOR['name'], imdbid=NEWACTOR['imdbid'])
        actor = Actor.get_actor(imdbid=NEWACTOR['imdbid'])
        MovieActor.add_movieactor(
            actorimdbid=actor['imdbid'],
            movieimdbid=movie['imdbid']
        )

        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['actors'],
                         [EXISTINGMOVIEACTOR, NEWMOVIEACTOR])

    def test_add_movieactor_movie_does_not_exist(self):
        """
        Test adding a existing Movie Actor object, but the Movie does not
        exist.
        :return: None
        """
        actor = Actor.get_actor(imdbid=EXISTINGACTOR['imdbid'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieActor.add_movieactor(
                actorimdbid=actor['imdbid'],
                movieimdbid=NEWMOVIE['imdbid']
            )
            self.assertEqual(exc.message,
                             MOVIENOTFOUND.format(
                                 NEWMOVIE['imdbid']
                             ),
                             'Incorrect Movie Actor exception message')

    def test_add_movieactor_actor_does_not_exist(self):
        """
        Test adding a existing Movie Actor object, but the Actor does not
        exist.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieActor.add_movieactor(
                actorimdbid=NEWACTOR['imdbid'],
                movieimdbid=movie['imdbid']
            )
            self.assertEqual(exc.message,
                             ACTORNOTFOUND.format(
                                 NEWACTOR['imdbid']
                             ),
                             'Incorrect Movie Actor exception message')


class TestGenreModelEmptyConstructor(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """

    def test_genre_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        genre = Genre()
        self.assertEqual(genre.dict(),
                         {},
                         'Unexpected default constructor for Genre model')

    def test_add(self):
        """
        Test adding a new Genre object.
        :return: None
        """
        Genre.add_genre(
            name=NONEXISTINGGENRE['name']
        )
        genre = Genre.query.filter_by(name=EXISTINGGENRE['name']).first()
        self.assertEqual(
            Genre.get_genre(name=EXISTINGGENRE['name'])['name'],
            EXISTINGGENRE['name'],
            'Incorrect Genre instance'
        )
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTGENRE),
                         genre.__repr__())


class TestGenreModelConstructor(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_genre_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        self.assertEqual(Genre.get_genre(name=EXISTINGGENRE['name']),
                         UNITTESTGENRE,
                         'Unexpected default constructor for Genre model')


class TestGenreModelGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_genres_get_all(self):
        """
        Test retrieving all Genres.
        :return: None
        """
        self.assertEqual(len(Genre.get_all_genres(page=1)),
                         0,
                         'Incorrect number of Genres')

    def test_genres_get(self):
        """
        Test retrieving an Genre.
        :return: None
        """
        self.assertEqual(Genre.get_genre(name=NONEXISTINGGENRE['name']),
                         {},
                         'Incorrect Genre instance')

    def test_genres_add(self):
        """
        Test adding a new Genre object.
        :return: None
        """
        Genre.add_genre(
            name=NONEXISTINGGENRE['name']
        )
        self.assertEqual(Genre.get_genre(EXISTINGGENRE['name'])['name'],
                         EXISTINGGENRE['name'],
                         'Incorrect Genre instance')
        genre = Genre.query.filter_by(
            name=EXISTINGGENRE['name']
        ).first()
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTGENRE),
                         genre.__repr__(),
                         "Genre representation does not correspond to the"
                         " JSON object")


class TestGenreModelGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_genres_get_all(self):
        """
        Test retrieving all Genres.
        :return: None
        """
        self.assertEqual(len(Genre.get_all_genres(page=1)),
                         1,
                         'Incorrect number of Genres')


class TestGenreModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_update_genre_genre_exists(self):
        """
        Test updating a existing Genre object.
        :return: None
        """
        self.assertEqual(204,
                         Genre.put_genre(name=EXISTINGGENRE['name'],
                                         newname=NEWGENRE['name']),
                         UNITTESTCHECK_CODE204
                         )

    def test_update_genre_genre_does_not_exist(self):
        """
        Test updating a non-existing Genre object.
        :return: None
        """
        self.assertEqual(201,
                         Genre.put_genre(name=NEWGENRE['name'],
                                         newname=NEWGENRE['name']
                                         ),
                         UNITTESTCHECK_CODE201
                         )


class TestGenreModelDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_delete_genre_exists(self):
        """
        Test deleting a existing Genre object.
        :return: None
        """
        self.assertTrue(Genre.delete_genre(name=EXISTINGGENRE['name']),
                        'Failed to delete Genre instance')

    def test_delete_genre_does_not_exist(self):
        """
        Test deleting a non-existing Genre object.
        :return: None
        """
        self.assertFalse(Genre.delete_genre(name=NEWGENRE['name']),
                         'Failed to delete Actor instance')


class TestGenreModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_patch_name(self):
        """
        Test patching a existing Genre object's name.
        :return: None
        """
        patchedvalue = 'Porn'
        currentgenre = Genre.get_genre(name=EXISTINGGENRE['name'])
        self.assertEqual(currentgenre['name'],
                         EXISTINGGENRE['name'],
                         'Incorrect Genre instance')
        httpcode = Genre.patch_genre(name=EXISTINGGENRE['name'],
                                     newname=patchedvalue)
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedgenre = Genre.get_genre(name=patchedvalue)
        self.assertEqual(updatedgenre['name'],
                         patchedvalue,
                         'Incorrect Genre instance')

    def test_patch_genre_genre_does_not_exist(self):
        """
        Test patching a non-existing Genre object.
        :return: None
        """
        self.assertEqual(404,
                         Genre.patch_genre(name=NEWGENRE['name'],
                                           newname=None),
                         UNITTESTCHECK_CODE404
                         )


class TestMovieGenreModelPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_add_new(self):
        """
        Test adding a new Movie Genre object.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['genres'],
                         [EXISTINGGENRE])

        Genre.add_genre(name=NEWGENRE['name'])
        genre = Genre.get_genre(name=NEWGENRE['name'])
        MovieGenre.add_moviegenre(
            genrename=genre['name'],
            movieimdbid=movie['imdbid']
        )

        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['genres'],
                         [EXISTINGGENRE, NEWGENRE])

    def test_add_moviegenre_movie_does_not_exist(self):
        """
        Test adding a existing Movie Genre object, but the Movie does not
        exist.
        :return: None
        """
        genre = Genre.get_genre(name=EXISTINGGENRE['name'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieGenre.add_moviegenre(
                genrename=genre['name'],
                movieimdbid=NEWMOVIE['imdbid']
            )
            self.assertEqual(exc.message,
                             MOVIENOTFOUND.format(
                                 NEWMOVIE['imdbid']
                             ),
                             'Incorrect Movie Genre exception message')

    def test_add_moviegenre_genre_does_not_exist(self):
        """
        Test adding a existing Movie Genre object, but the Genre does not
        exist.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieGenre.add_moviegenre(
                genrename=NEWGENRE['name'],
                movieimdbid=movie['imdbid']
            )
            self.assertEqual(exc.message,
                             GENRENOTFOUND.format(
                                 NEWGENRE['name']
                             ),
                             'Incorrect Movie Actor exception message')














class TestSubtitleModelEmptyConstructor(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """

    def test_subtitle_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        subtitle = Subtitle()
        self.assertEqual(subtitle.dict(),
                         {},
                         'Unexpected default constructor for Subtitle model')

    def test_add(self):
        """
        Test adding a new Subtitle object.
        :return: None
        """
        Subtitle.add_subtitle(
            name=NONEXISTINGSUBTITLE['name']
        )
        subtitle = Subtitle.query.filter_by(
            name=EXISTINGSUBTITLE['name']).first()
        self.assertEqual(
            Subtitle.get_subtitle(name=EXISTINGSUBTITLE['name'])['name'],
            EXISTINGSUBTITLE['name'],
            'Incorrect Subtitle instance'
        )
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTSUBTITLE),
                         subtitle.__repr__())


class TestSubtitleModelConstructor(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_subtitle_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        self.assertEqual(Subtitle.get_subtitle(name=EXISTINGSUBTITLE['name']),
                         UNITTESTSUBTITLE,
                         'Unexpected default constructor for Subtitle model')


class TestSubtitleModelGetEmpty(TestEmptyDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_subtitles_get_all(self):
        """
        Test retrieving all Subtitles.
        :return: None
        """
        self.assertEqual(len(Subtitle.get_all_subtitles(page=1)),
                         0,
                         'Incorrect number of Subtitles')

    def test_subtitles_get(self):
        """
        Test retrieving an Subtitle.
        :return: None
        """
        self.assertEqual(Subtitle.get_subtitle(
            name=NONEXISTINGSUBTITLE['name']),
                         {},
                         'Incorrect Subtitle instance')

    def test_subtitles_add(self):
        """
        Test adding a new Subtitle object.
        :return: None
        """
        Subtitle.add_subtitle(
            name=NONEXISTINGSUBTITLE['name']
        )
        self.assertEqual(Subtitle.get_subtitle(
            EXISTINGSUBTITLE['name'])['name'],
                         EXISTINGSUBTITLE['name'],
                         'Incorrect Subtitle instance')
        subtitle = Subtitle.query.filter_by(
            name=EXISTINGSUBTITLE['name']
        ).first()
        # Test JSON object representation (from a dict unit test object).
        self.assertEqual(json.dumps(UNITTESTSUBTITLE),
                         subtitle.__repr__(),
                         "Subtitle representation does not correspond to the"
                         " JSON object")


class TestSubtitleModelGet(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_subtitles_get_all(self):
        """
        Test retrieving all Subtitles.
        :return: None
        """
        self.assertEqual(len(Subtitle.get_all_subtitles(page=1)),
                         1,
                         'Incorrect number of Subtitles')


class TestSubtitleModelPut(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_update_subtitle_subtitle_exists(self):
        """
        Test updating a existing Subtitle object.
        :return: None
        """
        self.assertEqual(204,
                         Subtitle.put_subtitle(
                             name=EXISTINGSUBTITLE['name'],
                             newname=NEWSUBTITLE['name']
                         ),
                         UNITTESTCHECK_CODE204
                         )

    def test_update_subtitle_subtitle_does_not_exist(self):
        """
        Test updating a non-existing Subtitle object.
        :return: None
        """
        self.assertEqual(201,
                         Subtitle.put_subtitle(
                             name=NEWSUBTITLE['name'],
                             newname=NEWSUBTITLE['name']
                         ),
                         UNITTESTCHECK_CODE201
                         )


class TestSubtitleModelDelete(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models class Dvd.
    """
    def test_delete_subtitle_exists(self):
        """
        Test deleting a existing Subtitle object.
        :return: None
        """
        self.assertTrue(
            Subtitle.delete_subtitle(name=EXISTINGSUBTITLE['name']),
            'Failed to delete Subtitle instance'
        )

    def test_delete_subtitle_does_not_exist(self):
        """
        Test deleting a non-existing Subtitle object.
        :return: None
        """
        self.assertFalse(Subtitle.delete_subtitle(name=NEWSUBTITLE['name']),
                         'Failed to delete Subtitle instance')


class TestSubtitleModelPatch(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_patch_name(self):
        """
        Test patching a existing Subtitle object's name.
        :return: None
        """
        patchedvalue = 'Polish'
        currentsubtitle = Subtitle.get_subtitle(name=EXISTINGSUBTITLE['name'])
        self.assertEqual(currentsubtitle['name'],
                         EXISTINGSUBTITLE['name'],
                         'Incorrect Subtitle instance')
        httpcode = Subtitle.patch_subtitle(
            name=EXISTINGSUBTITLE['name'],
            newname=patchedvalue
        )
        self.assertEqual(httpcode,
                         204,
                         UNITTESTCHECK_CODE204)
        updatedsubtitle = Subtitle.get_subtitle(name=patchedvalue)
        self.assertEqual(updatedsubtitle['name'],
                         patchedvalue,
                         'Incorrect Subtitle instance')

    def test_patch_subtitle_subtitle_does_not_exist(self):
        """
        Test patching a non-existing Subtitle object.
        :return: None
        """
        self.assertEqual(404,
                         Subtitle.patch_subtitle(
                             name=NEWSUBTITLE['name'],
                             newname=None
                         ),
                         UNITTESTCHECK_CODE404
                         )


class TestMovieSubtitleModelPost(TestPopulatedDatabase):
    """
    Test Case suite: test moviebackend.models module features.
    """
    def test_add_new(self):
        """
        Test adding a new Movie Subtitle object.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['subtitles'],
                         [EXISTINGSUBTITLE])

        Subtitle.add_subtitle(name=NEWSUBTITLE['name'])
        subtitle = Subtitle.get_subtitle(name=NEWSUBTITLE['name'])
        MovieSubtitle.add_moviesubtitle(
            subtitlename=subtitle['name'],
            movieimdbid=movie['imdbid']
        )

        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])
        self.assertEqual(movie['subtitles'],
                         [EXISTINGSUBTITLE, NEWSUBTITLE])

    def test_add_moviesubtitle_movie_does_not_exist(self):
        """
        Test adding a existing Movie Subtitle object, but the Movie does not
        exist.
        :return: None
        """
        subtitle = Subtitle.get_subtitle(name=EXISTINGSUBTITLE['name'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieSubtitle.add_moviesubtitle(
                subtitlename=subtitle['name'],
                movieimdbid=NEWMOVIE['imdbid']
            )
            self.assertEqual(exc.message,
                             MOVIENOTFOUND.format(
                                 NEWMOVIE['name']
                             ),
                             'Incorrect Movie exception message')

    def test_add_moviesubtitle_subtitle_does_not_exist(self):
        """
        Test adding a existing Movie Subtitle object, but the Subtitle does not
        exist.
        :return: None
        """
        movie = Movie.get_movie(imdbid=EXISTINGMOVIE['imdbid'])

        with self.assertRaises(ExceptionAPIResourceNotFound) as exc:
            MovieSubtitle.add_moviesubtitle(
                subtitlename=NEWSUBTITLE['name'],
                movieimdbid=movie['imdbid']
            )
            self.assertEqual(exc.message,
                             SUBTITLENOTFOUND.format(
                                 NEWSUBTITLE['name']
                             ),
                             'Incorrect Movie Subtitle exception message')


if __name__ == '__main__':
    unittest.main()
