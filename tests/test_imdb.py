# coding=UTF-8
"""
Test Module: Test suites for the ../data/MySqlDatabase/imdb_v2.0.sh bash script
"""
from unittest import TestCase
from tests.utils import TestImdbBashScript


#
# Bash script testing - parsing and returning data from iMDB HTML pages.
# https://www.imdb.com
#
class TestAliens(TestCase, TestImdbBashScript):
    """
    Test Case suite: Aliens (1986).
    """
    def setUp(self):
        """
        Initialize shared variables.
        :return:
        """
        self.imdbid = "tt0090605"
        self.imdb = "https://www.imdb.com/title/{}".format(self.imdbid)
        # iMDB will return the title translated to the idiom using geolocation
        # (from your internet IP).
        self.title = "Aliens, o Resgate"
        # Likewise, the release year for you location might be different than
        # USA's release year.
        self.year = "1986"
        self.directors = "James Cameron"
        self.genres = [
            "Action",
            "Adventure",
            "Sci-Fi",
            "Thriller"
        ]
        self.actors = [
            '{"name":"Sigourney Weaver","imdbid":"0000244"}',
            '{"name":"Carrie Henn","imdbid":"0001343"}',
            '{"name":"Michael Biehn","imdbid":"0000299"}',
            '{"name":"Paul Reiser","imdbid":"0001663"}',
            '{"name":"Lance Henriksen","imdbid":"0000448"}',
            '{"name":"Bill Paxton","imdbid":"0000200"}',
            '{"name":"William Hope","imdbid":"0394054"}',
            '{"name":"Jenette Goldstein","imdbid":"0001280"}',
            '{"name":"Al Matthews","imdbid":"0559922"}',
            '{"name":"Mark Rolston","imdbid":"0001679"}',
            '{"name":"Ricco Ross","imdbid":"0743768"}',
            '{"name":"Colette Hiller","imdbid":"0384876"}',
            '{"name":"Daniel Kash","imdbid":"0440511"}',
            '{"name":"Cynthia Dale Scott","imdbid":"0778992"}',
            '{"name":"Tip Tipping","imdbid":"0864150"}',
            '{"name":"Trevor Steedman","imdbid":"0824365"}',
            '{"name":"Paul Maxwell","imdbid":"0561792"}',
            '{"name":"Valerie Colgan","imdbid":"0171438"}',
            '{"name":"Alan Polonsky","imdbid":"0689797"}',
            '{"name":"Alibe Parsons","imdbid":"0019544"}',
            '{"name":"Blain Fairman","imdbid":"0265629"}',
            '{"name":"Barbara Coles","imdbid":"0171292"}',
            '{"name":"Carl Toop","imdbid":"0867562"}',
            '{"name":"John Lees","imdbid":"0498727"}',
            '{"name":"William Armstrong","imdbid":"0035628"}',
            '{"name":"Jay Benedict","imdbid":"0070779"}',
            '{"name":"Holly De Jong","imdbid":"0208999"}',
            '{"name":"Jill Goldston","imdbid":"0326349"}',
            '{"name":"Christopher Henn","imdbid":"0377158"}',
            '{"name":"Elizabeth Inglis","imdbid":"0408876"}',
            '{"name":"Mac McDonald","imdbid":"0567881"}',
            '{"name":"Stuart Milligan","imdbid":"0589645"}',
            '{"name":"Bob Sherman","imdbid":"0792384"}',
            '{"name":"Chris Webb","imdbid":"0916051"}',
            '{"name":"Tom Woodruff Jr.","imdbid":"0940430"}'
        ]
        super(TestAliens, self).setUp()


class TestTheThing(TestCase, TestImdbBashScript):
    """
    Test Case suite: The Thing (1982).
    """
    def setUp(self):
        """
        Initialize shared variables.
        :return:
        """
        self.imdbid = "tt0084787"
        self.imdb = "https://www.imdb.com/title/{}".format(self.imdbid)
        # iMDB will return the title translated to the idiom using geolocation
        # (from your internet IP).
        self.title = "O Enigma de Outro Mundo"
        # Likewise, the release year for you location might be different than
        # USA's release year.
        self.year = "1983"
        self.directors = "John Carpenter"
        self.genres = [
            "Horror",
            "Mystery",
            "Sci-Fi"
        ]
        self.actors = [
            '{"name":"Kurt Russell","imdbid":"0000621"}',
            '{"name":"Wilford Brimley","imdbid":"0000979"}',
            '{"name":"T.K. Carter","imdbid":"0141953"}',
            '{"name":"David Clennon","imdbid":"0166359"}',
            '{"name":"Keith David","imdbid":"0202966"}',
            '{"name":"Richard Dysart","imdbid":"0246004"}',
            '{"name":"Charles Hallahan","imdbid":"0356251"}',
            '{"name":"Peter Maloney","imdbid":"0540595"}',
            '{"name":"Richard Masur","imdbid":"0557956"}',
            '{"name":"Donald Moffat","imdbid":"0595567"}',
            '{"name":"Joel Polis","imdbid":"0689177"}',
            '{"name":"Thomas G. Waites","imdbid":"0906640"}',
            '{"name":"Norbert Weisser","imdbid":"0919256"}',
            '{"name":"Larry Franco","imdbid":"0290581"}',
            '{"name":"Nate Irwin","imdbid":"0410429"}',
            '{"name":"William Zeman","imdbid":"0954730"}',
            '{"name":"Adrienne Barbeau","imdbid":"0000105"}',
            '{"name":"John Carpenter","imdbid":"0000118"}',
            '{"name":"Jed","imdbid":"1058600"}'
        ]
        super(TestTheThing, self).setUp()
