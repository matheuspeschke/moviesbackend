# coding=UTF-8
"""
Test Module: Test suites for the validations.py module.
"""
import json
from moviesbackend.models import Pino, Dvd, Actor, Genre
from moviesbackend.validations import valid_pino, valid_dvd, \
    valid_movie_for_post, valid_movie_for_patch, valid_actor, \
    valid_actor_for_put, valid_movieactor, valid_genre
from tests.utils import TestPopulatedDatabase, DB, NEWMOVIE, \
    NEWACTOR, UNITTESTGENRE, NEWGENRE


class TestValidations(TestPopulatedDatabase):
    """
    Test Case suite: test validations.py valid_pino function.
    """
    def test_invalid_default_pino_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        pino = Pino()
        self.assertFalse(valid_pino(pino),
                         'The default Pino constructor must be an invalid Pino'
                         ' object')

    def test_valid_pino_dict(self):
        """
        Test a valid Pino object.
        :return: None
        """
        pinos = DB.session.query(Pino)
        pino = pinos.filter_by(name='00000001')
        self.assertTrue(valid_pino(pino.all()[0].dict()),
                        "This Pino's dict object should be valid")

    def test_invalid_default_dvd_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        dvd = Dvd()
        self.assertFalse(valid_dvd(dvd),
                         'The default Pino constructor must be an invalid Dvd'
                         ' object')

    def test_valid_dvd_dict(self):
        """
        Test a valid Dvd object.
        :return: None
        """
        dvds = DB.session.query(Dvd)
        dvd = dvds.filter_by(name='00000001')
        self.assertTrue(valid_dvd(dvd.all()[0].dict()),
                        "This Dvd's dict object should be valid")

    def test_valid_movie_for_post(self):
        """
        Test a valid Movie object.
        :return: None
        """
        self.assertTrue(valid_movie_for_post(NEWMOVIE),
                        "This Movie's dict object should be valid")

    def test_invalid_movie_for_post_001(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = {
            'imdbid': NEWMOVIE['imdbid'],
            'name': NEWMOVIE['name'],
            'format': NEWMOVIE['format'],
            'year': NEWMOVIE['year'],
            'director': NEWMOVIE['director'],
            'actors': []
        }

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_002(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = {
            'dvd': NEWMOVIE['dvd'],
            'name': NEWMOVIE['name'],
            'format': NEWMOVIE['format'],
            'year': NEWMOVIE['year'],
            'director': NEWMOVIE['director'],
            'actors': []
        }

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_003(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = {
            'imdbid': NEWMOVIE['imdbid'],
            'dvd': NEWMOVIE['dvd'],
            'name': NEWMOVIE['name'],
            'year': NEWMOVIE['year'],
            'director': NEWMOVIE['director'],
            'actors': []
        }

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_004(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = {
            'imdbid': NEWMOVIE['imdbid'],
            'format': NEWMOVIE['format'],
            'dvd': NEWMOVIE['dvd'],
            'name': NEWMOVIE['name'],
            'year': NEWMOVIE['year'],
            'director': NEWMOVIE['director']
        }

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_005(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = {
            'WHAT': NEWMOVIE['imdbid'],
            'THE': NEWMOVIE['dvd'],
            'FUCK': NEWMOVIE['name'],
            'IS': NEWMOVIE['year'],
            'THIS?': NEWMOVIE['director']
        }

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_string(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = 'meu filme favorito'

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_post_nonetype(self):
        """
        Test a valid Movie object.
        :return: None
        """
        requestdata = None

        self.assertFalse(valid_movie_for_post(requestdata),
                         "This Movie's dict object should NOT be valid")

    def test_invalid_movie_for_patch_string(self):
        """
        Testing validation rule:
        1. movie is a dictionary
        :return: None
        """
        requestdata = 'meu filme favorito'

        self.assertFalse(valid_movie_for_patch(requestdata),
                         "This object should NOT be valid for a PATCH method.")

    def test_invalid_movie_for_patch_nonetype(self):
        """
        Testing validation rule:
        1. movie is a dictionary
        :return: None
        """
        requestdata = None

        self.assertFalse(valid_movie_for_patch(requestdata),
                         "This object should NOT be valid for a PATCH method.")

    def test_invalid_movie_for_patch_003(self):
        """
        Testing validation rule:
        2. movie has only valid keys for a Movie instance
        :return: None
        """
        requestdata = {
            'THIS': NEWMOVIE['imdbid'],
            'MAKES': NEWMOVIE['dvd'],
            'NO': NEWMOVIE['name'],
            'SENSE': NEWMOVIE['format'],
            'AT': NEWMOVIE['year'],
            'ALL': NEWMOVIE['director']
        }

        self.assertFalse(valid_movie_for_patch(requestdata),
                         "This object should NOT be valid for a PATCH method.")

    def test_invalid_movie_for_patch_004(self):
        """
        Testing validation rule:
        2. movie has only valid keys for a Movie instance
        :return: None
        """
        movie = {
            'imdb': NEWMOVIE['imdbid'],
            'dvd': NEWMOVIE['dvd'],
            'name': NEWMOVIE['name'],
            'format': NEWMOVIE['format'],
            'year': NEWMOVIE['year'],
            'oooooops': NEWMOVIE['director']
        }

        self.assertFalse(valid_movie_for_patch(json.dumps(movie)),
                         "This object should NOT be valid for a PATCH method.")

    def test_invalid_movie_for_patch_emptydict(self):
        """
        Testing validation rule:
        3. movie has the imdbid key and at least one other key
        :return: None
        """
        requestdata = {}

        self.assertFalse(valid_movie_for_patch(requestdata),
                         "This object should NOT be valid for a PATCH method.")

    def test_invalid_movie_for_patch_emptyarray(self):
        """
        Testing validation rule:
        3. movie has the imdbid key and at least one other key
        :return: None
        """
        requestdata = []

        self.assertFalse(valid_movie_for_patch(requestdata),
                         "This object should NOT be valid for a PATCH method.")

    def test_valid_movie_for_patch_001(self):
        """
        Testing validation rule:
        3. movie has the imdbid key and at least one other key
        :return: None
        """
        requestdata = {
            'imdb': NEWMOVIE['imdbid'],
            'dvd': NEWMOVIE['dvd']
        }

        self.assertTrue(valid_movie_for_patch(requestdata),
                        "This object should be valid for a PATCH method.")

    def test_valid_movie_for_patch_002(self):
        """
        Test a valid Movie object for a PATCH method.
        :return: None
        """
        requestdata = {
            'imdb': NEWMOVIE['imdbid'],
            'dvd': NEWMOVIE['dvd'],
            'name': NEWMOVIE['name'],
            'format': NEWMOVIE['format'],
            'year': NEWMOVIE['year'],
            'director': NEWMOVIE['director']
        }

        self.assertTrue(valid_movie_for_patch(requestdata),
                        "This object should be valid for a PATCH method.")

    def test_valid_movie_for_patch_imdbid(self):
        """
        Testing validation rule:
        3. movie has at least one key
        :return: None
        """
        requestdata = {
            'imdb': NEWMOVIE['imdbid']
        }

        self.assertTrue(valid_movie_for_patch(requestdata),
                        "This object should be valid for a PATCH method.")

    def test_valid_actor_dict(self):
        """
        Test a valid Actor object.
        :return: None
        """
        actors = DB.session.query(Actor)
        self.assertTrue(valid_actor(actors.all()[0].dict()),
                        "This Actor's dict object should be valid")

    def test_valid_actor_for_put(self):
        """
        Testing validation rule:
        3. movie has at least one key
        :return: None
        """
        requestdata = {
            'imdbid': NEWACTOR['imdbid'],
            'name': NEWACTOR['name']
        }

        self.assertTrue(valid_actor_for_put(requestdata),
                        "This object should be valid for a PUT method.")

    def test_valid_movieactor_for_post(self):
        """
        Test a valid MovieActor object.
        :return: None
        """
        requestdata = {
            'actorimdbid': NEWACTOR['imdbid'],
            'movieimdbid': NEWMOVIE['imdbid']
        }

        self.assertTrue(valid_movieactor(requestdata),
                        "This MovieActor's dict object should be valid")

    def test_invalid_movieactor_for_post(self):
        """
        Test a valid MovieActor object.
        :return: None
        """
        requestdata = {
            'movieimdbid': NEWMOVIE['imdbid']
        }

        self.assertFalse(valid_movieactor(requestdata),
                         "This MovieActor's dict object should NOT be valid")

    def test_invalid_default_genre_constructor(self):
        """
        Test default constructor values.
        :return: None
        """
        genre = Genre()
        self.assertFalse(valid_genre(genre),
                         'The default Genre constructor must be an invalid Ge'
                         'nre object')

    def test_valid_genre_dict(self):
        """
        Test a valid Genre object.
        :return: None
        """
        genres = DB.session.query(Genre)
        genre = genres.filter_by(name=UNITTESTGENRE['name'])
        self.assertTrue(valid_genre(genre.all()[0].dict()),
                        "This Genre's dict object should be valid")

    def test_valid_genre(self):
        """
        Test a valid Genre object.
        :return: None
        """
        requestdata = {
            'name': NEWGENRE['name']
        }

        self.assertTrue(valid_genre(requestdata),
                        "This Genre's dict object should be valid")
