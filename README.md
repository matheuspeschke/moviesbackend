# moviesbackend #

A flask RESTful API to manage a collection of DVDs containing movies and TV shows. The underlying database is MySQL, with an MVC (Model View Controller) managed by Flask-SQLAchemy. The Docker image uses gunicorn as the Python WSGI HTTP web server.      

## iMDB and Web Crawlers ##

Nowadays most cinephile websites (like letterboxd) provide an API to expose their contents and to allow users to customize their experience. To this day, iMDB still resists to share their database with others.   

This project provides a series of bash scripts that can download and parse iMDB webpages and feed this RESTful API with their data. See folder 'imdb-bash'.  

# Development Guidelines #

If using a Python IDE (Visual Studio Code, PyCharm, etc) it will try to detect the Python interpreter. The instructions below can be applied directly from the command line, without an IDE.    

*Note: this project has issues with Python > 3.6. Configure your IDE to use version 3.6 as the Python interpreter.*  

## Local dependencies ##

You must install the following system dependencies:

Mac OS:  
*$ source system-macos-dependencies.sh*

Ubuntu 18:  
*$ source system-ubuntu18-dependencies.sh*

CentOS 7:  
*$ source system-centos7-dependencies.sh*

Run the commands:

*$ python3 -m venv .venv*

*$ source .venv/bin/activate*

*$ pip3 install -r requirements.txt*

## Building ##

N/A

## Running unit tests ##

*$ export CONFIGENV=moviesbackend.config.TestingConfig; export DATABASECONNECTION=sqlite:///:memory:; coverage run -m unittest discover*

*Note: The bash scripts in 'imdb-bash' are also part of the unit tests. However, some of the unit tests might fail, because iMDB website relies on your geolocation to customize its contents.*
 
## Code coverage ##

*$ coverage report -m --omit=".venv/\*,tests/\*"*    

*$ coverage html --omit=".venv/\*,tests/\*"*  

## Running Code Quality checking ##

*$ pylint moviesbackend/\*.py tests/\*.py*  

## Running as a Docker container ##

First, create the MySQL database:  

*\# docker run --name movies-mysql --net=host -v ~/docker/movies-mysql/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD="<AKco-.12308vslcslkv" -d mysql:5.7*

*\# docker exec -i movies-mysql mysql --user=root --password="<AKco-.12308vslcslkv" --force < $PWD/data/MySqlDatabase/BurntDvdsDatabaseDump.sql*

### (Development) Running the RESTful API (Backend) ###

In your IDE or terminal:

*$ export CONFIGENV=moviesbackend.config.TestingConfig; export DATABASECONNECTION=mysql://burntdvds:BuRnTdVdS8902348.ovusoiud@127.0.0.1/burntdvds; python -m moviesbackend.app*

or using Docker:

*\# docker build --tag moviesbackend:develop .*

*\# docker run --rm -it --name moviesbackend --net=host -e CONFIGENV=moviesbackend.config.TestingConfig -e DATABASECONNECTION='mysql://burntdvds:BuRnTdVdS8902348.ovusoiud@127.0.0.1/burntdvds' moviesbackend:develop*

RestFUL API endpoints:  

http://127.0.0.1:5000

### (Production) Running the RESTful API (Backend) ###

*\# docker build --tag moviesbackend:latest .*  

*\# docker run --rm -it --name moviesbackend --net=host -e CONFIGENV=moviesbackend.config.ProdConfig -e DATABASECONNECTION='mysql://burntdvds:BuRnTdVdS8902348.ovusoiud@127.0.0.1/burntdvds' moviesbackend:latest*  

RestFUL API endpoints:    

http://127.0.0.1:5000  

## Generate documentation ##

For future releases, this project can constitute a pip package. In that case, documentation would be in the setup.py file.  

# Contribution guidelines #

This project uses unit tests to validate the implementation (folder tests/). Bitbucket pipelines build and deploy the Docker image to Docker Hub (https://hub.docker.com/r/mpeschke/moviesbackend).    

a. Ask the repo owner to be included in the Collaborators group (you must have a valid Bitbucket account (email)).  

b. Branch from Develop branch to a new Feature branch.  

c. Create the fixtures and test cases.  

d. Open a pull request to the Develop branch.  