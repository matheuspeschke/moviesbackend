#!/bin/bash

USAGE="bash Create Movie DJANGO.sh url=\"value\" port=\"value\" dvdname=\"value\" format=\"value\" subtitlelangs=\"value,value,value,etc\" imdbid=\"value\""

while [ $# -gt 0 ]; do
	case "$1" in
	url=*)
		MOVIESURL="${1#*=}"
		;;
	port=*)
		MOVIESPORT="${1#*=}"
		;;
	dvdname=*)
		DVDNAME="${1#*=}"
		;;
	format=*)
		FORMAT="${1#*=}"
		;;
  subtitlelangs=*)
    SUBTITLELANGS="${1#*=}"
    ;;
	imdbid=*)
		IMDBID="${1#*=}"
		;;
	*)
	printf "***************************\n"
	printf "* Error: Invalid argument.*\n"
	printf "* Usage (all arguments are optional): $USAGE*\n"
	printf "***************************\n"
	exit 1
	esac
	shift
done

if [ "$MOVIESURL" == "" ]; then
	echo "Using default URL http://localhost"
	MOVIESURL=http://localhost
fi

if [ "$MOVIESPORT" == "" ]; then
	echo "Using default URL port 8000"
	MOVIESPORT=8000
fi

if [ "$DVDNAME" == "" ]; then
	echo -n "Enter the dvd name: "
	read DVDNAME
fi

if [ "$FORMAT" == "" ]; then
	echo -n "Enter the format: "
	read FORMAT
fi

if [ "$IMDBID" == "" ]; then
	echo -n "Enter the imdb ID: "
	read IMDBID
fi

if [ "$SUBTITLELANGS" == "" ]; then
  echo -n "Enter the subtitle languages (coma separated): "
  read SUBTITLELANGS
fi

# This imdb parser bash script will read all the movie data from the iMDB website.
bash imdb_v2.0.sh "$MOVIESURL" "$MOVIESPORT" "$IMDBID" "$DVDNAME" "$FORMAT" "$SUBTITLELANGS"
