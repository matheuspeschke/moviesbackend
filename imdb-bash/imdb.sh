#!/usr/bin/env bash
# Calls the Flask API to create a Movie resource.

# Import functions to extract info from iMDB pages.
# This will make easier for unit testing.
source ./imdbfunctions.sh

MOVIESURL="${1}"
MOVIESPORT="${2}"
IMDBID="${3}"
MOVIEDVD="${4}"
MOVIEFORMAT="${5}"
SUBTITLES="${6}"

# Test for valid URL
URL="$(valid_movie_url "${IMDBID}")"
if [ "${URL}" == "" ]; then
	echo '
Invalid imdb ID. You must supply an iMDB movie ID.
For example:
In https://www.imdb.com/title/tt1446714 the movie ID is tt1446714.
'
	exit 1
fi

# Get the HTML content.
HTML="$(get_page_content "${URL}")"
if [ "${HTML}" == "" ]; then
	echo "
Page content for ${URL} was empty.
"
	exit 1
fi

# Get movie's full cast web page url.
URLFULLCREDIT="$(get_movie_fullcredit_url "${IMDBID}")"
if [ "${URLFULLCREDIT}" == "" ]; then
	echo '
Invalid imdb ID. You must supply an iMDB movie ID.
For example:
In https://www.imdb.com/title/tt1446714 the movie ID is tt1446714.
'
	exit 1
fi

# Get the Full Credit HTML content.
FC="$(get_page_content "${URLFULLCREDIT}")"
# Parse the HTML to contain only the list of tags representing the actors.
FCACTORSONLY="$(get_movie_cast_html "${FC}")"

# Get the movie's title.
MOVIETITLE="$(get_movie_title "${FC}")"
if [ "${MOVIETITLE}" == "" ]; then
	echo "
Could not determine the movie's title.
"
	exit 1
fi

# Get the director's name(s).
MOVIEDIRECTORS="$(get_movie_directors "${FC}")"
if [ "${MOVIEDIRECTORS}" == "" ]; then
	echo "
Could not determine the movie's director(s).
"
	exit 1
fi

# Get the year.
MOVIEYEAR="$(get_movie_release_year "${HTML}")"
if [ "${MOVIEYEAR}" == "" ]; then
	echo -n "Failed to retrieve year from iMDB. Manual input: "
	read -r MOVIEYEAR
fi

# Get the Actors (as JSON messages separated by a pipe).
ACTORSJSONMESSAGE="$(get_movie_actors_jsons "${FCACTORSONLY}")"

ACTORSJSONMESSAGE="$(get_movie_actors_jsons "$(get_movie_cast_html "$(get_page_content "${URLFULLCREDIT}")")")"

if [ "${ACTORSJSONMESSAGE}" == "" ]; then
	echo "
Unable to get actors in ${URL}.
"
	exit 1
fi

OLDIFS="${IFS}"
IFS='|'
read -r -a ACTORSARRAY <<< "${ACTORSJSONMESSAGE}"
IFS="${OLDIFS}"

###############################################################################
# Step 1 of 6: Create new Genres referenced by the movie in the database, if required.
GENRELIST="$(get_movie_genres "${HTML}")"
if [ "${GENRELIST}" == "" ]; then
	echo "
Unable to get genres in ${URL}.
"
	exit 1
fi
GENRESARRAY="$(echo "${GENRELIST}")"

for genre in "${GENRESARRAY[@]}"; do
	if [ "${genre}" == "" ]; then
		echo "ERROR: Genre returned from iMDB webpage is empty: '${genre}'"
		exit 2
	fi
	OLDIFS="${IFS}"
	IFS=$'\n'
	RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -H 'Accept: application/json' "${MOVIESURL}":"${MOVIESPORT}"/genres/"${genre}" -X GET)")
	IFS="$OLDIFS"
	i=0
	for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	do
		HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
		if [ "${HTTPSTATUSCODE}" != "" ]; then
			break
		fi
	done
	if [ "${HTTPSTATUSCODE}" == "200" ]; then
    echo "SUCCESS: Found genre '${genre}' in the database"
	else
    GENRENAME=${${genre}#"\""} # Remove leading characters
    GENRENAME=${GENRENAME%"\""} # Remove trailer characters
    echo "WARNING: Genre '${GENRENAME}' not found in the database - will be added"
    OLDIFS="${IFS}"
    IFS=$'\n'
    RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "{\"name\":\"${GENRENAME}\"}" -H 'Content-Type: application/json' "${MOVIESURL}":"${MOVIESPORT}"/genres)")
    IFS="${OLDIFS}"
    for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
    do
      HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
      if [ "${HTTPSTATUSCODE}" != "" ]; then
        break
      fi
    done
    if [ "${HTTPSTATUSCODE}" != "201" ]; then
      JOINED=""
      printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
      echo "ERROR: Failed to add genre '${GENRENAME}' to the database. HTTP status code: ${HTTPSTATUSCODE}.
Message:
${JOINED}"
      exit 4
    else
      echo "SUCCESS: New genre '${GENRENAME}' added to the database"
    fi
	fi
done

###############################################################################
# Step 2 of 6: Create all Actors in the database.
for JSACTOR in "${ACTORSARRAY[@]}"; do
	OLDIFS="${IFS}"
	IFS=$'\n'
	RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "${JSACTOR}" -H 'Content-Type: application/json' "${MOVIESURL}":"${MOVIESPORT}"/actors)")
	IFS="$OLDIFS"
	i=0
	for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	do
		HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
		if [ "${HTTPSTATUSCODE}" != "" ]; then
			break
		fi
	done # Previously tested on HTTP 1.0 web servers - status code 200. HTTP 1.1 servers return an additional status code 100. 
	if [ "${HTTPSTATUSCODE}" != "201" ] && [ "${HTTPSTATUSCODE}" != "100" ]; then
		JOINED=""	
		printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
		echo "ERROR: Failed to add actor to the database. HTTP status code: ${HTTPSTATUSCODE}.
Message:
${JOINED}"
	else
		echo "SUCCESS: New actor '${JSACTOR}' added to the database"
	fi
done

###############################################################################
# Step 3 of 6: Create the Movie.
MOVIEJSONMESSAGE="{\"imdbid\":\"${IMDBID}\",\"format\":\"${MOVIEFORMAT}\",\"dvd\": \"${MOVIEDVD}\",\"year\":\"${MOVIEYEAR}\",\"director\":\"${MOVIEDIRECTORS}\",\"name\":\"${MOVIETITLE}\", \"actors\": [], \"genres\": [], \"subtitles\": []}"
OLDIFS="${IFS}"
IFS=$'\n'
RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "${MOVIEJSONMESSAGE}" -H "Content-Type: application/json" "${MOVIESURL}":"${MOVIESPORT}"/movies)")
IFS="${OLDIFS}"

HTTPSTATUSCODE=""
for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
do
	HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
	if [ "${HTTPSTATUSCODE}" != "" ]; then
		break
	fi
done

# Previously tested on HTTP 1.0 web servers - status code 200. HTTP 1.1 servers return an additional status code 100. 
if [ "${HTTPSTATUSCODE}" != "201" ] && [ "${HTTPSTATUSCODE}" != "100" ]; then
	JOINED=""	
	printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	echo "ERROR: Creating a new movie failed with HTTP status code ${HTTPSTATUSCODE}.
Message:
${JOINED}
JSON data:
${MOVIEJSONMESSAGE}"
	exit 5
fi

###############################################################################
# Step 4 of 6: Update the movie with all actors.
for JSACTOR in "${ACTORSARRAY[@]}"; do
	OLDIFS="${IFS}"
	IFS=$'\n'
	ACTORIMDBID=$(echo "${JSACTOR}" | grep -o "\"imdbid\":\"[0-9]*\"" | sed 's/imdbid/actorimdbid/')
	RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "{\"movieimdbid\": \"$IMDBID\", $ACTORIMDBID}" -H 'Content-Type: application/json' "${MOVIESURL}":"${MOVIESPORT}"/movies/"${IMDBID}"/actors)")
	IFS="${OLDIFS}"
	for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	do
		HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
		if [ "${HTTPSTATUSCODE}" != "" ]; then
			break
		fi
	done # Previously tested on HTTP 1.0 web servers - status code 200. HTTP 1.1 servers return an additional status code 100.
	if [ "${HTTPSTATUSCODE}" != "201" ] && [ "${HTTPSTATUSCODE}" != "100" ]; then
		JOINED=""
		printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
		echo "ERROR: Failed to add movie actor to the database. HTTP status code: ${HTTPSTATUSCODE}.
Message:
$JOINED"
	else
		echo "SUCCESS: New movie '${IMDBID}' actor '{\"movieimdbid\": \"${IMDBID}\", ${ACTORIMDBID}}' added to the database"
	fi
done

###############################################################################
# Step 5 of 6: Update the movie with all genres.
for genre in "${GENRESARRAY[@]}"; do
	OLDIFS="${IFS}"
	IFS=$'\n'
	RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "{\"movieimdbid\": \"${IMDBID}\", \"genre\": \"${genre}\"}" -H 'Content-Type: application/json' "${MOVIESURL}":"${MOVIESPORT}"/movies/"${IMDBID}"/genres)")
	IFS="${OLDIFS}"
	for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	do
		HTTPSTATUSCODE=$(echo "${i}" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
		if [ "${HTTPSTATUSCODE}" != "" ]; then
			break
		fi
	done # Previously tested on HTTP 1.0 web servers - status code 200. HTTP 1.1 servers return an additional status code 100.
	if [ "${HTTPSTATUSCODE}" != "201" ] && [ "${HTTPSTATUSCODE}" != "100" ]; then
		JOINED=""
		printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
		echo "ERROR: Failed to add movie genre to the database. HTTP status code: ${HTTPSTATUSCODE}.
Message:
${JOINED}"
	else
		echo "SUCCESS: New movie '${IMDBID}' genre '${genre}' added to the database"
	fi
done

###############################################################################
# Step 6 of 6: Update the movie with all subtitles.
OLDIFS="${IFS}"
IFS=','
read -r -a SUBTITLELANGSARRAY <<< "${SUBTITLES}"
IFS="${OLDIFS}"
for sub in "${SUBTITLELANGSARRAY[@]}"; do
  OLDIFS="${IFS}"
	IFS=$'\n'
	RESTFULBACKENDMOVIEOUTPUT=("$(curl -silent -d "{\"movieimdbid\": \"${IMDBID}\", \"subtitle\": \"${sub}\"}" -H 'Content-Type: application/json' "${MOVIESURL}":"${MOVIESPORT}"/movies/"${IMDBID}"/subtitles)")
  IFS="${OLDIFS}"
	for i in "${RESTFULBACKENDMOVIEOUTPUT[@]}"
	do
		HTTPSTATUSCODE=$(echo "$i" | grep -o "HTTP/[0-9]*.[0-9]* [0-9]*" | grep -oP "\d{3}")
		if [ "${HTTPSTATUSCODE}" != "" ]; then
			break
		fi
	done # Previously tested on HTTP 1.0 web servers - status code 200. HTTP 1.1 servers return an additional status code 100.
	if [ "${HTTPSTATUSCODE}" != "201" ] && [ "${HTTPSTATUSCODE}" != "100" ]; then
		JOINED=""
		printf -v JOINED "%s\n" "${RESTFULBACKENDMOVIEOUTPUT[@]}"
		echo "ERROR: Failed to add movie subtitle to the database. HTTP status code: ${HTTPSTATUSCODE}.
Message:
${JOINED}"
	else
		echo "SUCCESS: New movie '${IMDBID}' subtitle '${sub}' added to the database"
	fi
done

echo "SUCCESS: Movie created. You can edit this movie at ${MOVIESURL}:${MOVIESPORT}/movies/${IMDBID}"