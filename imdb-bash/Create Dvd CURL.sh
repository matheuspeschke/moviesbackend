#!/bin/bash

USAGE="bash Create Dvd DJANGO.sh url=\"value\" port=\"value\" pinoname=\"value\" dvdname=\"value\""

while [ $# -gt 0 ]; do
	case "$1" in
	url=*)
		MOVIESURL="${1#*=}"
		;;
	port=*)
		MOVIESPORT="${1#*=}"
		;;
	pinoname=*)
		PINONAME="${1#*=}"
		;;
	dvdname=*)
		DVDNAME="${1#*=}"
		;;
	*)
	printf "***************************\n"
	printf "* Error: Invalid argument.*\n"
	printf "* Usage (all arguments are optional): $USAGE*\n"
	printf "***************************\n"
	exit 1
	esac
	shift
done

if [ "$MOVIESURL" == "" ]; then
	echo "Using default URL http://localhost"
	MOVIESURL=http://127.0.0.1
fi

if [ "$MOVIESPORT" == "" ]; then
	echo "Using default URL port 8000"
	MOVIESPORT=8000
fi

if [ "$PINONAME" == "" ]; then
	echo -n "Enter the pino name: "
	read PINONAME
fi

if [ "$DVDNAME" == "" ]; then
	echo -n "Enter the Dvd name: "
	read DVDNAME
fi

curl --verbose -d "{\"pino\": \"$PINONAME\", \"name\": \"$DVDNAME\"}" -H "Content-Type: application/json" $MOVIESURL:$MOVIESPORT/dvds

# OUTPUT EXAMPLE:
# Using default URL http://localhost
# Using default URL port 8000
# Enter the pino name: 00000017
# Enter the Dvd name: 000003CB
# *   Trying 127.0.0.1...
# * TCP_NODELAY set
# * Connected to 127.0.0.1 (127.0.0.1) port 8000 (#0)
# > POST /dvds HTTP/1.1
# > Host: 127.0.0.1:8000
# > User-Agent: curl/7.58.0
# > Accept: */*
# > Content-Type: application/json
# > Content-Length: 40
# >
# * upload completely sent off: 40 out of 40 bytes
# * HTTP 1.0, assume close after body
# < HTTP/1.0 201 CREATED
# < Content-Type: application/json
# < Content-Length: 0
# < Location: http://127.0.0.1:8000/dvds/000003CB
# < Server: Werkzeug/1.0.1 Python/3.6.9
# < Date: Sat, 18 Jul 2020 00:25:25 GMT
# <
# * Closing connection 0
#

echo ""
