#!/bin/bash

USAGE="bash Create Pino DJANGO.sh url=\"value\" port=\"value\" pinoname=\"value\""

while [ $# -gt 0 ]; do
	case "$1" in
	url=*)
		MOVIESURL="${1#*=}"
		;;
	port=*)
		MOVIESPORT="${1#*=}"
		;;
	pinoname=*)
		PINONAME="${1#*=}"
		;;
	*)
	printf "***************************\n"
	printf "* Error: Invalid argument.*\n"
	printf "* Usage (all arguments are optional): $USAGE*\n"
	printf "***************************\n"
	exit 1
	esac
	shift
done

if [ "$MOVIESURL" == "" ]; then
	echo "Using default URL http://localhost"
	MOVIESURL=http://127.0.0.1
fi

if [ "$MOVIESPORT" == "" ]; then
	echo "Using default URL port 8000"
	MOVIESPORT=8000
fi

if [ "$PINONAME" == "" ]; then
	echo -n "Enter the pino name: "
	read PINONAME
fi

curl --verbose -d "{\"name\": \"$PINONAME\"}" -H "Content-Type: application/json" $MOVIESURL:$MOVIESPORT/pinos

echo ""
