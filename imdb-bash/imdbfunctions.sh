#!/usr/bin/env bash
# Dependencies:
: <<'END'
bash
python3
recode
awk/gawk
xmllint
END

valid_movie_url() {
  local MOVIEURLREGEX='https://www.imdb.com/title/tt[0-9]*'
  local ID="${1}"
  local URL="https://www.imdb.com/title/${ID}"
  local TESTURL=$(echo "${URL}" | grep -o "${MOVIEURLREGEX}")

  echo "${TESTURL}"
}

get_movie_fullcredit_url() {
  local FULLCREDSUFFIX='/fullcredits?ref_=tt_cl_sm#cast'
  local ID="${1}"
  local URL="$(valid_movie_url "${ID}")"

  if [ "${URL}" != "" ]; then
    echo "${URL}${FULLCREDSUFFIX}"
  else
    echo ""
  fi
}

get_page_content() {
  local URL="${1}"

  if [ "${URL}" != "" ]; then
    local WEBCONTENT="$(wget -qO- "${URL}")"
    echo "${WEBCONTENT}"
  else
    echo ""
  fi
}

get_movie_release_year() {
  local HTML="${1}"
  local MOVIEYEAR=""

  if [ "${HTML}" != "" ]; then
    MOVIEYEAR=$(echo "${HTML}" | gawk -F";" \
'{
	if ($0 ~  "<h4 class=\"inline\">Release Date:</h4>.*")
	{
		year = $1
		gsub(/    <h4 class=\"inline\">Release Date:<\/h4> /, "", year)
		match(year, /([0-9]{4})/, arr)
		print arr[1]
	}
}' | recode HTML_4.0..ISO-8859-1)
    echo "${MOVIEYEAR}"
  else
    echo ""
  fi
}

get_movie_genres() {
  if [ "${1}" != "" ]; then
    local JSON="$(printf '%s\n' "${1}" | xmllint --html --xpath '//script[contains(@type,"application/ld+json")]//text()' 2>/dev/null -)"
    JSON="$(echo "${JSON}" | sed -z 's/\n//g')"
    local MOVIEGENRES="$(echo "${JSON}" | python3 -c "import sys, json;
for i in json.load(sys.stdin)['genre']:
  print(i)
")"
    echo "${MOVIEGENRES}"
  else
    echo ""
  fi
}

get_movie_cast_html() {
  if [ "${1}" != "" ]; then
    local MOVIECASTHTML="$(printf '%s\n' "${1}" | xmllint --html --xpath '//table[contains(@class,"cast_list")]' 2>/dev/null -)"
    echo "${MOVIECASTHTML}"
  else
    echo ""
  fi
}

get_movie_title() {
  if [ "${1}" != "" ]; then
    local MOVIETITLE
    read -r MOVIETITLE <<< $(echo "${1}" \
      | grep -o "<meta name=\"title\" content=\".* (.*) - IMDb\" />" \
      | grep -o "content=\".*(" \
      | grep -o "\".*(" \
      | grep -o "[^\"(]*" \
      | recode HTML_4.0..ISO-8859-1 \
    | awk -F";" \
    '{ print $1 }')
    MOVIETITLE=${MOVIETITLE#" "} # Remove leading characters
    MOVIETITLE=${MOVIETITLE%" "} # Remove trailer characters
    MOVIETITLE=$(echo "${MOVIETITLE}" | sed "s/\"//g") # remove all double quotes
    echo "${MOVIETITLE}"
  else
    echo ""
  fi
}

get_movie_directors() {
  if [ "${1}" != "" ]; then
    local MOVIEDIRECTORS
    MOVIEDIRECTORS=$(echo "${1}" | gawk -F";" \
'BEGIN {directors[0]="";
count=0} {
  if ($0 ~ "<h4 class=\"dataHeaderWithBorder\">Directed by")
  {
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    getline;
    directors[count]=substr($1, 3)
    count++
  }
} END {
  joineddirectors=""
  for (i = 0; i < count; i++)
    joineddirectors=joineddirectors ", " directors[i];
  print joineddirectors
}')
    MOVIEDIRECTORS=${MOVIEDIRECTORS#","} # Remove leading characters
    MOVIEDIRECTORS=${MOVIEDIRECTORS%","} # Remove trailer characters
    MOVIEDIRECTORS=${MOVIEDIRECTORS#" "} # Remove leading characters
    MOVIEDIRECTORS=${MOVIEDIRECTORS%" "} # Remove trailer characters
    echo "${MOVIEDIRECTORS}"
  else
    echo ""
  fi
}

get_movie_actors_jsons() {
  if [ "${1}" != "" ]; then
    local ACTORSJSONMESSAGE
    ACTORSJSONMESSAGE=$(echo "${1}" | gawk -F";" \
'BEGIN {actors[0]="";
count=0} {
	if ($0 ~  "<a href=\"/name/nm[0-9]*/\"> ")
	{
		actorname = $1
		actorid = $1
		sub(/<a href=\"\/name\/nm[0-9]*\/\"> /, "", actorname)
		sub(/<a href=\"\/name\/nm/, "", actorid)
		sub(/\/\"> .*/, "", actorid)
		actors[count]="{\x22name\x22:\x22"actorname"\x22,\x22imdbid\x22:\x22"actorid"\x22}"
		count++
	}
} END {
	joinedactors=""
	for (i = 0; i < count; i++)
		joinedactors=joinedactors "|" actors[i];
	print joinedactors
}' | recode HTML_4.0..ISO-8859-1)
    ACTORSJSONMESSAGE=${ACTORSJSONMESSAGE#"|"} # Remove leading characters
    ACTORSJSONMESSAGE=${ACTORSJSONMESSAGE%"|"} # Remove trailer characters
    echo "${ACTORSJSONMESSAGE}"
  else
    echo ""
  fi
}