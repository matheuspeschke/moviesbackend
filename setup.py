# coding=UTF-8
# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='moviesbackend',
    version='0.1.0',
    description='Flask backend for the movies application',
    long_description=readme,
    author='Matheus Peschke de Azevedo',
    author_email='mpeschke@gmail.com',
    url='https://bitbucket.org/matheuspeschke/movies-flask-react',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)