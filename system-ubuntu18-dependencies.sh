#!/usr/bin/env bash

apt-get update -y && apt install -y python-dev python3-dev python3-pip python3-venv python3-pymysql libmysqlclient-dev \
default-libmysqlclient-dev libssl-dev

# For development, uncomment the line below:
#apt install -y docker.io